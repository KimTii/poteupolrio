﻿using UnityEngine;
using System.Collections;

public class UI : MonoBehaviour {

    public UILabel lMeter;
    public UILabel lScore;
    public UILabel lCoin;
    public UILabel lBox;
    //public UIButton bPowerShot;
    public UILabel lPowerShotCount;
    //public UIButton bSlow;
    public UILabel lSlowCount;

    int meter;
    int addMeter;
    int score;
    int coin;
    int box;
    int powershotCount;
    int slowCount;
    float powershotSecond;

    bool isPowershot;

	void Start () {
        meter = score = coin = box = slowCount = 0;
        powershotCount = 1;
        addMeter = 1;
        powershotSecond = 5;

        lMeter.text = meter.ToString("n0") + " M";
        lScore.text = score.ToString("n0");
        lCoin.text = score.ToString("n0");
        lBox.text = 'X' + score.ToString("n0");
        lPowerShotCount.text = powershotCount.ToString();
        lSlowCount.text = slowCount.ToString();

        isPowershot = false;

        StartCoroutine("MyUpdate");
	}
	
    IEnumerator MyUpdate()
    {
        while(true)
        {
            meter += addMeter;
            lMeter.text = meter.ToString("n0") + " M";
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void ScorePlus(int addScore)
    {
        score += addScore;
        lScore.text = score.ToString("n0");
    }

    public void CoinPlus(int addCoin)
    {
        coin += addCoin;
        lCoin.text = coin.ToString("n0");
    }

    public void PowerShot() {
        if (!isPowershot && powershotCount > 0)
        {
            --powershotCount;
            lPowerShotCount.text = powershotCount.ToString();
            isPowershot = true;
            StartCoroutine("PowerShotCoroutine");
        }
    }
    IEnumerator PowerShotCoroutine()
    {
        GameObject.Find("Sunny/BulletPoint").SendMessage("ActivePowerShot", powershotSecond);
        yield return new WaitForSeconds(powershotSecond);
        isPowershot = false;
    }
    
}
