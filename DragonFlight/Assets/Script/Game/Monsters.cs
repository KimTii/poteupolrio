﻿using UnityEngine;
using System.Collections;

public class Monsters : MonoBehaviour {

    const int MAX_DRAGON_COUNT = 10;
    GameObject[] dragons;

	void Start () {
        dragons = new GameObject[MAX_DRAGON_COUNT];
        GameObject dragon = Resources.Load("Prefab/Dragon_01") as GameObject;

        for(int i = 0; i < MAX_DRAGON_COUNT; ++i)
        {
            dragons[i] = Instantiate(dragon, transform, false) as GameObject;
            dragons[i].transform.localPosition = new Vector3(XPoint(i), 2000, 0);
        }
        StartCoroutine("MyUpdate");
	}

    IEnumerator MyUpdate()
    {
        int front, end;
        bool forwardParty = false;

        while (true)
        {
            if(forwardParty)
            {
                front = 0;
                end = 4;
                forwardParty = false;
            }
            else
            {
                front = 5;
                end = 9;
                forwardParty = true;
            }
            for(; front <= end; ++front)
            {
                dragons[front].transform.localPosition = new Vector3(XPoint(front), 1500, 0);
                dragons[front].SendMessage("TurnOnDragon");
            }
            yield return new WaitForSeconds(4);
        }
    }

    int XPoint(int count)
    {
        if (count == 0 || count == 5)
            return -410;
        else if (count == 1 || count == 6)
            return -205;
        else if (count == 2 || count == 7)
            return 0;
        else if (count == 3 || count == 8)
            return 205;
        else
            return 410;
    }
}
