﻿using UnityEngine;
using System.Collections;

public class BulletPoint : MonoBehaviour {

    GameObject bullet;
    public UIPanel bulletsPanel;

    GameObject[] bullets;
    const int MAX_BULLET_COUNT = 20;

    int bulletIndex;
    public float createSpeed;

    public ParticleSystem powershotAura;

	void Start () {
        bullet = Resources.Load("Prefab/bullet") as GameObject;
        bullets = new GameObject[MAX_BULLET_COUNT];

        bulletIndex = 0;
        //createSpeed = 0.2f;

        for (int i = 0; i < MAX_BULLET_COUNT; ++i)
        {
            bullets[i] = Instantiate(bullet, this.transform, false) as GameObject;
            bullets[i].transform.parent = bulletsPanel.transform;
            bullets[i].transform.localPosition = new Vector3(0, -2000f, 0);
        }
        StartCoroutine(MyUpdate());
	}
    void Wakeup() { Start(); }

    IEnumerator MyUpdate()
    {
        while(true)
        {
            bullets[bulletIndex].transform.position = this.transform.position;
            bullets[bulletIndex].SendMessage("BulletFire");

            ++bulletIndex;
            if (bulletIndex >= MAX_BULLET_COUNT)
                bulletIndex = 0;
            yield return new WaitForSeconds(createSpeed);
        }
    }

    void ActivePowerShot(float powershotSecond)
    {
        StartCoroutine("ActivePowerShotCoroutine", powershotSecond);
    }
    IEnumerator ActivePowerShotCoroutine(float powershotSecond)
    {
        StartCoroutine("ChangeBullet", true);
        yield return new WaitForSeconds(powershotSecond);
        StartCoroutine("ChangeBullet", false);
    }
    IEnumerator ChangeBullet(bool powershot)
    {
        // 대기중인 Bullet만 이미지 변경하기 위한 수단
        int index = bulletIndex;
        // Bullet이 모두 이미지 변경이 되었는지 체크하기 위한 변수
        bool[] changePowerShot = new bool[MAX_BULLET_COUNT];
        bool allChange = false;
        for(int i = 0; i < MAX_BULLET_COUNT; ++i) { changePowerShot[i] = false; }
        // 무엇으로 변경해야하는지 설정
        string bulletName;
        int x, y;
        if (powershot)
        {
            bulletName = "bullet_powershot";
            x = 150;
            y = 218;
            powershotAura.Play();
        }
        else
        {
            bulletName = "bullet_01_01";
            x = y = 64;
            powershotAura.Stop();
        }

        while (true)
        {
            ++index;
            if (index >= MAX_BULLET_COUNT)
                index = 0;

            if (bullets[index].GetComponent<Bullet>().GetBulletState() != Bullet.BulletState.Fire)
            {
                bullets[index].GetComponent<UISprite>().spriteName = bulletName;
                bullets[index].GetComponent<UISprite>().SetDimensions(x, y);
                bullets[index].GetComponent<BoxCollider>().size = new Vector3(x, y, 0);
                changePowerShot[index] = true;
            }
            foreach(bool member in changePowerShot)
            {
                if (!member)
                {
                    allChange = false;
                    break;
                }
                else
                    allChange = true;
            }

            if (allChange)
                StopCoroutine("ChangeBullet");
            yield return null;
        }
    }
}
