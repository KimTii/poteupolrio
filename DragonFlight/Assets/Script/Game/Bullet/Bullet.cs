﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public enum BulletState { Ready, Fire }
    [HideInInspector] public BulletState bulletState;
    public int damege;
    public int bulletSpeed;


    void Start () {
        bulletState = BulletState.Ready;
        damege = 1;
        bulletSpeed = 1000; // 발사체(총알) 속도
	}

    void SetDamege(int _damege = 1) { damege = _damege; }
    void SetBulletSpeed(int _speed = 1000) { bulletSpeed = _speed; }
    public BulletState GetBulletState() { return bulletState; }
}
