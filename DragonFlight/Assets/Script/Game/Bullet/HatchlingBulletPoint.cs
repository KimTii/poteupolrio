﻿using UnityEngine;
using System.Collections;

public class HatchlingBulletPoint : MonoBehaviour {

    GameObject bullet;
    public UIPanel bulletsPanel;

    GameObject[] bullets;
    const int MAX_BULLET_COUNT = 10;

    int bulletIndex;
    public float createSpeed;
    bool firstHatchling;

    void Start()
    {
        bullet = Resources.Load("Prefab/HatchlingBullet") as GameObject;
        SetupBullet();
        bullets = new GameObject[MAX_BULLET_COUNT];
        bulletIndex = 0;
        //createSpeed = 0.2f;
        for (int i = 0; i < MAX_BULLET_COUNT; ++i)
        {
            bullets[i] = Instantiate(bullet, this.transform, false) as GameObject;
            bullets[i].transform.parent = bulletsPanel.transform;
            bullets[i].transform.localPosition = new Vector3(0, -2000f, 0);
        }
        StartCoroutine(MyUpdate());
    }
    void Wakeup() { Start(); }

    IEnumerator MyUpdate()
    {
        while (true)
        {
            bullets[bulletIndex].transform.position = this.transform.position;
            bullets[bulletIndex].SendMessage("Fire", firstHatchling);

            ++bulletIndex;
            if (bulletIndex >= MAX_BULLET_COUNT)
                bulletIndex = 0;
            yield return new WaitForSeconds(createSpeed);
        }
    }

    void SetupBullet()
    {
        int hatclingNumber = 0;
        if (transform.parent.name == "Hatchling1") {
            hatclingNumber = UserData.Instance.userInformation.GetHatchling1Number();
            firstHatchling = true;
        }
        else { // Hatchling2 
            hatclingNumber = UserData.Instance.userInformation.GetHatchling2Number();
            firstHatchling = false;
        }
            

        DB.HATCHLING_TYPE hatchlingType = DB.Instance.GetHatchlingType(hatclingNumber);
        DB.ELEMENT element1 = DB.Instance.GetHatchlingElement1(hatclingNumber);
        DB.ELEMENT element2 = DB.Instance.GetHatchlingElement2(hatclingNumber);
        string bulletName = DB.Instance.GetHatchlingBulletName(hatclingNumber);

        bulletName = bulletName.TrimEnd();
        bullet.GetComponent<UISprite>().spriteName = bulletName;
    }

    IEnumerator ChangeBullet(bool powershot)
    {
        // 대기중인 Bullet만 이미지 변경하기 위한 수단
        int index = bulletIndex;
        // Bullet이 모두 이미지 변경이 되었는지 체크하기 위한 변수
        bool[] changePowerShot = new bool[MAX_BULLET_COUNT];
        bool allChange = false;
        for (int i = 0; i < MAX_BULLET_COUNT; ++i) { changePowerShot[i] = false; }
        // 무엇으로 변경해야하는지 설정
        string bulletName;
        int x, y;
        if (powershot)
        {
            bulletName = "bullet_powershot";
            x = 150;
            y = 218;
        }
        else
        {
            bulletName = "bullet_01_01";
            x = y = 64;
        }

        while (true)
        {
            ++index;
            if (index >= MAX_BULLET_COUNT)
                index = 0;

            if (bullets[index].GetComponent<Bullet>().GetBulletState() != Bullet.BulletState.Fire)
            {
                bullets[index].GetComponent<UISprite>().spriteName = bulletName;
                bullets[index].GetComponent<UISprite>().SetDimensions(x, y);
                bullets[index].GetComponent<BoxCollider>().size = new Vector3(x, y, 0);
                changePowerShot[index] = true;
            }
            foreach (bool member in changePowerShot)
            {
                if (!member)
                {
                    allChange = false;
                    break;
                }
                else
                    allChange = true;
            }

            if (allChange)
                StopCoroutine("ChangeBullet");
            yield return null;
        }
    }
}
