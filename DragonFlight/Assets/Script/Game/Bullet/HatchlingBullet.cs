﻿using UnityEngine;
using System.Collections;

public class HatchlingBullet : Bullet {

    int hatchlingNumber;
    DB.HATCHLING_TYPE hatchlingType;

    public void Fire(bool firstHatchling)
    {
        if(firstHatchling)
            hatchlingNumber = UserData.Instance.userInformation.GetHatchling1Number();
        else
            hatchlingNumber = UserData.Instance.userInformation.GetHatchling2Number();

        hatchlingType = DB.Instance.GetHatchlingType(hatchlingNumber);
        switch (hatchlingType)
        {
            case DB.HATCHLING_TYPE.AUTOMATIC: StartCoroutine("Automatic"); break;
        }
    }
    IEnumerator Automatic()
    {
        while (true)
        {
            if (this.transform.localPosition.y > 1000 || this.transform.localPosition.y < -1900)
            {
                bulletState = BulletState.Ready;
                this.transform.localPosition = new Vector3(0, -2000f, 0);
                StopCoroutine("Automatic");
            }
            else
            {
                bulletState = BulletState.Fire;
                this.transform.localPosition += new Vector3(0, bulletSpeed, 0) * Time.deltaTime;
            }
            yield return null;
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Monster")
        {
            coll.gameObject.SendMessage("Hit", damege);
            this.transform.localPosition = new Vector3(0, -2000f, 0);
        }
    }
}
