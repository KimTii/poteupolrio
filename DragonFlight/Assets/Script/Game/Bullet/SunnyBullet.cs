﻿using UnityEngine;
using System.Collections;

public class SunnyBullet : Bullet {

    void BulletFire() { StartCoroutine("BulletFireCoroutine"); }
    IEnumerator BulletFireCoroutine()
    {
        while(true)
        {
            if (this.transform.localPosition.y > 1000 || this.transform.localPosition.y < -1900)
            {
                bulletState = BulletState.Ready;
                this.transform.localPosition = new Vector3(0, -2000f, 0);
                StopCoroutine("BulletFireCoroutine");
            }
            else
            {
                bulletState = BulletState.Fire;
                this.transform.localPosition += new Vector3(0, bulletSpeed, 0) * Time.deltaTime;
            }
            yield return null;
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Monster")
        {
            coll.gameObject.SendMessage("Hit", damege);
            this.transform.localPosition = new Vector3(0, -2000f, 0);
        }
    }
}
