﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

    public UISprite background1;
    public UISprite background2;
    public UISprite background3;

    // Use this for initialization
    void Start () {
        StartCoroutine(MyUpdate());
	}
	
    IEnumerator MyUpdate()
    {
        float speed = 500f;
        Vector3 speedVector = new Vector3(0, speed, 0);
        while(true)
        {
            background1.transform.localPosition -= speedVector * Time.deltaTime;
            background2.transform.localPosition -= speedVector * Time.deltaTime;
            background3.transform.localPosition -= speedVector * Time.deltaTime;

            if (background1.transform.localPosition.y < -1700)
                background1.transform.localPosition = new Vector3(0, 2300f, 0);
            if (background2.transform.localPosition.y < -1700)
                background2.transform.localPosition = new Vector3(0, 2300f, 0);
            if (background3.transform.localPosition.y < -1700)
                background3.transform.localPosition = new Vector3(0, 2300f, 0);
            yield return null;
        }
    }
}
