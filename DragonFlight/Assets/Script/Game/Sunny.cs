﻿using UnityEngine;
using System.Collections;

public class Sunny : MonoBehaviour {

    Vector2 pastPosition, currentPosition, deltaPosition;
    bool clicking;
    int speed;

	void Start () {
        clicking = false;
        speed = 200;

        StartCoroutine(MyUpdate());
	}

    IEnumerator MyUpdate()
    {
        while(true)
        {
            if(Input.GetMouseButtonDown(0))
            {
                clicking = true;
                pastPosition = currentPosition = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
                    clicking = false;

            if(clicking)
            {
                currentPosition = Input.mousePosition;
                deltaPosition = currentPosition - pastPosition;
                pastPosition = Input.mousePosition;

                if (this.transform.localPosition.x <= -400)
                    this.transform.localPosition = new Vector2(-400, this.transform.localPosition.y);
                if (this.transform.localPosition.x >= 400)
                    this.transform.localPosition = new Vector2(400, this.transform.localPosition.y);

                if ((this.transform.localPosition.x > -400 && this.transform.localPosition.x < 400)     // -400 ~ 400 까지 이동가능
                    || (this.transform.localPosition.x <= -400 && deltaPosition.x > 0)                  // 또는 -400 이하의 경우엔 data.x가 양수인 경우 이동가능
                    || (this.transform.localPosition.x >= 400 && deltaPosition.x < 0)                   // 또는 400 이상의 경우엔 data.x가 음수인 경우 이동가능
                    )
                    this.transform.localPosition += new Vector3(deltaPosition.x, 0, 0) * speed * Time.deltaTime;
            }
            yield return null;
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Monster"))
        {
            print("너님 드래곤에 맞아 죽었음!!");
        }
    }
}
