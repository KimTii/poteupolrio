﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    void Start()
    {
        GetComponent<GameInitialization>().SunnyInitialization();
        GetComponent<GameInitialization>().HatchlingInitialization();
    }
}
