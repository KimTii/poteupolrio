﻿using UnityEngine;
using System.Collections;

public class Meteorite : MonoBehaviour {

    enum MeteoriteState
    {
        Stay, Move, Follow
    }

    public ParticleSystem warmLine;
    public UISprite meteorite;
    public UISprite exclamationMark;
    public UISprite fire;

    void Start () {
        exclamationMark.enabled = false;

        StartCoroutine("TestMeteorite");
    }

    IEnumerator TestMeteorite()
    {
        while(true)
        {
            MeteoriteIsComming();
            yield return new WaitForSeconds(5);
            StopCoroutine("MeteoriteCoroutine");

            MeteoriteIsComming(MeteoriteState.Move);
            yield return new WaitForSeconds(5);
            StopCoroutine("MeteoriteCoroutine");

            MeteoriteIsComming(MeteoriteState.Follow);
            yield return new WaitForSeconds(5);
            StopCoroutine("MeteoriteCoroutine");
        }
    }
    void MeteoriteIsComming(MeteoriteState meteoriteState = MeteoriteState.Stay)
    {
        StartCoroutine("MeteoriteCoroutine", meteoriteState);
    }
    IEnumerator MeteoriteCoroutine(MeteoriteState meteoriteState)
    {
        float addScale = 0.01f;
        int meteoriteRotationSpeed = 3;
        int meteoriteMoveSpeed = 15;
        int originalWarmLineSpeed = Random.Range(-2, 2);
        int warmLineSpeed = originalWarmLineSpeed;
        float deltaX;

        Transform sunnyTransform= GameObject.Find("Sunny").transform;
        Transform warmLineTransform = warmLine.transform;
        Transform fireTransform = fire.transform;
        Transform exclamationMarkTransform = exclamationMark.transform;

        ResetMeteorite();

        // Particle Play
        warmLine.Play();
        // Exclamation Mark Enable True
        exclamationMark.enabled = true;
        while (true)
        {
            // Exclamtion Mark Animation Play
            if (exclamationMark.transform.localScale.x > 1.2f || exclamationMark.transform.localScale.x < 1f)
                addScale *= -1;
            exclamationMark.transform.localScale += new Vector3(addScale, addScale, 0);

            // Meteorite Animation Play
            meteorite.transform.Rotate(new Vector3(0, 0, meteoriteRotationSpeed));

            // Meteorite Move Start & Exclamation Mark Move
            fireTransform.localPosition += new Vector3(0, -meteoriteMoveSpeed, 0);
            fireTransform.localPosition = new Vector3(warmLineTransform.localPosition.x, fireTransform.localPosition.y, fireTransform.localPosition.z);
            exclamationMarkTransform.localPosition = new Vector3(warmLineTransform.localPosition.x, exclamationMarkTransform.localPosition.y, exclamationMarkTransform.localPosition.z);

            if (meteoriteState == MeteoriteState.Move || meteoriteState == MeteoriteState.Follow)
            {
                if (meteoriteState == MeteoriteState.Follow)
                {
                    // Set Warm Line Speed
                    deltaX = sunnyTransform.localPosition.x - warmLineTransform.localPosition.x;
                    if (deltaX == 0)
                        warmLineSpeed = 0;
                    else if (deltaX < 0)
                    {
                        warmLineSpeed = originalWarmLineSpeed;
                        if (warmLineSpeed > 0)
                            warmLineSpeed *= -1;
                    }
                    else
                    {
                        warmLineSpeed = originalWarmLineSpeed;
                        if (warmLineSpeed < 0)
                            warmLineSpeed *= -1;
                    }
                }
                    // Warm Line Move
                    warmLineTransform.localPosition += new Vector3(warmLineSpeed, 0, 0);
                    if (warmLineTransform.localPosition.x < -450 || warmLineTransform.localPosition.x > 450)
                        warmLineSpeed = 0;
            }

            if (fireTransform.localPosition.y < -800)
            {
                warmLine.Stop();
                warmLine.Clear();
                exclamationMark.enabled = false;
            }
            if (fireTransform.localPosition.y < -2000)
                StopCoroutine("MeteoriteCoroutine");
                
            yield return null;
        }
    }
    void ResetMeteorite()
    {
        warmLine.Stop();
        warmLine.Clear();
        fire.transform.localPosition = new Vector3(0, 2000, 0);
        warmLine.transform.localPosition = new Vector3(Random.Range(-450, 450), 0, -10);
        exclamationMark.enabled = false;
    }
}
