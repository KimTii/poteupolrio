﻿using UnityEngine;
using System.Collections;

public class Dragon : MonoBehaviour {

    enum MonsterState
    {
        // Idle     기본 이동 상태
        // Dameged  공격 받은 상태
        // Die      죽은 상태
        // Ready    대기 장소에서 기다리는 상태
        Idle, Dameged, Die, Ready
    }

    UISprite leftWing, rightWing;
    ParticleSystem dieParticle;

    int originalHp;
    int hp;
    MonsterState state;

    bool isHitCoroutineRunning;         // Hit Coroutine 이 현재 돌고 있는지 체크하는 bool 변수

    void Awake () {
        leftWing = rightWing = new UISprite();
        dieParticle = new ParticleSystem();
        leftWing = this.transform.FindChild("Dragon_01_LeftWing").GetComponent<UISprite>();
        rightWing = this.transform.FindChild("Dragon_01_RightWing").GetComponent<UISprite>();
        dieParticle = this.transform.FindChild("DragonDieParticle").GetComponent<ParticleSystem>();

        originalHp = 1;
        hp = originalHp;
        state = MonsterState.Ready;

        isHitCoroutineRunning = false;

        //StartCoroutine("MyUpdate");
	}

    IEnumerator MyUpdate()
    {
        int speed = 500;
        int dropProbability;

        while(true)
        {
            if (hp <= 0 && state != MonsterState.Ready)
                state = MonsterState.Die;

            if (transform.localPosition.y < 1000)
                this.GetComponent<BoxCollider>().enabled = true;
            if (transform.localPosition.y < -1100)
                state = MonsterState.Ready;

            //print(state);
            // -----------------------------------------------------------------------------------------------------
            // State Pattern----------------------------------------------------------------------------------------
            if (state == MonsterState.Idle || state == MonsterState.Dameged)
            {
                this.transform.localPosition -= new Vector3(0, speed, 0) * Time.deltaTime;
            }
            else if(state == MonsterState.Die)
            {
                // Sprite Off
                this.GetComponent<UISprite>().enabled = false;
                leftWing.enabled = false;
                rightWing.enabled = false;
                // Particle
                dieParticle.Play();
                state = MonsterState.Ready;

                // Score Plus
                GameObject.Find("UI").SendMessage("ScorePlus", 100);
                // Create Coin
                // 여기서는 확율 계산을 이용해 보석을 생성해야한다. 추후 구현합시다.
                dropProbability = Random.Range(1, 10);
                if(dropProbability <= 5)
                    GameObject.Find("UI/ItemUI").SendMessage("CreateCoin", transform.localPosition);
                else if(dropProbability <= 8)
                    GameObject.Find("UI/ItemUI").SendMessage("CreateGem01", transform.localPosition);
                else if (dropProbability <= 10)
                    GameObject.Find("UI/ItemUI").SendMessage("CreateDualshot", transform.localPosition);
            }
            else if(state == MonsterState.Ready)
            {
                this.GetComponent<BoxCollider>().enabled = false;
                yield return new WaitForSeconds(1);
                this.transform.localPosition = new Vector2(0, 1500);
                // Particle
                dieParticle.Stop();
                StopCoroutine("MyUpdate");
                yield return null;
            }
            else
            {
                print("MONSTER MyUpdate ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }

            
            // State Pattern----------------------------------------------------------------------------------------
            // -----------------------------------------------------------------------------------------------------
            yield return null;
        }
    }

    public void Hit(int damege)
    {
        hp -= damege;
        state = MonsterState.Dameged;
        if (isHitCoroutineRunning)
            StopCoroutine("HitCoroutine");
        StartCoroutine("HitCoroutine");

    }
    IEnumerator HitCoroutine()
    {
        isHitCoroutineRunning = true;
        GetComponent<UISprite>().spriteName = "Dragon_01_Dameged";
        yield return new WaitForSeconds(0.5f);
        GetComponent<UISprite>().spriteName = "Dragon_01_Idle";
        isHitCoroutineRunning = false;
    }
    public void TurnOnDragon()
    {
        this.GetComponent<BoxCollider>().enabled = false;
        hp = originalHp;
        // Sprite Off
        this.GetComponent<UISprite>().enabled = true;
        leftWing.enabled = true;
        rightWing.enabled = true;
        
        state = MonsterState.Idle;
        StartCoroutine("MyUpdate");
    }
}
