﻿using UnityEngine;
using System.Collections;

public class GameInitialization : MonoBehaviour {

    public UISprite sSunnyBody;
    public UISprite sSunnyLeftWing;
    public UISprite sSunnyRightWing;

    public UISprite sHatchling1Body;
    public UISprite sHatchling1LeftWing;
    public UISprite sHatchling1RightWing;

    public UISprite sHatchling2Body;
    public UISprite sHatchling2LeftWing;
    public UISprite sHatchling2RightWing;

    public void SunnyInitialization()
    {
        // 1. 캐릭터 초기화
        //  - 캐릭터의 Sprite 변경(UserData와 동기화)
        //  - 캐릭터의 Bullet 변경(UserData와 동기화)
        int characterNumber = UserData.Instance.userInformation.GetSelectedCharacter();
    }
    public void HatchlingInitialization()
    {
        // 2. 새끼용 초기화
        //  - 새끼용 Sprite 변경(UserData와 동기화)
        //  - 새끼용의 Type과 Element에 따라 Bullet 초기화
        int hatchling1Number = UserData.Instance.userInformation.GetHatchling1Number();
        int hatchling2Number = UserData.Instance.userInformation.GetHatchling2Number();
        string hatchling1InGameName = DB.Instance.GetHatchlingInGameName(hatchling1Number);
        hatchling1InGameName = hatchling1InGameName.TrimEnd();
        string hatchling2InGameName = DB.Instance.GetHatchlingInGameName(hatchling2Number);
        hatchling2InGameName = hatchling2InGameName.TrimEnd();
        sHatchling1Body.spriteName = hatchling1InGameName + "_01";
        sHatchling1LeftWing.spriteName = sHatchling1RightWing.spriteName = hatchling1InGameName + "_02";
        sHatchling2Body.spriteName = hatchling2InGameName + "_01";
        sHatchling2LeftWing.spriteName = sHatchling2RightWing.spriteName = hatchling2InGameName + "_02";

        // 3. UI 초기화
        //  - 하단 2종의 아이템 초기화(UserData와 동기화)
    }
}
