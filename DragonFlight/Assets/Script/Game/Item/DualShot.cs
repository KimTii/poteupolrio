﻿using UnityEngine;
using System.Collections;

public class DualShot : Item {

    public int dualshotSecond;
    Items items;
    GameObject bulletPoint;
    GameObject dualshotObj;

    void Start()
    {
        items = GameObject.Find("UI/ItemUI").GetComponent<Items>();
        bulletPoint = items.GetBulletPoint();
        dualshotObj = items.GetDualshotObject();
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("Player") && !dualshotObj.activeInHierarchy)
        {
            Reset();
            StartCoroutine("DualShotCoroutine");
        }
    }
    IEnumerator DualShotCoroutine()
    {
        bulletPoint.transform.localPosition = new Vector3(-50, 200, 0);
        dualshotObj.SetActive(true);
        dualshotObj.SendMessage("Wakeup");
        yield return new WaitForSeconds(dualshotSecond);
        bulletPoint.transform.localPosition = new Vector3(0, 200, 0);
        dualshotObj.SetActive(false);
    }
}
