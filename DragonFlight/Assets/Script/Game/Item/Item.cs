﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    float x, y, yPower;

    void Start () {
        x = y = 0;
        yPower = 3;
    }

    public void EnableItem()
    {
        StartCoroutine("Animation");
    }
    IEnumerator Animation()
    {
        x = Random.Range(-5, 5);
        while (true)
        {
            if (this.transform.localPosition.y < -1100)
                Reset();
            if (this.transform.localPosition.x <= -500 || this.transform.localPosition.x >= 500)
                x *= -1;
                

            y += yPower;
            this.transform.localPosition += new Vector3(x, y, 0);

            yPower -= 0.5f;
            if (yPower < -1)
                yPower = -1;
            yield return null;
        }
    }
    public void Reset()
    {
        StopCoroutine("Animation");
        this.transform.localPosition = new Vector3(700, 1000);

        x = y = 0;
        yPower = 3;
    }
}
