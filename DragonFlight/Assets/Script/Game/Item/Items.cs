﻿using UnityEngine;
using System.Collections;

public class Items : MonoBehaviour {

    class ItemManager {
        int maxCount;
        GameObject[] items;
        GameObject item;
        int index;

        public ItemManager() { }
        public ItemManager(int _maxCount, string path)
        {
            maxCount = _maxCount;
            items = new GameObject[maxCount];
            index = 0;

            item = Resources.Load(path) as GameObject;
        }
        public GameObject[] GetItems() { return items; }
        public GameObject GetItem() { return item; }
        public int GetMaxCount() { return maxCount; }
        public int GetIndex() { return index; }

        public void PlusIndex(int number = 1) {
            ++index;

            if (index >= maxCount)
                index = 0;
        }
    }

    ItemManager coin;
    ItemManager gem01;
    ItemManager dualshot;
    GameObject bulletPoint;
    GameObject dualshotObj;

    void Start () {
        coin = new ItemManager(10, "Prefab/Coin");
        ArrayInitialization(coin.GetItems(), coin.GetItem(), coin.GetMaxCount());

        gem01 = new ItemManager(10, "Prefab/Gem01");
        ArrayInitialization(gem01.GetItems(), gem01.GetItem(), gem01.GetMaxCount());

        dualshot = new ItemManager(5, "Prefab/DualShot");
        ArrayInitialization(dualshot.GetItems(), dualshot.GetItem(), dualshot.GetMaxCount());
        bulletPoint = GameObject.Find("Sunny/BulletPoint");
        dualshotObj = Instantiate(bulletPoint, GameObject.Find("Sunny").transform, false) as GameObject;
        dualshotObj.transform.localPosition = new Vector3(50, 200, 0);
        dualshotObj.SetActive(false);
    }

    void ArrayInitialization(GameObject[] array, GameObject gameObject, int MAX_COUNT)
    {
        for (int i = 0; i < MAX_COUNT; ++i)
        {
            array[i] = Instantiate(gameObject, this.transform, false) as GameObject;
            array[i].transform.localPosition = new Vector3(700, 1000, 0);
        }
    }

    void CreateCoin(Vector3 position) { CreateItem(coin, position); }
    void CreateGem01(Vector3 position) { CreateItem(gem01, position); }
    void CreateDualshot(Vector3 position) { CreateItem(dualshot, position); }
    void CreateItem(ItemManager itemManager, Vector3 position)
    {
        GameObject[] items = itemManager.GetItems();
        int index = itemManager.GetIndex();
        int maxIndex = itemManager.GetMaxCount();

        items[index].GetComponent<UISprite>().enabled = true;
        items[index].transform.localPosition = position;
        items[index].SendMessage("EnableItem");
        itemManager.PlusIndex();
    }

    public GameObject GetBulletPoint() { return bulletPoint; }
    public GameObject GetDualshotObject() { return dualshotObj; }
}
