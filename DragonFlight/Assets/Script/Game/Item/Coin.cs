﻿using UnityEngine;
using System.Collections;

public class Coin : Item
{
    public int addCoin;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player"))
        {
            Reset();
            GameObject.Find("UI").SendMessage("CoinPlus", addCoin);
        }
    }
}
