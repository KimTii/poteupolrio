﻿using UnityEngine;
using System.Collections;

public class HomeManager : MonoBehaviour {

	void Start () {
        // DB Load
        DB.Instance.AllDBLoad();

        // User Data Initialization
        UserData.Instance.CharacterInitialization();
        
        // Scene Load
        GetComponent<CharacterSelect>().CharacterNumbersUpdate();
        GetComponent<HatchlingWindow>().HatchlingWindowUpdate();

        // User Data Load
        //UserData.Instance.LoadData();

        //UserData.Instance.userInformation.PrintAll();

        //PlayerPrefs.DeleteAll();
        //UserData.Instance.SaveData();
    }
}
