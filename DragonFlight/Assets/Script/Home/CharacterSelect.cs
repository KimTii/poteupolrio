﻿using UnityEngine;
// try&catch 때문에 TEST 용도
using System;
using System.Collections;
using System.Collections.Generic;

public class CharacterSelect : MonoBehaviour {

    public UILabel lCharacterTitle;
    public UILabel lCharacterName;
    public UISprite sCharacterSelecIcon;
    public UISprite sCharacterIllust;
    // Bullet
    public UILabel lBulletLevel;
    public UISprite sBullet;

    // UnActive & Active
    public UIPanel pActive;
    public UIPanel pUnActiveShadow;
    public UISprite sUnActiveIcon;
    public UISprite sElement;
    public UILabel lCost;
    

    int[] numbers;
    int currentCharacterIndex;

    void Start()
    {
        currentCharacterIndex = 0;
    }

    public void CharacterNumbersUpdate()
    {
        numbers = new int[] { 11, 21, 31, 41, 51, 61, 71, 81, 91, 101, 111, 121, 131, 141, 151 };
        if (UserData.Instance.userInformation.GetCharacterEnable(12))
            numbers[0] = 12;
        else if (UserData.Instance.userInformation.GetCharacterEnable(13))
            numbers[0] = 13;

        if (UserData.Instance.userInformation.GetCharacterEnable(22))
            numbers[1] = 22;
        else if (UserData.Instance.userInformation.GetCharacterEnable(23))
            numbers[1] = 23;

        if (UserData.Instance.userInformation.GetCharacterEnable(32))
            numbers[2] = 32;
        else if (UserData.Instance.userInformation.GetCharacterEnable(33))
            numbers[2] = 33;

        if (UserData.Instance.userInformation.GetCharacterEnable(42))
            numbers[3] = 42;
        else if (UserData.Instance.userInformation.GetCharacterEnable(43))
            numbers[3] = 43;

        if (UserData.Instance.userInformation.GetCharacterEnable(52))
            numbers[4] = 52;
        else if (UserData.Instance.userInformation.GetCharacterEnable(53))
            numbers[4] = 53;

        if (UserData.Instance.userInformation.GetCharacterEnable(152))
            numbers[14] = 152;
        else if (UserData.Instance.userInformation.GetCharacterEnable(153))
            numbers[14] = 153;
    }

    public void LeftRightButton(GameObject obj)
    {
        if (obj.name == "Right")
            ++currentCharacterIndex;
        else
            --currentCharacterIndex;

        if (currentCharacterIndex < 0)
            currentCharacterIndex = numbers.Length - 1;
        if (currentCharacterIndex > numbers.Length - 1)
            currentCharacterIndex = 0;

        CharacterSelectUpdate();
    }
    public void CharacterSelectUpdate()
    {
        int currentCharacterNumber = numbers[currentCharacterIndex];
        string bulletName = null;

        lCharacterTitle.text = DB.Instance.GetTitle(currentCharacterNumber);
        lCharacterName.text = DB.Instance.GetName(currentCharacterNumber);
        sCharacterSelecIcon.spriteName = DB.Instance.GetSelectIconName(currentCharacterNumber);
        sCharacterIllust.spriteName = DB.Instance.GetIllustName(currentCharacterNumber);
        sCharacterIllust.transform.localPosition = GetComponent<HomeUI>().SettingCharacterIllustPosition(currentCharacterNumber);


        if (UserData.Instance.userInformation.GetCharacterEnable(currentCharacterNumber))
        {
            // Bullet
            try
            {
                pUnActiveShadow.enabled = false;
                sUnActiveIcon.enabled = false;
                lCost.enabled = false;
                lBulletLevel.enabled = true;
                sBullet.enabled = true;
                sElement.enabled = true;
                pActive.enabled = true;

                lBulletLevel.text = UserData.Instance.userInformation.GetBulletLevel(currentCharacterNumber).ToString();
                bulletName = DB.Instance.GetWeaponName(currentCharacterNumber, System.Convert.ToInt32(lBulletLevel.text));
                bulletName = bulletName.TrimEnd();
                sBullet.spriteName = bulletName;
            }
            catch (Exception e)
            {
                print("Bullet Sprite or Bullet Level 을 불러올 수 없습니다.");
            }
        }
        else
        {
            lBulletLevel.enabled = false;
            sBullet.enabled = false;
            sElement.enabled = false;
            pActive.enabled = false;
            pUnActiveShadow.enabled = true;
            sUnActiveIcon.enabled = true;
            lCost.enabled = true;

            sUnActiveIcon.spriteName = DB.Instance.GetIconName(currentCharacterNumber);
            lCost.text = DB.Instance.GetCost(currentCharacterNumber).ToString();
        }
    }
}
