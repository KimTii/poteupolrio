﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class HomeUI : MonoBehaviour {
    public UILabel lUserID;
    public UILabel lUserCoin;
    public UILabel lUserGem;
    public UILabel lUserBestScore;

    public UISprite sCharacterIllust;
    public UISprite sCharacterSelectIcon;
    public UISprite sHatchling1;
    public UISprite sHatchling2;

    public UIPanel pLobby;
    public UIPanel pCharacterSelect;
    public UIPanel pInventory;
    public UIPanel pHatchling;
    public UIPanel pShop;
    public UIPanel pLoading;

    bool gameStart;

    void Start()
    {
        gameStart = false;
        HomeUIUpdate();
        OpenLobby();
    }

    void HomeUIUpdate()
    {
        // ID, Coin, Gem, BestScore
        lUserID.text = UserData.Instance.userInformation.GetID();
        lUserCoin.text = UserData.Instance.userInformation.GetCoin().ToString("n0");
        lUserGem.text = UserData.Instance.userInformation.GetGem().ToString("n0");
        lUserBestScore.text = UserData.Instance.userInformation.GetBestScore().ToString("n0");
        // Illust
        int characterIdentificationNumber = UserData.Instance.userInformation.GetSelectedCharacter();
        string illustName = DB.Instance.GetIllustName(characterIdentificationNumber);
        sCharacterIllust.spriteName = illustName;
        sCharacterIllust.transform.localPosition = SettingCharacterIllustPosition(characterIdentificationNumber);
        // Icon
        string selectIconName = DB.Instance.GetSelectIconName(characterIdentificationNumber);
        sCharacterSelectIcon.spriteName = selectIconName;
        sCharacterSelectIcon.GetComponent<UIButton>().normalSprite = selectIconName;
        // Hatchling1, 2
        UserHatchlingUpdate(sHatchling1, sHatchling2);
    }
    public void UserHatchlingUpdate(UISprite hatchling1, UISprite hatchling2)
    {
        int hatchlingNumber = UserData.Instance.userInformation.GetHatchling1Number();
        string hatchlingName = DB.Instance.GetHatchlingIllustName(hatchlingNumber);
        if (hatchlingName == null)
            hatchling1.gameObject.SetActive(false);
        else
        {
            hatchling1.gameObject.SetActive(true);
            hatchling1.spriteName = hatchlingName;
        }

        hatchlingNumber = UserData.Instance.userInformation.GetHatchling2Number();
        hatchlingName = DB.Instance.GetHatchlingIllustName(hatchlingNumber);
        if (hatchlingName == null)
            hatchling2.gameObject.SetActive(false);
        else
        {
            hatchling2.gameObject.SetActive(true);
            hatchling2.spriteName = hatchlingName;
        }
    }
    
    public void OpenLobby()
    {
        HomeUIUpdate();

        pLobby.transform.localPosition =            Vector3.zero;
        pCharacterSelect.transform.localPosition =  new Vector3(-1200, -2000, 0);
        pInventory.transform.localPosition =        new Vector3(0, -2000, 0);
        pHatchling.transform.localPosition =        new Vector3(1200, -2000, 0);
        pShop.transform.localPosition =             new Vector3(2400, -2000, 0);
    }
    public void OpenCharacterSelect()
    {
        GetComponent<CharacterSelect>().CharacterSelectUpdate();

        pLobby.transform.localPosition =            new Vector3(-2400, -2000, 0);
        pCharacterSelect.transform.localPosition =  Vector3.zero;
        pInventory.transform.localPosition =        new Vector3(0, -2000, 0);
        pHatchling.transform.localPosition =        new Vector3(1200, -2000, 0);
        pShop.transform.localPosition =             new Vector3(2400, -2000, 0);
    }
    public void OpenInventory()
    {
        pLobby.transform.localPosition =            new Vector3(-2400, -2000, 0);
        pCharacterSelect.transform.localPosition =  new Vector3(-1200, -2000, 0);
        pInventory.transform.localPosition =        Vector3.zero;
        pHatchling.transform.localPosition =        new Vector3(1200, -2000, 0);
        pShop.transform.localPosition =             new Vector3(2400, -2000, 0);
    }
    public void OpneHatchling()
    {
        GetComponent<HatchlingWindow>().HatchlingWindowUpdate();

        pLobby.transform.localPosition =            new Vector3(-2400, -2000, 0);
        pCharacterSelect.transform.localPosition =  new Vector3(-1200, -2000, 0);
        pInventory.transform.localPosition =        new Vector3(0, -2000, 0);
        pHatchling.transform.localPosition =        Vector3.zero;
        pShop.transform.localPosition =             new Vector3(2400, -2000, 0);
    }
    public void OpenShop()
    {
        pLobby.transform.localPosition =            new Vector3(-2400, -2000, 0);
        pCharacterSelect.transform.localPosition =  new Vector3(-1200, -2000, 0);
        pInventory.transform.localPosition =        new Vector3(0, -2000, 0);
        pHatchling.transform.localPosition =        new Vector3(-1200, -2000, 0);
        pShop.transform.localPosition =             Vector3.zero;
    }

    public Vector3 SettingCharacterIllustPosition(int number)
    {
        // 캐릭터 일러스트 Position 이동 ㅜㅜ (일러스트 해상도도 다 다르고.. 원하는 대로 하기 위함)
        switch (number)
        {
            case 11: return Vector3.zero;
            case 12: return Vector3.zero;
            case 13: return new Vector3(-150, 0, 0);
            case 21: return new Vector3(260, -240, 0);
            case 22: return new Vector3(150, -300, 0);
            case 23: return new Vector3(0, -150, 0);
            case 31: return new Vector3(0, -200, 0);
            case 32: return new Vector3(220, -100, 0);
            case 33: return new Vector3(150, -220, 0);
            case 41: return new Vector3(80, -400, 0);
            case 42: return new Vector3(100, -370, 0);
            case 43: return new Vector3(200, -130, 0);
            case 51: return Vector3.zero;
            case 52: return new Vector3(100, -150, 0);
            case 53: return new Vector3(0, -60, 0);
            case 61: return new Vector3(170, -200, 0); 
            case 71: return Vector3.zero; 
            case 81: return new Vector3(200, -200, 0); 
            case 91: return new Vector3(150, -250, 0); 
            case 101: return new Vector3(-20, -350, 0); 
            case 111: return new Vector3(0, -120, 0); 
            case 121: return new Vector3(0, -200, 0); 
            case 131: return new Vector3(0, -200, 0); 
            case 141: return new Vector3(260, -200, 0); 
            case 151: return new Vector3(200, -150, 0);
            case 152: return new Vector3(-100, -100, 0);
            case 153: return new Vector3(250, -150, 0);

            default: return Vector3.one;
        }
    }
    public void GameReadyButton()
    {
        /*
        if (gameStart)
            return;
        gameStart = true;

        SceneManager.LoadScene(1);
        */
        StartCoroutine("GameReadyCoroutine");
    }

    IEnumerator GameReadyCoroutine()
    {
        pLoading.transform.localPosition = Vector3.zero;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadSceneAsync(1);

        yield return null;
    }
   
    public void XButton(GameObject obj)
    {
        obj.transform.parent.transform.localPosition = new Vector3(0, -2000, 0);
    }
}
