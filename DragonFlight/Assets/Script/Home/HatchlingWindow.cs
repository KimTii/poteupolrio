﻿using UnityEngine;
using System.Collections;

public class HatchlingWindow : MonoBehaviour {

    public UISprite sSelectedHatchling1;
    public UISprite sSelectedHatchling1Grade;
    public UILabel lSelectedHatchling1Level;
    public UISprite sHatchlingBody1;
    public UISprite sHatchlingWingLeft1;
    public UISprite sHatchlingWingright1;

    public UISprite sSelectedHatchling2;
    public UISprite sSelectedHatchling2Grade;
    public UILabel lSelectedHatchling2Level;
    public UISprite sHatchlingBody2;
    public UISprite sHatchlingWingLeft2;
    public UISprite sHatchlingWingright2;

    public UIPanel pHatchlingInfomationWindow;
    public UISprite sHatchlingIllust;
    public UISprite sHatchlingGrade;
    public UILabel lHatchlingName;
    public UILabel lHatchlingLevel;

    public void HatchlingWindowUpdate()
    {
        // UserData의 Hatchling Update
        int hatchling1Number, hatchling2Number;
        hatchling1Number = hatchling2Number = 0;
        hatchling1Number = UserData.Instance.userInformation.GetHatchling1Number();
        hatchling2Number = UserData.Instance.userInformation.GetHatchling2Number();

        // Set Illust
        GetComponent<HomeUI>().UserHatchlingUpdate(sSelectedHatchling1, sSelectedHatchling2);

        // Set Grade
        SetGrade(hatchling1Number, sSelectedHatchling1Grade);
        SetGrade(hatchling2Number, sSelectedHatchling2Grade);

        // SetLevel
        lSelectedHatchling1Level.text = UserData.Instance.userInformation.GetHatchling1Level().ToString();
        lSelectedHatchling2Level.text = UserData.Instance.userInformation.GetHatchling2Level().ToString();

        // SetSprite
        string hatchling1InGameName = DB.Instance.GetHatchlingInGameName(hatchling1Number);
        hatchling1InGameName = hatchling1InGameName.TrimEnd();
        string hatchling2InGameName = DB.Instance.GetHatchlingInGameName(hatchling2Number);
        hatchling2InGameName = hatchling2InGameName.TrimEnd();
        sHatchlingBody1.spriteName = hatchling1InGameName + "_01";
        sHatchlingWingLeft1.spriteName = sHatchlingWingright1.spriteName = hatchling1InGameName + "_02";
        sHatchlingBody2.spriteName = hatchling2InGameName + "_01";
        sHatchlingWingLeft2.spriteName = sHatchlingWingright2.spriteName = hatchling2InGameName + "_02";
        // ----------------------------------------------------------------------------------------------------------

        // 보유중인 Hatchling Update
        int[] hatchlingNumbers = UserData.Instance.userInformation.GetAllHatchlingNumber();
        for(int i = 0; i < hatchlingNumbers.Length; ++i)
        {

        }

        // ----------------------------------------------------------------------------------------------------------
    }
    void SetGrade(int hatchlingNumber, UISprite sHatchlingGrade)
    {
        int grade = DB.Instance.GetHatchlingGrade(hatchlingNumber);
        if (grade == 1) {
            sHatchlingGrade.spriteName = "grade_mark1";
            sHatchlingGrade.SetDimensions(57, 50);
        }
        else if (grade == 2) {
            sHatchlingGrade.spriteName = "grade_mark2";
            sHatchlingGrade.SetDimensions(107, 50);
        }
        else if (grade == 3) {
            sHatchlingGrade.spriteName = "grade_mark3";
            sHatchlingGrade.SetDimensions(157, 50);
        }
        else
            print("SetGrade() ERROR!!");
    }

    public void HatchlingSelectButton(GameObject obj)
    {
        print("HatchlingSelectButton : " + obj.name);
        HatchlingInfomationWindow(true, System.Convert.ToInt32(obj.name));
    }
    void HatchlingInfomationWindow(bool open, int hatchlingNumber = 0)
    {
        if(open) {
            HatchlingInfomationWindowUpdate(hatchlingNumber);
            pHatchlingInfomationWindow.transform.localPosition = Vector3.zero;
        }
        else {
            pHatchlingInfomationWindow.transform.localPosition = new Vector3(0, -2000, 0); }
    }
    void HatchlingInfomationWindowUpdate(int hatchlingNumber)
    {
        // 일러스트
        // sHatchlingIllust
        sHatchlingIllust.spriteName = DB.Instance.GetHatchlingIllustName(hatchlingNumber);
        // 그레이드
        // sHatchlingGrade
        SetGrade(hatchlingNumber, sHatchlingGrade);
        // 이름
        // lHatchlingName
        lHatchlingName.text = DB.Instance.GetHatchlingName(hatchlingNumber);
        // 레벨
        // lHatchlingLevel
        lHatchlingLevel.text = "레벨 " + UserData.Instance.userInformation.GetHatchlingLevel(hatchlingNumber).ToString();
    }
}

