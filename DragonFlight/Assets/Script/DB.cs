﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DB : MonoBehaviour {

    private static DB instance = null;
    public static DB Instance
    {
        get
        {
            if (instance == null)
                Debug.LogError("Singleturn instance == null");
            return instance;
        }
    }

    public enum HATCHLING_TYPE
    {
        AUTOMATIC,  // 연사형
        BREATH,     // 브레스형
        CHANGE,     // 교체형
        CHARGE,     // 돌격형
        COPY,       // 복사형
        DEFENSE,    // 방어형
        EVENT,      // 이벤트
        EXPLOSION,  // 폭발형
        FLAME,      // 화염형
        FREEZING,   // 결빙형
        GUIDANCE,   // 유도형
        LASER,      // 레이저형
        LEARNING,   // 학습형
        MULTIPLE,   // 다속성형
        PIERCE,     // 관통형
        POISON,     // 부식형(독형)
        POWERSHOT,  // 파워샷형
        PRODUCTION, // 생산형
        RECHARGE,   // 충전형
        SHOCK,      // 전격형
        SLOW,       // 슬로우형
        SPIRIT,     // 정령형
        WING        // 비행단형
    }
    public enum ELEMENT
    {
        NONE,           // 무속성
        WATER,          // 수속성
        THUNDER,        // 전기속성
        FIRE,           // 화속성
        DARK            // 암흑속성
    }



    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }

    public void AllDBLoad()
    {
        CharacterDBLoad();
        WaeponDBLoad();
        HatchlingDBLoad();
    }

    class CharacterDB
    {
        int characterIdentificationNumber;
        string title;
        string name;
        string iconName;
        string selectIconName;
        string illustName;
        int bullet;
        int cost;

        public CharacterDB() { }
        public CharacterDB(int _number, string _title, string _name, string _iconName, string _selectIconName, string _illustName, int _bullet, int _cost)
        {
            characterIdentificationNumber = _number;
            title = _title;
            name = _name;
            iconName = _iconName;
            selectIconName = _selectIconName;
            illustName = _illustName;
            bullet = _bullet;
            cost = _cost;
        }
        public int GetCharacterIdentificationNumber() { return characterIdentificationNumber; }
        public string GetTitle() { return title; }
        public string GetName() { return name; }
        public string GetIconName() { return iconName; }
        public string GetSelecIconName() { return selectIconName; }
        public string GetIllustName() { return illustName; }
        public int GetCost() { return cost; }
    }
    Dictionary<int, CharacterDB> characterDB;
    public int[] GetCharacterIdentificationNumbers()
    {
        int[] numbers = new int[characterDB.Count];
        int index = 0;
        foreach(KeyValuePair<int, CharacterDB> member in characterDB)
        {
            numbers[index] = member.Key;
            ++index;
        }
        return numbers;
    }
    public string GetTitle(int characterNumber) { return characterDB[characterNumber].GetTitle(); }
    public string GetName(int characterNumber) { return characterDB[characterNumber].GetName(); }
    public string GetIconName(int characterNumber) { return characterDB[characterNumber].GetIconName(); }
    public string GetSelectIconName(int characterNumber) { return characterDB[characterNumber].GetSelecIconName(); }
    public string GetIllustName(int characterNumber) { return characterDB[characterNumber].GetIllustName(); }
    public int GetCost(int characterNumber) { return characterDB[characterNumber].GetCost(); }
    void CharacterDBLoad()
    {
        characterDB = new Dictionary<int, CharacterDB>();
        int number, bullet, cost;
        string title, name, iconName, selectIconName, illustName;
        number = bullet = cost = 0;
        title = name = iconName = selectIconName = illustName = null;

        // File Load
        TextAsset txtFile = (TextAsset)Resources.Load("DB/CharacterDB") as TextAsset;
        string fileFullPath = txtFile.text;
        string[] stringArr = fileFullPath.Split('\n');
        string[] data;
        for(int index = 1; index < stringArr.Length; ++index)
        {
            data = stringArr[index].Split(',');
            for(int i = 0; i < data.Length; ++i)
            {
                switch (i)
                {
                    case 0: number = System.Convert.ToInt32(data[i]); break;
                    case 1: title = data[i]; break;
                    case 2: name = data[i]; break;
                    case 3: iconName = data[i]; break;
                    case 4: selectIconName = data[i]; break;
                    case 5: illustName = data[i]; break;
                    case 6: bullet = System.Convert.ToInt32(data[i]); break;
                    case 7: cost = System.Convert.ToInt32(data[i]); break;
                    default: print("Switch Default : 이상상태 발생"); break;
                }
            }
            characterDB[number] = new CharacterDB(number, title, name, iconName, selectIconName, illustName, bullet, cost);
        }
        print("Character Load Finish!");
    }


    class WeaponDB
    {
        Dictionary<int, string> weapon;

        public WeaponDB() { weapon = new Dictionary<int, string>(); }

        public void AddWeapon(int level, string path) { weapon[level] = path; }
        public string GetWeaponName(int level) { return weapon[level]; }
        // TEST
        //public Dictionary<int, string> GetWeapon() { return weapon; }
    }
    Dictionary<int, WeaponDB> weaponDB;
    public string GetWeaponName(int number, int level) { return weaponDB[number].GetWeaponName(level); }
    void WaeponDBLoad()
    {
        weaponDB = new Dictionary<int, WeaponDB>();
        int number, level;
        string path = null;
        number = level = 0;

        // File Load
        TextAsset txtFile = (TextAsset)Resources.Load("DB/WeaponDB") as TextAsset;
        string fileFullPath = txtFile.text;
        string[] stringArr = fileFullPath.Split('\n');
        string[] data;
        for (int index = 1; index < stringArr.Length; ++index)
        {
            data = stringArr[index].Split(',');
            for (int i = 0; i < data.Length; ++i)
            {
                switch (i)
                {
                    case 0: number = System.Convert.ToInt32(data[i]); break;
                    case 1: level = System.Convert.ToInt32(data[i]); break;
                    case 2: path = data[i]; break;
                    default: print("Switch Default : 이상상태 발생"); break;
                }
            }            
            try { weaponDB[number].AddWeapon(level, path); }
            catch(Exception e) {
                weaponDB[number] = new WeaponDB();
                weaponDB[number].AddWeapon(level, path);
            }
        }
        print("Weapon Load Finish!");
    }


    class HatchlingDB
    {
        int hatchlingIdentificationNumber;
        string name;
        string illustName;
        int grade;
        HATCHLING_TYPE type;
        ELEMENT element1;
        ELEMENT element2;
        string bulletName;
        string inGameName;

        public HatchlingDB() { }
        public HatchlingDB(int _hatchlingNumber, string _name, string _illustName, int _grade, HATCHLING_TYPE _type, ELEMENT _element1, ELEMENT _element2, string _bulletName, string _inGameName)
        {
            hatchlingIdentificationNumber = _hatchlingNumber;
            name = _name;
            illustName = _illustName;
            grade = _grade;
            type = _type;
            element1 = _element1;
            element2 = _element2;
            bulletName = _bulletName;
            inGameName = _inGameName;
        }
        public string GetName() { return name; }
        public string GetIllustName() { return illustName; }
        public int GetGrade() { return grade; }
        public HATCHLING_TYPE GetType() { return type; }
        public ELEMENT GetElement1() { return element1; }
        public ELEMENT GetElement2() { return element2; }
        public string GetBulletName() { return bulletName; }
        public string GetInGameName() { return inGameName; }
    }
    Dictionary<int, HatchlingDB> hatchlingDB;
    public string GetHatchlingName(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetName(); }
    public string GetHatchlingIllustName(int hatchlingNumber)
    {
        try { return hatchlingDB[hatchlingNumber].GetIllustName(); }
        catch { return null; }
    }
    public int GetHatchlingGrade(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetGrade(); }
    public HATCHLING_TYPE GetHatchlingType(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetType(); }
    public ELEMENT GetHatchlingElement1(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetElement1(); }
    public ELEMENT GetHatchlingElement2(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetElement2(); }
    public string GetHatchlingBulletName(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetBulletName(); }
    public string GetHatchlingInGameName(int hatchlingNumber) { return hatchlingDB[hatchlingNumber].GetInGameName(); }

    void HatchlingDBLoad()
    {
        hatchlingDB = new Dictionary<int, HatchlingDB>();
        int hatchlingNumber, grade;
        string name, illustName, bulletName, inGameName;
        HATCHLING_TYPE type;
        ELEMENT element1;
        ELEMENT element2;
        hatchlingNumber = grade = 0;
        name = illustName = bulletName = inGameName = null;
        type = HATCHLING_TYPE.AUTOMATIC;
        element1 = element2 = ELEMENT.NONE;

        // File Load
        TextAsset txtFile = (TextAsset)Resources.Load("DB/HatchlingDB") as TextAsset;
        string fileFullPath = txtFile.text;
        string[] stringArr = fileFullPath.Split('\n');
        string[] data;
        for (int index = 1; index < stringArr.Length; ++index)
        {
            data = stringArr[index].Split(',');
            for (int i = 0; i < data.Length; ++i)
            {
                switch (i)
                {
                    case 0: hatchlingNumber = System.Convert.ToInt32(data[i]); break;
                    case 1: name = data[i]; break;
                    case 2: illustName = data[i]; break;
                    case 3: grade = System.Convert.ToInt32(data[i]); break;
                    case 4: type = SetHatchlingType(data[i]); break;
                    case 5: element1 = SetHatchlingElement(data[i]); break;
                    case 6: element2 = SetHatchlingElement(data[i]); break;
                    case 7: bulletName = data[i]; break;
                    case 8: inGameName = data[i]; break;
                    default: print("Switch Default : 이상상태 발생"); break;
                }
            }
            hatchlingDB[hatchlingNumber] = new HatchlingDB(hatchlingNumber, name, illustName, grade, type, element1, element2, bulletName, inGameName);
        }
        print("Hatchling Load Finish!");
    }
    HATCHLING_TYPE SetHatchlingType(string type)
    {
        if (type.Equals("AUTOMATIC"))
            return HATCHLING_TYPE.AUTOMATIC;
        else if (type.Equals("BREATH"))
            return HATCHLING_TYPE.BREATH;
        else if (type.Equals("CHANGE"))
            return HATCHLING_TYPE.CHANGE;
        else if (type.Equals("CHARGE"))
            return HATCHLING_TYPE.CHARGE;
        else if (type.Equals("COPY"))
            return HATCHLING_TYPE.COPY;
        else if (type.Equals("DEFENSE"))
            return HATCHLING_TYPE.DEFENSE;
        else if (type.Equals("EVENT"))
            return HATCHLING_TYPE.EVENT;
        else if (type.Equals("EXPLOSION"))
            return HATCHLING_TYPE.EXPLOSION;
        else if (type.Equals("FLAME"))
            return HATCHLING_TYPE.FLAME;
        else if (type.Equals("FREEZING"))
            return HATCHLING_TYPE.FREEZING;
        else if (type.Equals("GUIDANCE"))
            return HATCHLING_TYPE.GUIDANCE;
        else if (type.Equals("LASER"))
            return HATCHLING_TYPE.LASER;
        else if (type.Equals("LEARNING"))
            return HATCHLING_TYPE.LEARNING;
        else if (type.Equals("MULTIPLE"))
            return HATCHLING_TYPE.MULTIPLE;
        else if (type.Equals("PIERCE"))
            return HATCHLING_TYPE.PIERCE;
        else if (type.Equals("POISON"))
            return HATCHLING_TYPE.POISON;
        else if (type.Equals("POWERSHOT"))
            return HATCHLING_TYPE.POWERSHOT;
        else if (type.Equals("PRODUCTION"))
            return HATCHLING_TYPE.PRODUCTION;
        else if (type.Equals("RECHARGE"))
            return HATCHLING_TYPE.RECHARGE;
        else if (type.Equals("SHOCK"))
            return HATCHLING_TYPE.SHOCK;
        else if (type.Equals("SLOW"))
            return HATCHLING_TYPE.SLOW;
        else if (type.Equals("SPIRIT"))
            return HATCHLING_TYPE.SPIRIT;
        else if (type.Equals("WING"))
            return HATCHLING_TYPE.WING;
        else
        {
            Debug.LogError("SetHatchlingType() ERROR!! : " + type);
            return HATCHLING_TYPE.AUTOMATIC;
        }
            
    }
    ELEMENT SetHatchlingElement(string element)
    {
        if (element.Equals("NONE"))
            return ELEMENT.NONE;
        else if (element.Equals("WATER"))
            return ELEMENT.WATER;
        else if (element.Equals("THUNDER"))
            return ELEMENT.THUNDER;
        else if (element.Equals("FIRE"))
            return ELEMENT.FIRE;
        else if (element.Equals("DARK"))
            return ELEMENT.DARK;
        else
        {
            Debug.LogError("SetHatchlingElement() ERROR!! : " + element);
            return ELEMENT.NONE;
        }
    }
}
