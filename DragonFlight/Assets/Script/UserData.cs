﻿using UnityEngine;
using System.Collections;
// List와 Binary Formatter을 사용하기 위해 선언
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class UserData : MonoBehaviour
{

    enum ELEMENT { NONE, FIRE, WATER, THUNDER, DARK }

    private static UserData instance = null;
    public static UserData Instance
    {
        get
        {
            if (instance == null)
                Debug.LogError("Singleturn instance == null");
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }

    [Serializable]
    public class UserInformation
    {
        int userIdentificationNumber;
        int selectedCharacter;
        string ID;
        int coin;
        int gem;
        int bestScore;
        Hatchling hatchling1;
        Hatchling hatchling2;

        //List<Character> characters;
        Dictionary<int, Character> characters;
        Dictionary<int, int> inventory;
        Dictionary<int, Hatchling> hatchling;

        public UserInformation()
        {
            userIdentificationNumber = coin = gem = bestScore = 0;
            hatchling1 = new Hatchling(1, 1);
            hatchling2 = new Hatchling(134, 2);
            selectedCharacter = 11;
            ID = "KimTii";

            characters = new Dictionary<int, Character>();
            inventory = new Dictionary<int, int>();
            hatchling = new Dictionary<int, Hatchling>();
            hatchling[1] = new Hatchling(1, 10);
            hatchling[10] = new Hatchling(10, 10);
            hatchling[20] = new Hatchling(20, 10);
            hatchling[80] = new Hatchling(80, 10);
        }
        // User Infomation
        public string GetID() { return ID; }
        public int GetSelectedCharacter() { return selectedCharacter; }
        public int GetCoin() { return coin; }
        public int GetGem() { return gem; }
        public int GetBestScore() { return bestScore; }

        // Character
        public void AddCharacter(int number) { characters[number] = new Character(number); }
        public bool GetCharacterEnable(int number) { return characters[number].GetEnable(); }
        public int GetBulletLevel(int number) { return characters[number].GetBulletLevel(); }

        // Inventory

        // Hatchling
        public int GetHatchling1Number() { return hatchling1.GetNumber(); }
        public int GetHatchling2Number() { return hatchling2.GetNumber(); }
        public int GetHatchling1Level() { return hatchling1.GetLevel(); }
        public int GetHatchling2Level() { return hatchling2.GetLevel(); }
        public int GetHatchlingLevel(int hatchlingNumber) { return hatchling[hatchlingNumber].GetLevel(); }
        public int[] GetAllHatchlingNumber()
        {
            int[] numbers = new int[hatchling.Count];
            int numbersIndex = 0;
            foreach(KeyValuePair<int,Hatchling> member in hatchling)
            {
                numbers[numbersIndex] = member.Key;
                ++numbersIndex;
            }
            return numbers;
        }

        // TEST
        public void PrintAll()
        {
            print("[PRINT ALL] Characters Count : " + characters.Count);
            foreach (var member in characters)
            {
                print("[PRINT ALL] " + member.Value.characterIdentifiactionNumber + " : " + member.Value.enable);
            }
        }
    }
    public UserInformation userInformation = new UserInformation();

    [Serializable]
    class Character
    {
        public int characterIdentifiactionNumber;
        public bool enable;
        int level;
        int bulletLevel;
        ELEMENT element;
        int item1;
        int item2;
        int item3;

        public Character() { }
        public Character(int number)
        {
            if (number == 11)
                enable = true;
            else
                enable = false;

            characterIdentifiactionNumber = number;
            level = bulletLevel = 1;
            element = ELEMENT.NONE;
            item1 = item2 = item3 = 0;
        }
        public bool GetEnable() { return enable; }
        public int GetBulletLevel() { return bulletLevel; }
        // TEST
        //public int GetCharacterIdentificationNumber() { return characterIdentifiactionNumber; }
    }
    public void CharacterInitialization()
    {
        int[] numbers = DB.Instance.GetCharacterIdentificationNumbers();
        foreach (int member in numbers)
        {
            //print("[초기화] : " + member);
            userInformation.AddCharacter(member);
        }
    }

    [Serializable]
    class Hatchling
    {
        int hatchlingIdentificationNumber;
        int level;

        public Hatchling() { }
        public Hatchling(int _hatchlingNumber, int _level)
        {
            hatchlingIdentificationNumber = _hatchlingNumber;
            level = _level;
        }
        public int GetNumber() { return hatchlingIdentificationNumber; }
        public int GetLevel() { return level; }
    }

    public void SaveData()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        MemoryStream memoryStream = new MemoryStream();
        // Data를 바이트 배열로 변환해서 저장합니다.
        binaryFormatter.Serialize(memoryStream, userInformation);
        // 그것을 다시 한 번 문자열 값으로 변환해 PlayerPrefs에 저장합니다.
        PlayerPrefs.SetString("UserData", Convert.ToBase64String(memoryStream.GetBuffer()));
    }

    public void LoadData()
    {
        userInformation = new UserInformation();
        string userString = PlayerPrefs.GetString("UserData");
        if (!string.IsNullOrEmpty(userString))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(userString));
            // 가져온 데이터를 바이트 배열로 변환하고 사용하기 위해 캐스팅합니다.
            userInformation = (UserInformation)binaryFormatter.Deserialize(memoryStream);
        }
    }
}
