﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;  // AssetDataBase.Refresh

namespace SaveLoad
{
    public class SaveLoad : MonoBehaviour
    {
        public void UserDataCreate(string fileName)
        {
            string path = PathForDocumentsFile("Assets/Resources/" + fileName);
            if (!File.Exists(path))
            {
                FileStream file = new FileStream(path, FileMode.CreateNew, FileAccess.Write);
                StreamWriter sw = new StreamWriter(file);
                string data;
                
                sw.WriteLine("IdentificationNumber, Name, Score, WinCount, LoseCount");

                for(int i = 1; i < 100000; ++i)
                {
                    int iNumber = i;
                    string[] alphabet = new string[26] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                    string name = null;
                    for (int j = 0; j < 5; ++j)
                        name += alphabet[Random.Range(0, alphabet.Length)];
                    int score = Random.Range(0, 1000000);
                    int winCount = Random.Range(0, 999999);
                    int loseCount = Random.Range(0, 999999);
                    data = iNumber.ToString() + ',' + name + ',' + score + ',' + winCount + ',' + loseCount;
                    sw.WriteLine(data);
                }
                sw.Close();
                file.Close();
                AssetDatabase.Refresh();
            }
        }      
        public void UserDataLoad(string fileName, List<UserData.UserData> userDataList)
        {
            int iNumber, score, winCount, loseCount;
            string name = null;
            iNumber = score = winCount = loseCount = 0;

            // File Load
            TextAsset txtFile = (TextAsset)Resources.Load(fileName) as TextAsset;
            
            if (txtFile == null)
            {
                Debug.Log("Not Found File!");
                return;
            }
                
            string fileFullPath = txtFile.text;
            string[] stringArr = fileFullPath.Split('\n');
            string[] data;
            for (int index = 1; index < stringArr.Length-1; ++index)
            {
                data = stringArr[index].Split(',');
                for (int i = 0; i < data.Length; ++i)
                {
                    switch (i)
                    {
                        case 0: iNumber = System.Convert.ToInt32(data[i]); break;       // Identification Number
                        case 1: name = data[i]; break;                                  // Name
                        case 2: score = System.Convert.ToInt32(data[i]); break;         // Score
                        case 3: winCount = System.Convert.ToInt32(data[i]); break;      // Win Count
                        case 4: loseCount = System.Convert.ToInt32(data[i]); break;     // Lose Count
                        default: Debug.Log("Switch Default : 이상상태 발생"); break;
                    }
                }
                userDataList.Add(new UserData.UserData(iNumber, name, score, winCount, loseCount));
            }
        }
        
        public string PathForDocumentsFile(string fileName)
        {
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
                path = path.Substring(0, path.LastIndexOf('/'));
                return Path.Combine(Path.Combine(path, "Documents"), fileName);
            }
            else if (Application.platform == RuntimePlatform.Android)
            {
                string path = Application.persistentDataPath;
                path = path.Substring(0, path.LastIndexOf('/'));
                return Path.Combine(path, fileName);
            }
            else
            {
                string path = Application.dataPath;
                path = path.Substring(0, path.LastIndexOf('/'));
                return Path.Combine(path, fileName);
            }
        }
    }
}
