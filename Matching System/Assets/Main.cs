﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SaveLoad;
using UserData;

public class Main : MonoBehaviour {

    public List<UserData.UserData> userDataList;

	void Start () {
        //
        userDataList = new List<UserData.UserData>();
        Init();
    }

    void Init()
    {
        SaveLoad.SaveLoad fileManager = new SaveLoad.SaveLoad();
        fileManager.UserDataCreate("UserData.txt");
        fileManager.UserDataLoad("UserData", userDataList);

        userDataList.Sort(delegate(UserData.UserData a, UserData.UserData b)
        {
            if (a.GetScore() > b.GetScore()) return 1;
            else if (a.GetScore() < b.GetScore()) return -1;
            else return 0;
        });
    }
}
