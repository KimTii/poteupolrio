﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {
    public GameObject pMatching;
    public GameObject pSearch;

    public UILabel lName;
    public UILabel lScore;
    public UILabel lWinCount;
    public UILabel lLoseCount;

    public UILabel[] lResults;

    public UILabel lSearchName;
    public UILabel[] lStartsWiths;

    void Start()
    {
        MatchingButton();
    }

    public void MatchingButton()
    {
        pMatching.SetActive(true);
        pSearch.SetActive(false);
    }
    public void SearchButton()
    {
        pMatching.SetActive(false);
        pSearch.SetActive(true);
    }

    public void StartMatching()
    {
        // Infomation Setting
        int iNumber, score, winCount, loseCount;
        string name = null;
        iNumber = score = winCount = loseCount = 0;
        Main main = GetComponent<Main>();
        try
        {
            iNumber = main.userDataList.Count;
            name = lName.text;
            score = System.Convert.ToInt32(lScore.text);
            winCount = System.Convert.ToInt32(lWinCount.text);
            loseCount = System.Convert.ToInt32(lLoseCount.text);
        }
        catch
        {
            Debug.LogError("입력한 타입이 맞지 않습니다.");
            return;
        }

        // ADD
        main.userDataList.Add(new UserData.UserData(iNumber, name, score, winCount, loseCount));
        // Sort
        main.userDataList.Sort(delegate (UserData.UserData a, UserData.UserData b)
        {
            if (a.GetScore() > b.GetScore()) return 1;
            else if (a.GetScore() < b.GetScore()) return -1;
            else return 0;
        });
        // Save

        // Matching
        int myIndex = main.userDataList.FindIndex(x => (name == x.GetName() && score == x.GetScore()));
        int startIndex = myIndex - 5;

        for(int i = 0; i < lResults.Length; ++i)
        {
            if(startIndex < 0 || startIndex > main.userDataList.Count)
            {
                lResults[i].text = " ";
                ++startIndex;
                continue;
            }
            else
            {
                lResults[i].text = main.userDataList[startIndex].GetName() + "\n"
                    + main.userDataList[startIndex].GetScore().ToString("n0")
                    + " (" + main.userDataList[startIndex].GetWinCount().ToString("n0") + " / "
                    + main.userDataList[startIndex].GetLoseCount().ToString("n0") + ")";
                ++startIndex;
            }
        }
    }
    public void StartSearch()
    {
        Main main = GetComponent<Main>();

        int myIndex = main.userDataList.FindIndex(x => (lSearchName.text == x.GetName()));
        int startIndex = myIndex - 5;
        try
        {
            for (int i = 0; i < lResults.Length; ++i)
            {
                if (startIndex < 0 || startIndex > main.userDataList.Count)
                {
                    lResults[i].text = " ";
                    ++startIndex;
                    continue;
                }
                else
                {
                    lResults[i].text = main.userDataList[startIndex].GetName() + "\n"
                        + main.userDataList[startIndex].GetScore().ToString("n0")
                        + " (" + main.userDataList[startIndex].GetWinCount().ToString("n0") + " / "
                        + main.userDataList[startIndex].GetLoseCount().ToString("n0") + ")";
                    ++startIndex;
                }
            }
        }
        catch
        {
            Debug.LogError("서치를 할 수 없습니다.");
            return;
        }
    }
    public void StartsWith()
    {
        Main main = GetComponent<Main>();
        foreach (UILabel member in lStartsWiths)
            member.text = " ";
        int wordsIndex = 0;
        foreach(UserData.UserData member in main.userDataList)
        {
            if (wordsIndex == 5) break;
            if (member.GetName().StartsWith(lSearchName.text))
            {
                lStartsWiths[wordsIndex].text = member.GetName();
                ++wordsIndex;
            }
        }
    }
    public void ChooseWord(GameObject gameobject)
    {
        UILabel label = gameobject.GetComponent<UILabel>();
        if (label.text == " ")
            return;
        else
            lSearchName.text = label.text;
    }
}
