﻿using UnityEngine;
using System.Collections;

namespace UserData
{
    public class UserData
    {
        private int identificationNumber;
        private string name;
        private int score;
        private int winCount;
        private int loseCount;

        public UserData() { }
        public UserData(int _iNumber, string _name, int _score, int _winCount, int _loseCount)
        {
            identificationNumber = _iNumber;
            name = _name;
            score = _score;
            winCount = _winCount;
            loseCount = _loseCount;
        }

        public int GetiNumber() { return identificationNumber; }
        public string GetName() { return name; }
        public int GetScore() { return score; }
        public int GetWinCount() { return winCount; }
        public int GetLoseCount() { return loseCount; }
    }
}
