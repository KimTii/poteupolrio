﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Stage01Director : MonoBehaviour {
    GameObject pickObject;
    PlayerManager player;

    // Use this for initializationLever
    void Start()
    {
        player = GetComponent<PlayerManager>();
        StartCoroutine("MyUpdate");
    }

    IEnumerator MyUpdate()
    {
        while(true)
        {
            yield return new WaitForFixedUpdate();
            //TriggerMove();
        }
    }

    // 오브젝트를 클릭 하는 경우에 이동
    void TriggerMove()
    {
        if (Input.GetMouseButtonDown(0))
        {
            pickObject = GetComponent<GameManager>().GetPickObject();

            if (pickObject == null)
                return;

            if (pickObject.name == "Lever")
                player.Move(pickObject.transform.position);

            if (pickObject.name == "DoorButton")
                player.Move(pickObject.transform.position);
        }
    }
}
