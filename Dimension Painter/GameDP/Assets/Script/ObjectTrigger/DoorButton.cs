﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoorButton : MonoBehaviour {
    public GameObject linkDoorTrigger;
    public Vector3 camPosition;

    bool onePlayShot = false;
    bool OnSwithch = false;

    CameraManager cmManager;
    PlayerManager plManager;

    Vector3 currentCameraPosition;

    void Start()
    {
        cmManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        plManager = GameObject.Find("Manager").GetComponent<PlayerManager>();

        camPosition = new Vector3(camPosition.x, Camera.main.transform.position.y, camPosition.z);
    }

    void OnTriggerEnter(Collider coll)
    {
        currentCameraPosition = Camera.main.transform.position;
        if (coll.CompareTag("Player") || coll.CompareTag("PushBox"))
        {
            if (!OnSwithch)
            {
                if(!onePlayShot)
                {
                    Camera.main.transform.DOMove(camPosition, 1.3f).SetEase(Ease.OutExpo)
                             .OnComplete(() => Camera.main.transform.DOMove(currentCameraPosition, 1.3f).SetEase(Ease.OutExpo));

                        onePlayShot = true;
                }

                linkDoorTrigger.transform.DOMoveY(-1f, 1.0f);
            }
            else
                linkDoorTrigger.transform.DOMoveY(0.6f, 1.0f);

        }
    }

    void OnTriggerStay(Collider coll)
    {
        if (coll.tag == "Player" || coll.tag == "PushBox")
        {
            OnSwithch = true;
           

        }
                
        else
        {
            OnSwithch = false;
            
        }
           
    }

    public bool GetDoorSwithch() { return OnSwithch; }
}
