﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoorButtonBlue : MonoBehaviour {
    public GameObject linkDoorTrigger;
    public float second;
    public Vector3 camPosition;
    public bool onePlayShot = false;
    
    bool OnSwithch = false;

    CameraManager cmManager;
    PlayerManager plManager;
    GameManager gameManager;

    Vector3 currentCameraPosition;

    void Start()
    {
        cmManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        plManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
        gameManager = GameObject.Find("Manager").GetComponent<GameManager>();

        camPosition = new Vector3(camPosition.x, Camera.main.transform.position.y, camPosition.z);
    }
    
    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player"))
        {
            if (!OnSwithch)
            {
                currentCameraPosition = Camera.main.transform.position;
                if(!onePlayShot)
                {
                    StartCoroutine(this.OnePlayShot());
                }
                else
                {
                    StartCoroutine(this.Timer(second));
                }
                
                OnSwithch = true;
            }
        }
    }
    
    IEnumerator OnePlayShot()
    {
        //Player 움직임 정지
        plManager.SetPlayerAnimatorIdle();
        plManager.player.GetComponent<NavMeshAgent>().enabled = false;
        plManager.player.GetComponent<NavMeshAgent>().enabled = true;

        float stopSecond = 1.3f + 1.0f + 1.3f;
        gameManager.StartCoroutine("StopTouch", stopSecond);
        onePlayShot = true;

        //카메라 이동
        Camera.main.transform.DOMove(camPosition, 1.3f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.3f);

        //내려갔다가 올라오는 타이머
        StartCoroutine(this.Timer(second));
        yield return new WaitForSeconds(1.0f);

        //카메라 복귀
        Camera.main.transform.DOMove(currentCameraPosition, 1.3f).SetEase(Ease.OutQuint);
        yield return new WaitForSeconds(1.3f);
    }

    IEnumerator Timer(float second)
    {
        //내려감
        linkDoorTrigger.GetComponent<AudioSource>().Play();
        linkDoorTrigger.transform.Find("Column").transform.DOLocalMoveY(-0.86f, 1.0f);
        linkDoorTrigger.GetComponent<NavMeshObstacle>().enabled = false;
        yield return new WaitForSeconds(second);

        //올라감
        linkDoorTrigger.transform.Find("Column").transform.DOLocalMoveY(0f, 1.0f);
        linkDoorTrigger.GetComponent<NavMeshObstacle>().enabled = true;
        
        OnSwithch = false;
    }

    public bool GetDoorSwithch() { return OnSwithch; }
}
