﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoorButtonRed: MonoBehaviour {
    public GameObject linkDoorTrigger;
    public Vector3 camPosition;
    public bool onePlayShot = false;

    
    bool OnSwithch = false;

    CameraManager cmManager;
    PlayerManager plManager;
    GameManager gameManager;

    Vector3 currentCameraPosition;

    void Start()
    {
        cmManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        plManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
        gameManager = GameObject.Find("Manager").GetComponent<GameManager>();

        camPosition = new Vector3(camPosition.x, Camera.main.transform.position.y, camPosition.z);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("PushBox"))
        {
            if (!OnSwithch)
            {
                if(!onePlayShot)
                {
                    currentCameraPosition = Camera.main.transform.position;
                    StartCoroutine(this.OnePlayShot(false));
                }
                else
                {
                    //linkDoorTrigger.transform.DOMoveY(-1f, 1.0f);
                    linkDoorTrigger.GetComponent<AudioSource>().Play();
                    linkDoorTrigger.transform.Find("Column").transform.DOLocalMoveY(-0.86f, 1.0f);
                    linkDoorTrigger.GetComponent<NavMeshObstacle>().enabled = false;
                }

                OnSwithch = true;
            }
            else
            {
                //linkDoorTrigger.transform.DOMoveY(0.6f, 1.0f);
                linkDoorTrigger.transform.Find("Column").transform.DOLocalMoveY(0f, 1.0f);
                linkDoorTrigger.GetComponent<NavMeshObstacle>().enabled = true;
            }
        }
    }

    IEnumerator OnePlayShot(bool up)
    {
        float stopSecond = 1.3f + 1.0f + 1.3f;
        gameManager.StartCoroutine("StopTouch", stopSecond);
        onePlayShot = true;

        if(!up)
        {
            //카메라 이동
            Camera.main.transform.DOMove(camPosition, 1.3f).SetEase(Ease.OutExpo);
            yield return new WaitForSeconds(1.3f);

            //문 내려감
            linkDoorTrigger.GetComponent<AudioSource>().Play();
            linkDoorTrigger.transform.Find("Column").transform.DOLocalMoveY(-0.86f, 1.3f);
            yield return new WaitForSeconds(1.3f);

            //카메라 복귀
            linkDoorTrigger.GetComponent<NavMeshObstacle>().enabled = false;
            Camera.main.transform.DOMove(currentCameraPosition, 1.3f).SetEase(Ease.OutExpo);
            yield return new WaitForSeconds(1.3f);
        }
        
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.CompareTag("Player") || coll.CompareTag("PushBox"))
        {
            if (OnSwithch)
            {
                //linkDoorTrigger.transform.DOMoveY(0.6f, 1.0f);
                linkDoorTrigger.transform.Find("Column").transform.DOLocalMoveY(0f, 1.0f);
                linkDoorTrigger.GetComponent<NavMeshObstacle>().enabled = true;
                OnSwithch = false;
            }
        }
    }
    
    public bool GetDoorSwithch() { return OnSwithch; }
}
