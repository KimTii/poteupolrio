﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class JewelryGoalTriggerFootSwitch : MonoBehaviour {

    public GameObject jewelryGoalTrigger;
    //public ParticleSystem goalTriggerParticle;
    public GameObject jewelry;
    public Vector3 goalTriggerPosition;

    CameraManager cameraManager;
    GameManager gameManager;

    Vector3 nomalPosition;
    Vector3 downPosition;

    void Awake()
    {
        DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(100, 10);

        //골트리거 비활성화
        jewelryGoalTrigger.GetComponent<BoxCollider>().enabled = false;

        //Getcomponent
        cameraManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        gameManager = GameObject.Find("Manager").GetComponent<GameManager>();

        nomalPosition = transform.position;
        downPosition = nomalPosition + new Vector3(0, -0.2f, 0);
    }

    void OnTriggerEnter(Collider coll)
    {
        //Player나 PushBox에만 작동하라
        if(coll.CompareTag("PushBox") || coll.CompareTag("Player"))
        {
            StartCoroutine(this.OnePlayShot());
        }
    }

    IEnumerator OnePlayShot()
    {
        Vector3 currentCameraPosition = Camera.main.transform.position;
        float stopSecond = 1.3f + 5.0f + 1.3f;
        gameManager.StartCoroutine("StopTouch", stopSecond);

        //눌리면서 스위치 내려가고
        transform.DOMove(downPosition, 0.2f);
        //카메라 골 트리거로 이동
        Camera.main.transform.DOMove(goalTriggerPosition, 1.3f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.3f);
        
        //골트리거 활성화
        jewelryGoalTrigger.GetComponent<BoxCollider>().enabled = true;
        //애니메이션 실행
            //파티클 실행
        jewelryGoalTrigger.GetComponent<ParticleSystem>().Play();
            //천천히 내려오면서
            //다 내려오면 위아래로 루프
        jewelry.transform.DOMoveY(1, 5).OnComplete(() => jewelry.transform.DOMoveY(2, 2).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo));
            //360도 회전
        jewelry.transform.DOLocalRotate(new Vector3(360, 0, 0), 2, RotateMode.LocalAxisAdd).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);
            
        //다중 실행 방지
        this.GetComponent<BoxCollider>().enabled = false;
        //사운드 실행
        //jewelryGoalTrigger.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(5.0f);

        //카메라 복귀
        Camera.main.transform.DOMove(currentCameraPosition, 1.3f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.3f);
    }
}
