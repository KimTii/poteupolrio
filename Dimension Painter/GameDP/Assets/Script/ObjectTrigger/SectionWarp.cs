﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SectionWarp : MonoBehaviour {
    public Vector3 camPosition;
    public Vector3 WarpZonePosition;
    public ParticleSystem waitParticle;
    public ParticleSystem warpParticle;

    bool onePlayShot = false;
    bool OnSwithch = false;

    CameraManager cmManager;
    PlayerManager plManager;
    GameManager gameManager;

    Transform playerTransform;

    void Start()
    {
        cmManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        plManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
        gameManager = GameObject.Find("/Manager").GetComponent<GameManager>();

        playerTransform = plManager.player.transform;

        camPosition = new Vector3(camPosition.x, Camera.main.transform.position.y, camPosition.z);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player"))
        {
            // On
            if (!OnSwithch)
            {
                StartCoroutine(this.PlayShot());
            }

            // Off
            else
            {
                if (!onePlayShot)
                {
                    onePlayShot = true;
                }
                OnSwithch = false;
            }
        }
    }

    IEnumerator PlayShot()
    {
        float stopSecond = 0.5f + 0.5f + 1.0f + 0.5f + 0.5f;
        gameManager.StartCoroutine("StopTouch", stopSecond);

        //plManager.Move(new Vector3(transform.position.x, playerTransform.position.y, transform.position.z));
        plManager.Move(playerTransform.position);
        plManager.player.GetComponent<NavMeshAgent>().enabled = false;
        
        //연출
        //Player 바라보기
        cmManager.FollowPlayer();
        //파티클
        waitParticle.Stop();
        warpParticle.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y - 0.41f, playerTransform.position.z);
        warpParticle.Play();
        yield return new WaitForSeconds(0.5f);

        //plManager.player.transform.DORotate(new Vector3(0, 360, 0), 0.25f, RotateMode.FastBeyond360).SetEase(Ease.InQuint).SetLoops(4, LoopType.Incremental);
        playerTransform.DOScale(0, 0.5f);
        //playerTransform.DOMoveY(-1.0f, 1.0f);
        yield return new WaitForSeconds(0.5f);

        Camera.main.transform.DOMove(camPosition, 1.0f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.0f);

        playerTransform.position = WarpZonePosition;
        warpParticle.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y - 0.41f, playerTransform.position.z);
        warpParticle.Play();
        yield return new WaitForSeconds(0.5f);

        //plManager.player.transform.DORotate(new Vector3(0, -360, 0), 0.25f, RotateMode.FastBeyond360).SetEase(Ease.InQuint).SetLoops(4, LoopType.Incremental);
        plManager.player.transform.DOScale(1, 0.5f);
        //playerTransform.DOMoveY(0.92f, 1.0f);
        yield return new WaitForSeconds(0.5f);

        waitParticle.Play();
    }

    public bool GetWarpSwithch() { return OnSwithch; }
}