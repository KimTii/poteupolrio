﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RotationMapTrigger : MonoBehaviour {
    public GameObject rotationMap;
    public GameObject[] endMap;

    Ray[] rayEndMap;
    RaycastHit hit;
    byte mapNumber;

	// Use this for initialization
	void Start () {
        rayEndMap = new Ray[4];
        CheckMap();
	}

    void Update()
    {
        //Debug.DrawRay(rayEndMap01.origin, Vector3.forward, Color.red);
    }

    void OnTriggerEnter(Collider coll)
    {
        //돌아가는 동안에는 컨트롤 할 수 없도록 하자.
        //컨트롤을 할 수 없을때는 당연히 연출이 들어가야 한다.
        if(coll.CompareTag("Player"))
        {
            rotationMap.transform.DORotate(rotationMap.transform.rotation.eulerAngles + new Vector3(0, 90, 0), 1.0f)
                .OnComplete(() => CheckMap());
        }
    }

    void CheckMap()
    {
        Debug.Log("CheckMap()");

        for (int i = 0; i < endMap.Length; ++i )
        {
            //초기화
            rayEndMap[i] = new Ray();
            rayEndMap[i].origin = endMap[i].transform.position;

            //4방향으로 레이캐스트 체크!
            for (int arrow = 0; arrow < 4; ++arrow)
            {
                if (arrow == 0)
                    rayEndMap[i].direction = Vector3.forward;
                else if (arrow == 1)
                    rayEndMap[i].direction = Vector3.right;
                else if (arrow == 2)
                    rayEndMap[i].direction = Vector3.back;
                else if (arrow == 3)
                    rayEndMap[i].direction = Vector3.left;
                else
                {
                    Debug.Log("CheckMap Error!!");
                    return;
                }

                Physics.Raycast(rayEndMap[i], out hit, 1.0f);
                if (hit.collider != null && hit.collider.CompareTag("Map"))
                    ++mapNumber;

                print(mapNumber);
            }
            Debug.Log("mapNumber : " + mapNumber);

            if (mapNumber > 1)
                endMap[i].GetComponent<NavMeshObstacle>().enabled = false;
            else
                endMap[i].GetComponent<NavMeshObstacle>().enabled = true;

            mapNumber = 0;   
        }
    }
}
