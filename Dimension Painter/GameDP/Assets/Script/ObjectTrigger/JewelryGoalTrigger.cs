﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class JewelryGoalTrigger : MonoBehaviour {
    //public ParticleSystem goalTriggerParticle;
    PlayerManager playerManager;
    CameraManager cameraManager;
    GameManager gameManager;

    private int currentNumber;

    void Start()
    {
        playerManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
        cameraManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        gameManager = GameObject.Find("Manager").GetComponent<GameManager>();
    }

    void CurrntStageInfo()
    {
        switch ( Application.loadedLevelName )
        {
            case "GRASS01":
                currentNumber = 1;
                break;
            case "GRASS02":
                currentNumber = 2;
                break;
            case "GRASS03":
                currentNumber = 3;
                break;
            case "GRASS04":
                currentNumber = 4;
                break;
            case "GRASS05":
                currentNumber = 5;
                break;
            case "GRASS06":
                currentNumber = 6;
                break;
            case "GRASS07":
                currentNumber = 7;
                break;
            case "GRASS08":
                currentNumber = 8;
                break;

            default :
                currentNumber = 0;
                break;
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player"))
        {
            //연출
            StartCoroutine(this.GoalEffect());
            GetComponent<BoxCollider>().enabled = false;

            // Data Save
            if ( GameData.Instance.GetStageInfo() == currentNumber )
            {
                GameData.Instance.NextStageInfo();
                PlayerPrefs.SetInt("STAGEPROGRASS", GameData.Instance.GetStageInfo() );
            }
        }
    }

    IEnumerator GoalEffect()
    {
        float stopTouchTime = 1.0f + 0.7f + 1.0f;
        gameManager.StartCoroutine("StopTouch", stopTouchTime);

        playerManager.Move(playerManager.GetPlayerPosition());
        playerManager.player.GetComponent<NavMeshAgent>().enabled = false;
        cameraManager.FollowPlayer();
        yield return new WaitForSeconds(1.0f);

        playerManager.player.transform.DOScale(0, 1.0f).SetEase(Ease.InOutBack).SetLoops(1, LoopType.Yoyo);
        yield return new WaitForSeconds(0.7f);
        //goalTriggerParticle.transform.position = playerManager.GetPlayerPosition();
        //goalTriggerParticle.Play();
        yield return new WaitForSeconds(1.0f);
        GameObject.Find("FadeImage").GetComponentInChildren<Image>().DOFade(1, 4.0f)
                .OnComplete(() => GameObject.Find("Manager").GetComponent<Loading>().StartCoroutine("StartLoad", "WorldMap"));
    }
}
