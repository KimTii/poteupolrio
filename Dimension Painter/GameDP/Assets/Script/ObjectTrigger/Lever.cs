﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Lever : MonoBehaviour {
    public GameObject[] Logs;
    public GameObject gear;
    public Vector3 camPosition;
    public bool onePlayShot = false;
    
    bool OnSwithch = false;

    CameraManager cmManager;
    PlayerManager plManager;
    GameManager gmManager;

    Vector3 currentCameraPosition;

    void Start()
    {
        cmManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        plManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
        gmManager = GameObject.Find("Manager").GetComponent<GameManager>();

        camPosition = new Vector3(camPosition.x, Camera.main.transform.position.y, camPosition.z);
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player"))
        {
            //Player 움직임 정지
            plManager.SetPlayerAnimatorIdle();
            plManager.player.GetComponent<NavMeshAgent>().enabled = false;
            plManager.player.GetComponent<NavMeshAgent>().enabled = true;

            // On
            if (!OnSwithch)
            {
                if (!onePlayShot)
                {
                    currentCameraPosition = Camera.main.transform.position;
                    StartCoroutine(this.OnePlayShot(true));
                }
                else
                {
                    StartCoroutine(this.LeverOnOff(true));
                }
            }
            
            // Off
            else
            {
                if (!onePlayShot)
                {
                    currentCameraPosition = Camera.main.transform.position;
                    StartCoroutine(this.OnePlayShot(false));
                }
                else
                {
                    StartCoroutine(this.LeverOnOff(false));
                }
            }
        }
    }

    IEnumerator OnePlayShot(bool up)
    {
        //카메라 움직인 시간 + 다리들 움직인 시간 + 다시 카메라 움직인 시간
        float stopSecond = 1.3f + (0.7f * Logs.Length) + 1.3f;
        gmManager.StartCoroutine("StopTouch", stopSecond);
        onePlayShot = true;

        if(up)
            GetComponent<Animation>().Play("On");
        else
            GetComponent<Animation>().Play("Off");

        GetComponent<AudioSource>().Play();

        //카메라 이동
        Camera.main.transform.DOMove(camPosition, 1.3f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.3f);
        //다리 움직임
        StartCoroutine(this.LogsAnimation(up));
    }
    IEnumerator LeverOnOff(bool up)
    {
        if(up)
        {
            GetComponent<Animation>().Play("On");
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.7f);
            
            StartCoroutine(this.LogsAnimation(true));
        }
        else
        {
            GetComponent<Animation>().Play("Off");
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(0.7f);
            
            StartCoroutine(this.LogsAnimation(false));
        }
    }

    IEnumerator LogsAnimation(bool up)
    {
        if(up)
        {
            OnSwithch = true;
            for (int i = 0; i < Logs.Length; ++i)
            {
                Logs[i].transform.DOLocalMoveY(0.12f, 1.0f);
                Logs[i].GetComponent<NavMeshObstacle>().enabled = false;
                Logs[i].GetComponent<AudioSource>().Play();
                Handheld.Vibrate();
                gear.GetComponent<Animation>().Play("Rotation");

                yield return new WaitForSeconds(1f);
            }
        }
        else
        {
            OnSwithch = false;
            for (int i = 0; i < Logs.Length; ++i)
            {
                Logs[i].transform.DOLocalMoveY(-0.6f, 1.0f);
                Logs[i].GetComponent<NavMeshObstacle>().enabled = true;
                Logs[i].GetComponent<AudioSource>().Play();

                yield return new WaitForSeconds(1f);
            }
        }

        Camera.main.transform.DOMove(currentCameraPosition, 1.3f).SetEase(Ease.InOutQuint);
    }

    public bool GetLeverSwithch() { return OnSwithch; }
}
