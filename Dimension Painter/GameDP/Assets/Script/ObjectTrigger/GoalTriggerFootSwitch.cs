﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GoalTriggerFootSwitch : MonoBehaviour {

    public GameObject goalTrigger;
    public ParticleSystem goalTriggerParticle;
    public Vector3 goalTriggerPosition;

    CameraManager cameraManager;
    GameManager gameManager;
    PlayerManager playerManager;

    Vector3 nomalPosition;
    Vector3 downPosition;

    void Awake()
    {
        DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(100, 10);

        //골트리거 비활성화
        goalTrigger.GetComponent<BoxCollider>().enabled = false;

        //Getcomponent
        cameraManager = GameObject.Find("Manager").GetComponent<CameraManager>();
        gameManager = GameObject.Find("Manager").GetComponent<GameManager>();
        playerManager = GameObject.Find("Manager").GetComponent<PlayerManager>();

        nomalPosition = transform.position;
        downPosition = nomalPosition + new Vector3(0, -0.2f, 0);
    }

    void OnTriggerEnter(Collider coll)
    {
        //Player나 PushBox에만 작동하라
        if(coll.CompareTag("PushBox") || coll.CompareTag("Player"))
        {
            if(coll.CompareTag("Player"))
            {
                //Player 움직임 정지
                playerManager.SetPlayerAnimatorIdle();
                playerManager.player.GetComponent<NavMeshAgent>().enabled = false;
                playerManager.player.GetComponent<NavMeshAgent>().enabled = true;
            }
                
            StartCoroutine(this.OnePlayShot());
        }
    }

    IEnumerator OnePlayShot()
    {
        Vector3 currentCameraPosition = Camera.main.transform.position;
        float stopSecond = 1.3f + 1.0f + 1.3f;
        gameManager.StartCoroutine("StopTouch", stopSecond);

        //눌리면서 스위치 내려가고
        transform.DOMove(downPosition, 0.2f);
        //카메라 골 트리거로 이동
        Camera.main.transform.DOMove(goalTriggerPosition, 1.3f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.3f);
        
        //골트리거 활성화
        goalTrigger.GetComponent<BoxCollider>().enabled = true;
        //애니메이션 실행
        goalTrigger.GetComponent<Animator>().SetBool("Goal", true);
        goalTriggerParticle.Play();
        //다중 실행 방지
        this.GetComponent<BoxCollider>().enabled = false;
        goalTrigger.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1.0f);

        //카메라 복귀
        Camera.main.transform.DOMove(currentCameraPosition, 1.3f).SetEase(Ease.InOutQuint);
        yield return new WaitForSeconds(1.3f);
    }
}
