﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Pen : MonoBehaviour {
    
    Transform thisTransform;
    bool isPlay;

    void Awake()
    {
        DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(750, 10);
        thisTransform = this.transform;
        isPlay = false;

        //thisTransform.DOMoveY(thisTransform.position.y + 1.0f, 1.0f).SetLoops(-1, LoopType.Yoyo);
    }

    public void StartPenAnimation()
    {
        thisTransform.DOMoveY(thisTransform.position.y + 1.0f, 1.0f).SetLoops(-1, LoopType.Yoyo);
    }

    public void StopPenAnimation()
    {
        thisTransform.DOPause();
    }
}
