﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraManager : MonoBehaviour {

    public enum TouchState
    {
        Wait,
        Swipe,
        SingleTouch,
        MultiTouch,
        UI,
        doubleTouch
    }
    public TouchState touchState = TouchState.Wait;

    public Transform top;
    public Transform bottom;
    public Transform right;
    public Transform left;

    //GetComponent
    GameManager gameManager;
    PlayerManager playerManager;

    //Single Touch 계산에 필요한 변수들
    Vector2 beganPosition;
    Vector2 endPosition;
    Vector2 swipeBeganPosition;
    float deltaPositionX;
    float deltaPositionY;
    Vector3 deltaPosition;

    float swipeSpeed;

    //Multi Touch 계산에 필요한 변수들
    Vector2 currentFirstTouch;
    Vector2 lastFirstTouch;
    Vector2 currentSecondTouch;
    Vector2 lastSecondTouch;
    float currentDistance;
    float lastDistance;
    float zoomFactor;

    float cameraOrthographicSize;

    //허용오차
    float minTolerance;
    float maxTolerance;

    //자주 쓰이는 변수들 미리 선언
    Transform mainCameraTransform;

    //
    bool followPlayer;

	// Use this for initialization
    void Awake() {
        DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(300, 10);
        gameManager = GetComponent<GameManager>();
        playerManager = GetComponent<PlayerManager>();

        swipeSpeed = 0.7f;
        minTolerance = 20.0f;
        maxTolerance = 300;
        mainCameraTransform = Camera.main.transform;
        followPlayer = false;
	}

    public void CameraSwipeInitialization()
    {
        beganPosition = Input.mousePosition;
        swipeBeganPosition = Input.mousePosition;
    }

    public void DragAndSwipe()
    {
        StopTween();
        endPosition = Input.mousePosition;

        if ((Vector2.Distance(swipeBeganPosition, endPosition) > minTolerance && Vector2.Distance(swipeBeganPosition, endPosition) < maxTolerance) || touchState == TouchState.Swipe)
        {
            touchState = TouchState.Swipe;
            cameraOrthographicSize = Camera.main.orthographicSize;
            deltaPositionX = (endPosition.x - swipeBeganPosition.x);
            deltaPositionY = (endPosition.y - swipeBeganPosition.y);
            deltaPosition = new Vector3(-(deltaPositionX * swipeSpeed), 0, (deltaPositionX * swipeSpeed)) + new Vector3(-(deltaPositionY * swipeSpeed), 0, -(deltaPositionY * swipeSpeed));
            deltaPosition *= Time.deltaTime;
            //줌인아웃에 따라서 스와이프 속도 변환
            deltaPosition *= (cameraOrthographicSize * 0.2f);
            //print("스와이프");
            if ((Mathf.Clamp(mainCameraTransform.position.x, left.position.x, right.position.x) == left.position.x && deltaPosition.x < 0) ||
                (Mathf.Clamp(mainCameraTransform.position.x, left.position.x, right.position.x) == right.position.x && deltaPosition.x > 0))
            {
                deltaPosition.x = 0;
            }
            if((Mathf.Clamp(mainCameraTransform.position.z, bottom.position.z, top.position.z) == bottom.position.z && deltaPosition.z < 0) ||
                (Mathf.Clamp(mainCameraTransform.position.z, bottom.position.z, top.position.z) == top.position.z && deltaPosition.z > 0))
            {
                deltaPosition.z = 0;
            }

            //mainCameraTransform.Translate(deltaPosition, Space.World);
            mainCameraTransform.DOMove(mainCameraTransform.position + deltaPosition, 0.1f);

            swipeBeganPosition = Input.mousePosition;
        }
    }

    public void PinchZoomInOut()
    {
        StopTween();
        currentFirstTouch = Input.GetTouch(0).position;
        lastFirstTouch = currentFirstTouch - Input.GetTouch(0).deltaPosition;
        currentSecondTouch = Input.GetTouch(1).position;
        lastSecondTouch = currentSecondTouch - Input.GetTouch(1).deltaPosition;

        currentDistance = Vector2.Distance(currentFirstTouch, currentSecondTouch);
        lastDistance = Vector2.Distance(lastFirstTouch, lastSecondTouch);

        //확대 -1  /  축소 +1
        zoomFactor = Mathf.Clamp(lastDistance - currentDistance, -1.0f, 1.0f);
        //print(zoomFactor);
        cameraOrthographicSize = Camera.main.orthographicSize;
        Camera.main.DOOrthoSize(Mathf.Clamp(cameraOrthographicSize + zoomFactor, 2, 5), 0.2f);
    }
   
    public void FollowPlayer()
    {
        StopTween();
        Vector3 playerPosition = playerManager.GetPlayerPosition();

       // transform.DOMove(transform.position,1).SetEase(Ease.)
        //if (!followPlayer)
        //{
        mainCameraTransform.DOMove(new Vector3((playerPosition.x - 8), 21, (playerPosition.z - 8)), 1.0f).SetEase(Ease.InOutQuint);
        //}
        //else
        //{
        //    mainCameraTransform.DOMove(new Vector3((playerPosition.x - 8), 21, (playerPosition.z - 8)), 0.1f).SetEase(Ease.InQuad);
        //}
    }

    void StopTween()
    {
        Camera.main.transform.DOPause();
    }

    public void LookAtTarget(Vector3 targetPosition, float time)
    {
        StopTween();
        gameManager.StopTouch(time);
        mainCameraTransform.DOMove(new Vector3((targetPosition.x - 8), 21, (targetPosition.z - 8)), time).SetEase(Ease.InQuad);
    }

    public TouchState GetTouchState() { return touchState; }
    public void SetTouchState(TouchState _touchState) { touchState = _touchState; }
}

