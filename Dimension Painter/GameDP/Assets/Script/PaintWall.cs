﻿using UnityEngine;
using System.Collections;

static class PaintWallState
{
    public const int Door = 0;
    public const int Warp = 1;
}

public class PaintWall : MonoBehaviour {
    //GetComponent
    SpriteRenderer doorSpriteFront;
    SpriteRenderer doorSpriteBack;
    BoxCollider boxCollider;
    NavMeshObstacle navMeshObstacle;

    bool door;
    bool warp;

    void Awake()
    {
        doorSpriteFront = transform.Find("DoorSpriteFront").GetComponent<SpriteRenderer>();
        doorSpriteBack = transform.Find("DoorSpriteBack").GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider>();
        navMeshObstacle = GetComponent<NavMeshObstacle>();

        door = false;
        warp = false;
    }

    public bool CheckPaintWallState()
    {
        if (door || warp)
            return false;
        else
            return true;
    }

    public bool CheckPaintWallWarpState()
    {
        if (warp)
            return true;
        else
            return false;
    }

    public void SetPaintWallState(int id, bool setState)
    {
        if (id == PaintWallState.Door)
        {
            door = setState;
            GetComponent<AudioSource>().Play();
        }
            
        else if (id == PaintWallState.Warp)
        {
            warp = setState;
            GameObject.Find("Warp").GetComponent<AudioSource>().Play();
        }

           
        else
            print("SetPaintWallState Error!!");

        StartCoroutine(this.DoorTimer(5.0f));
    }

    IEnumerator DoorTimer(float time)
    {
        if (door == true)
        {
            //시간 기다리고
            yield return new WaitForSeconds(time);
            //상태 변경
            door = false;
            //Sprite 제거

            //boxCollider랑  navMeshObstacle 다시 켜주기
            boxCollider.enabled = true;
            navMeshObstacle.enabled = true;
            //여기에 쓰이는 기능들이 혹시나 나중에 규모가 커지게 되면 따로 함수화 시키길 권장한다.
            //현재 수준에서는 이렇게 써도 무방하다고 판단한다.
        }
        else
            yield break;
        
    }

    public void TurnOffColliderAndObstacle()
    {
        boxCollider.enabled = false;
        navMeshObstacle.enabled = false; 
    }
}
