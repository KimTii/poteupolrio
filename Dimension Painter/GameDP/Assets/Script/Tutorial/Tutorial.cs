﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Tutorial : MonoBehaviour {

    public Image touchImage;
    public Vector2 touchImagePosition;
    public Vector3 cameraPosition;
    public float delay;
    public bool onTriggerEnter;
    public bool tutorialEnable;

    Vector3 currentCameraPosition;
    float stopTouchTime;
    

    //자주쓰는 Transform & GetComponent
    Transform cameraTransform;
    GameManager gameManager;
    PlayerManager playerManager;

    void Awake()
    {
        //
        cameraTransform = Camera.main.transform;
        gameManager = GameObject.Find("Manager").GetComponent<GameManager>() ;
        playerManager = GameObject.Find("Manager").GetComponent<PlayerManager>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.CompareTag("Player") && onTriggerEnter)
        {
            GetComponent<BoxCollider>().enabled = false;
            StartCoroutine(CameraEffect());
        }
    }

    void OnTriggerStay(Collider coll)
    {
        if (coll.CompareTag("Player") && tutorialEnable && !onTriggerEnter)
        {
            GetComponent<BoxCollider>().enabled = false;
            StartCoroutine(CameraEffect());
        }
    }

    IEnumerator CameraEffect()
    {
        //움직임 정지
        //이렇게 이상하게 짠 이유는 PlaerManager의 Move가 말을 안들어서 이다.
        playerManager.SetPlayerAnimatorIdle();
        playerManager.player.GetComponent<NavMeshAgent>().enabled = false;
        playerManager.player.GetComponent<NavMeshAgent>().enabled = true;

        stopTouchTime = delay + 1.0f + 1.0f + 1.0f + 1.0f;
        gameManager.StartCoroutine("StopTouch", stopTouchTime);
        currentCameraPosition = cameraTransform.position;

        yield return new WaitForSeconds(delay);

        //카메라 이동
        cameraTransform.DOMove(cameraPosition, 1.0f);
        yield return new WaitForSeconds(1.0f);

        //터치 연출
        touchImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(2000, 0);
        touchImage.color = new Color(0,0,0,0);

        touchImage.GetComponent<RectTransform>().anchoredPosition = touchImagePosition;
        touchImage.DOFade(1, 0.5f).OnComplete(() => touchImage.DOFade(0, 0.5f));
        yield return new WaitForSeconds(1.0f);
        touchImage.DOFade(1, 0.5f).OnComplete(() => touchImage.DOFade(0, 0.5f));
        yield return new WaitForSeconds(1.0f);

        //카메라 복귀
        cameraTransform.DOMove(currentCameraPosition, 1.0f);
        yield return new WaitForSeconds(1.0f);
    }

    public void SetTrueTutorialEnable()
    {
        tutorialEnable = true;
    }
}
