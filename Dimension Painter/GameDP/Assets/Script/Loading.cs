﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Loading : MonoBehaviour {

    public GameObject loadingSet;                               // loading image
    public Slider loadingSlider;                                // loading prograss
    bool isLoadGame = false;                                    // loding drag

    public IEnumerator StartLoad(string strSceneName)
    {

        if (Application.loadedLevelName != "WorldMap")
        {
            // Loding Fade Out On
            loadingSet.transform.localPosition = new Vector3(0, 0, 0);
            loadingSet.GetComponentInChildren<Image>().DOFade(1, 0f);
            loadingSet.GetComponentInChildren<Image>().GetComponentInChildren<Text>().DOFade(1, 0f);


            GameObject.Find("IndicaterLoding").transform.DOLocalRotate(new Vector3(0, 0, 360f), 1.5f, RotateMode.FastBeyond360)
                .SetLoops(-1, LoopType.Incremental);
        }
        // 4.5Second after Scene Call.
        yield return new WaitForSeconds(0.5f);

        if (isLoadGame == false)
        {
            isLoadGame = true;

            AsyncOperation async = Application.LoadLevelAsync(strSceneName);

            while (async.isDone == false)
            {
               //float p = async.progress * 100f;
               // int pRounded = Mathf.RoundToInt(p);

                loadingSlider.value = async.progress;

                yield return true;
            }
        }
    }

}
