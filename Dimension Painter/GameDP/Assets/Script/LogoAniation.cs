﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class LogoAniation : MonoBehaviour {
    // alpha aniamation
    IEnumerator LogoAnimation()
    {
        yield return new WaitForSeconds(1.0f);

        GameObject.Find("IZZLE Logo").GetComponent<Image>().DOFade(1, 3.0f)
            .OnComplete( () => GameObject.Find("IZZLE Logo").GetComponent<Image>().DOFade(0, 2.0f));
        GameObject.Find("Studio Logo").GetComponent<Image>().DOFade(1, 3.0f)
            .OnComplete(() => GameObject.Find("Studio Logo").GetComponent<Image>().DOFade(0, 2.0f).OnComplete(() => this.GetComponent<Loading>().StartCoroutine("StartLoad", "WorldMap")));

        yield return null;
    }

	// Use this for initialization
	void Start () {
        // Game First Display & Infomation Management

        Screen.SetResolution(Screen.width, Screen.width / 16 * 9, true);
        //Screen.SetResolution(Screen.width, Screen.width / 16 * 9, true);
        Application.targetFrameRate = 60;

        this.GetComponent<Loading>().StartCoroutine("StartLoad", "WorldMap");
        
        // logo animation function call.
        //PlayerPrefs.DeleteAll();
        //StartCoroutine(this.LogoAnimation());
	}
}
