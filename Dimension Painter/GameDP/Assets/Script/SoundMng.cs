﻿using UnityEngine;
using System.Collections;


// 캐릭터의 행동에 따른 사운드는 ->> 애니메이션의 판별 여부에 따라 플레이 시켜준다.
public class SoundMng : MonoBehaviour {

    PlayerManager playerManager;
    AudioSource playerRunSound;

	// Use this for initialization
	void Awake () {
        playerManager = GetComponent<PlayerManager>();
        // 사운드 인스턴스를 추가
        playerRunSound = this.GetComponent<AudioSource>();
        this.StartCoroutine("WalkSound");
	}

    // 0.3초에 한번씩 발걸음 사운드 출력
    public IEnumerator WalkSound()
    {
        while(true)
        {
            yield return new WaitForSeconds(0.32f);
            if (playerManager.GetStateMovement())
                playerRunSound.Play();
        }
    }


}
