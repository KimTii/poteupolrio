﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

static class ButtonName
{
    public const string GreenButton = "GreenButton";
    public const string BlueButton = "BlueButton";
    public const string RedButton = "RedButton";
    public const string YellowButton = "YellowButton";
}

public class PairTrigger : MonoBehaviour {

    public GameObject buttonUp;
    public GameObject buttonDown;
    public GameObject[] target;

    //미리선언(이부분은 모든 씬에서 모두 적용되야만 한다.
    //만일 초기화 단계에서 에러가 발생할 경우,
    //하나의 씬에서라도 사용되지 않는경우 과감히 지우고, 코드수정 하길 바란다.)
    Transform targetTransform;
    NavMeshObstacle targetNavMeshObstacle;

    string sceneName;
    string myName;
    bool oneTime;

    void Awake()
    {
        DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(100, 10);
        buttonDown.SetActive(false);

        //주로 많이 쓰이는 0번은 미리 만들어 놓습니다.
        targetTransform = target[0].transform;
        targetNavMeshObstacle = target[0].GetComponent<NavMeshObstacle>();

        sceneName = Application.loadedLevelName;
        myName = this.name;

        //Trigger들의 초기화! 맵마다 다 다르니 시작할때 함께 합니다.
        TriggerInitialization();
    }

    void OnTriggerEnter(Collider coll)
    {
        //콜라이더는 최상단에 존재합니다.
        //혹시 모델만 옴기시지 않으셨나요?
        if(coll.CompareTag("Player") || coll.CompareTag("PushBox"))
        {
            buttonUp.SetActive(false);
            buttonDown.SetActive(true);

            SceneFunction(true);
        }
    }
    /*
    void OnTriggerStay(Collider coll)
    {
        if (coll.CompareTag("Player") || coll.CompareTag("PushBox"))
        {
            buttonUp.SetActive(false);
            buttonDown.SetActive(true);

            SceneFunction(true);
        }
    }
     * */
    void OnTriggerExit(Collider coll)
    {
        if (oneTime)
            return;

        if (coll.CompareTag("Player") || coll.CompareTag("PushBox"))
        {
            buttonUp.SetActive(true);
            buttonDown.SetActive(false);

            SceneFunction(false);
        }
    }
    void TriggerInitialization()
    {
        switch (sceneName)
        {
            case "GRASS02":
                switch(myName)
                {
                    case ButtonName.GreenButton:
                        targetTransform.position += new Vector3(0, -1, 0);
                        break;
                }
                break;

            case "GRASS03":
                switch(myName)
                {
                    case ButtonName.GreenButton:
                        targetTransform.position = new Vector3(0, -1.2f, 0);
                        break;
                }
                
                break;

            case "Combination":
                switch(myName)
                {
                    case ButtonName.GreenButton:
                    case ButtonName.RedButton:
                        targetTransform.position += new Vector3(0, -1.0f, 0);
                        break;
                }
                break;

            case "Cave":
                switch (myName)
                {
                    case ButtonName.RedButton:
                        targetTransform.Rotate(0, 180, 0);
                        break;

                    case ButtonName.GreenButton:
                        targetTransform.position = new Vector3(0, -1, -2);
                        break;

                    case ButtonName.YellowButton:
                        targetTransform.position = new Vector3(3, -1, 3);
                        target[1].transform.position = new Vector3(3, -1, -3);
                        break;

                    case ButtonName.BlueButton:
                        targetTransform.Rotate(0, 180, 0);
                        break;
                }
                break;

            case "HurryUp":
                switch(myName)
                {
                    case ButtonName.YellowButton:
                        targetTransform.position += new Vector3(0, -1, 0);
                        break;
                }
                break;

            case "Castle1":
                switch(myName)
                {
                    case "BlueButton01":
                        target[1].transform.position += new Vector3(0, -1.5f, 0);
                        break;

                    case "RedTrigger":
                        targetTransform.position += new Vector3(0, -1.5f, 0);
                        break;
                }
                break;

            case "temp":
                switch(myName)
                {
                    case ButtonName.GreenButton:
                    case ButtonName.BlueButton:
                    case ButtonName.RedButton:
                        targetTransform.position += new Vector3(0, 1, 0);
                        break;
                }
                break;

            //시연용
            case "HELL01":
                switch (myName)
                {
                    case ButtonName.GreenButton:
                    case ButtonName.RedButton:
                        targetTransform.position += new Vector3(0, -1.0f, 0);
                        break;

                    case ButtonName.YellowButton:
                        targetTransform.position += new Vector3(0, -1.0f, 0);
                        target[1].transform.position += new Vector3(0, -1.0f, 0);
                        target[2].transform.position += new Vector3(0, -1.0f, 0);
                        break;
                }
                break;

            case "DESERT01":
                switch(myName)
                {
                    case ButtonName.RedButton:
                        targetNavMeshObstacle.enabled = false;
                        break;
                }
                break;
        }
    }

    void SceneFunction(bool triggerEnter)
    {
        switch(sceneName)
        {
            case "GRASS02":
                MovingStar(triggerEnter);
                break;

            case "GRASS03":
                StartCoroutine(this.PushBox(triggerEnter));
                break;

            case "STAGE05 LetsWarp":
                StartCoroutine(this.LetsWarp());
                break;

            case "Combination":
                StartCoroutine(this.Combination(triggerEnter));
                break;

            case "Cave":
                StartCoroutine(this.Cave(triggerEnter));
                break;

            case "EnterTheCastle":
                StartCoroutine(this.EnterTheCastle(triggerEnter));
                break;

            case "HurryUp":
                StartCoroutine(this.HurryUp());
                break;

            case "Castle1":
                StartCoroutine(this.Castle1(triggerEnter));
                break;

            case "temp":
                StartCoroutine(this.temp(triggerEnter));
                break;

            //시연용
            case "HELL01":
                StartCoroutine(this.HELL01(triggerEnter));
                break;

            case "DESERT01":
                StartCoroutine(this.DESERT01(triggerEnter));
                break;

            case "ICE01":
                StartCoroutine(this.ICE01());
                break;
        }
    }

    void MovingStar(bool triggerEnter)
    {
        if (triggerEnter)
        {
            targetTransform.DOMoveY(0.12f, 0.2f);
            targetNavMeshObstacle.enabled = false;
        }
        else
        {
            targetTransform.DOMoveY(-1.12f, 0.2f);
            targetNavMeshObstacle.enabled = true;
        }
    }
    IEnumerator PushBox(bool triggerEnter)
    {
        switch(myName)
        {
            case ButtonName.GreenButton:
            case ButtonName.BlueButton:
            case ButtonName.RedButton:
            case ButtonName.YellowButton:
                targetTransform.DOMoveY(targetTransform.position.y + 0.3f, 0.2f);
                break;

            case "RigidbodyTrigger":
                target[0].AddComponent<Rigidbody>();
                target[0].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                break;
        }
       
        yield return new WaitForSeconds(0.5f);

        if(targetTransform.position.y == 0)
            targetNavMeshObstacle.enabled = false;
        else
            targetNavMeshObstacle.enabled = true;
    }
    IEnumerator LetsWarp()
    {
        target[0].GetComponent<NavMeshObstacle>().enabled = true;
        target[1].GetComponent<NavMeshObstacle>().enabled = true;
        target[2].GetComponent<NavMeshObstacle>().enabled = true;

        target[0].transform.DOMoveY(-1.0f, 0.3f);
        yield return new WaitForSeconds(0.5f);
        target[1].transform.DOMoveY(-1.0f, 0.3f);
        yield return new WaitForSeconds(0.5f);
        target[2].transform.DOMoveY(-1.0f, 0.3f);
        yield return new WaitForSeconds(0.5f);
    }
    IEnumerator Combination(bool triggerEnter)
    {
        switch(myName)
        {
            case ButtonName.GreenButton:
                if (triggerEnter)
                {
                    target[0].transform.DOMoveY(0, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = false;
                }
                break;

            case ButtonName.YellowButton:
                if (triggerEnter)
                {
                    target[0].GetComponent<NavMeshObstacle>().enabled = false;
                    target[1].GetComponent<NavMeshObstacle>().enabled = false;
                    target[2].GetComponent<NavMeshObstacle>().enabled = false;

                    target[0].transform.DOMoveY(0.12f, 0.3f);
                    yield return new WaitForSeconds(0.5f);
                    target[1].transform.DOMoveY(0.12f, 0.3f);
                    yield return new WaitForSeconds(0.5f);
                    target[2].transform.DOMoveY(0.12f, 0.3f);
                    yield return new WaitForSeconds(0.5f);
                }
                break;

            case ButtonName.BlueButton:
                if (triggerEnter)
                {
                    target[0].transform.DOMoveY(-1.0f, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = true;
                }
                break;

            case ButtonName.RedButton:
                if (triggerEnter)
                {
                    target[0].transform.DOMoveY(0, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = false;
                }
                else
                {
                    target[0].transform.DOMoveY(-1.0f, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = true;
                }
                break;
        }
    }
    IEnumerator Cave(bool triggerEnter)
    {
        switch (myName)
        {
            case ButtonName.RedButton:
                if (triggerEnter)
                {
                    targetTransform.DORotate(new Vector3(0, 90, 0), 0.5f);
                    targetNavMeshObstacle.enabled = false;
                    break;
                }
                else
                {
                    targetTransform.DORotate(new Vector3(0, -90, 0), 0.5f);
                    targetNavMeshObstacle.enabled = true;
                    break;
                }
                

            case ButtonName.GreenButton:
                targetTransform.DOMoveY(0, 0.5f);
                targetNavMeshObstacle.enabled = false;
                break;

            case ButtonName.YellowButton:
                //[0] : BlueButton, [1] : GoalTriggerFootSwitch
                targetTransform.DOMoveY(0.1f, 0.5f);
                target[1].transform.DOMoveY(0, 0.5f);
                yield return new WaitForSeconds(3f);
                targetTransform.DOMoveY(-1, 0.5f);
                target[0].transform.DOMoveY(-1, 0.5f);
                break;

            case ButtonName.BlueButton:
                targetTransform.DORotate(new Vector3(0, 90, 0), 0.5f);
                targetNavMeshObstacle.enabled = false;
                break;

            case "RigidbodyTrigger":
                target[0].AddComponent<Rigidbody>();
                target[0].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                break;
        }
        yield return null;
    }
    IEnumerator EnterTheCastle(bool triggerEnter)
    {
        switch(myName)
        {
            case ButtonName.BlueButton:
            case ButtonName.RedButton:
            case ButtonName.YellowButton:
            case ButtonName.GreenButton:
                if (triggerEnter)
                {
                    targetTransform.DOMoveY(-1, 0.5f);
                    targetNavMeshObstacle.enabled = true;
                    yield return new WaitForSeconds(0.2f);
                    target[1].transform.DOMoveY(-1, 0.5f);
                    target[1].GetComponent<NavMeshObstacle>().enabled = true;
                }
                else
                {
                    targetTransform.DOMoveY(0.12f, 0.5f);
                    targetNavMeshObstacle.enabled = false;
                    yield return new WaitForSeconds(0.2f);
                    target[1].transform.DOMoveY(0.12f, 0.5f);
                    target[1].GetComponent<NavMeshObstacle>().enabled = false;
                }
                break;
        }
        yield return null;
    }
    IEnumerator HurryUp()
    {
        switch(myName)
        {
            case ButtonName.YellowButton:
                myName = "use";
                targetTransform.DOMoveY(0.6f, 0.5f);
                target[1].transform.DOMoveY(-0.5f, 0.5f);
                target[1].GetComponent<NavMeshObstacle>().enabled = false;

                yield return new WaitForSeconds(8.0f);
                targetTransform.DOMoveY(-1f, 0.5f);

                yield return new WaitForSeconds(35.0f);
                target[1].transform.DOMoveY(1.0f, 0.5f);
                target[1].GetComponent<NavMeshObstacle>().enabled = true;
                break;

            case ButtonName.GreenButton:
                myName = "use";
                targetTransform.DOMoveY(-0.5f, 0.5f);
                targetNavMeshObstacle.enabled = false;
                
                yield return new WaitForSeconds(10.0f);
                targetTransform.DOMoveY(1.0f, 0.5f);
                targetNavMeshObstacle.enabled = true;;
                break;

            case ButtonName.BlueButton:
                myName = "use";
                targetTransform.DOMoveY(-0.5f, 0.5f);
                targetNavMeshObstacle.enabled = false;
                
                yield return new WaitForSeconds(10.0f);
                targetTransform.DOMoveY(1.0f, 0.5f);
                targetNavMeshObstacle.enabled = true;;
                break;

            case ButtonName.RedButton:
                myName = "use";
                targetTransform.DOMoveY(-0.5f, 0.5f);
                targetNavMeshObstacle.enabled = false;
                
                yield return new WaitForSeconds(10.0f);
                targetTransform.DOMoveY(1.0f, 0.5f);
                targetNavMeshObstacle.enabled = true;;
                break;
        }
        yield return null;
    }
    IEnumerator Castle1(bool triggerEnter)
    {
        switch(myName)
        {
            case ButtonName.RedButton:
                targetTransform.DOMoveY(-0.5f, 0.5f);
                target[1].transform.DOMoveY(-0.5f, 0.5f);

                yield return new WaitForSeconds(10.0f);
                targetTransform.DOMoveY(1.0f, 0.5f);
                target[1].transform.DOMoveY(1.0f, 0.5f);
                break;
            case ButtonName.GreenButton:
                targetTransform.DOMoveY(-0.5f, 0.5f);
                break;

            case "BlueButton01":
            case "BlueButton02":
            case "BlueButton03":
                if (triggerEnter)
                {
                    targetTransform.DOMoveY(-0.5f, 0.5f);
                    target[1].transform.DOMoveY(1.0f, 0.1f);
                }
                    
                else
                    targetTransform.DOMoveY(1.0f, 0.5f);
                break;

            case ButtonName.YellowButton:
                targetTransform.DOMoveY(-0.5f, 0.5f);

                yield return new WaitForSeconds(10.0f);
                targetTransform.DOMoveY(1.0f, 0.5f);
                break;

            case "RedTrigger":
                myName = "use";
                targetTransform.DOMoveY(1.0f, 2.0f);
                break;
        }
        yield return null;
    }
    IEnumerator temp(bool triggerEnter)
    {
        switch(myName)
        {
            case ButtonName.GreenButton:
                targetTransform.DOMoveY(0, 0.5f);
                targetNavMeshObstacle.enabled = false;
                myName = "use";
                yield return new WaitForSeconds(13.0f);
                targetTransform.DOMoveY(1, 0.5f);
                targetNavMeshObstacle.enabled = true;
                myName = "GreenButton";
                break;

            case ButtonName.RedButton:
                targetTransform.DOMoveY(0, 0.5f);
                targetNavMeshObstacle.enabled = false;
                break;

            case ButtonName.BlueButton:
                if(triggerEnter)
                {
                    targetTransform.DOMoveY(0, 0.5f);
                    targetNavMeshObstacle.enabled = false;
                }
                else
                {
                    targetTransform.DOMoveY(1, 0.5f);
                    targetNavMeshObstacle.enabled = true;
                }
                break;
        }
        yield return null;
    }

    //시연용
    IEnumerator HELL01(bool triggerEnter)
    {
        switch (myName)
        {
            case ButtonName.GreenButton:
                if (triggerEnter)
                {
                    target[0].transform.DOMoveY(0, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = false;
                }
                break;

            case ButtonName.YellowButton:
                if (triggerEnter)
                {
                    target[0].GetComponent<NavMeshObstacle>().enabled = false;
                    target[1].GetComponent<NavMeshObstacle>().enabled = false;
                    target[2].GetComponent<NavMeshObstacle>().enabled = false;

                    target[0].transform.DOMoveY(0.12f, 0.3f);
                    yield return new WaitForSeconds(0.5f);
                    target[1].transform.DOMoveY(0.12f, 0.3f);
                    yield return new WaitForSeconds(0.5f);
                    target[2].transform.DOMoveY(0.12f, 0.3f);
                    yield return new WaitForSeconds(0.5f);
                }
                break;

            case ButtonName.BlueButton:
                if (triggerEnter)
                {
                    target[0].transform.DOMoveY(-1.0f, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = true;
                }
                break;

            case ButtonName.RedButton:
                if (triggerEnter)
                {
                    target[0].transform.DOMoveY(0, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = false;
                }
                else
                {
                    target[0].transform.DOMoveY(-1.0f, 0.5f);
                    target[0].GetComponent<NavMeshObstacle>().enabled = true;
                }
                break;
        }
    }
    IEnumerator DESERT01(bool triggerEnter)
    {
        switch(myName)
        {
            case ButtonName.GreenButton:
                //지속성
                if(triggerEnter)
                    targetTransform.DOMoveY(0.5f, 0.5f);
                else
                    targetTransform.DOMoveY(-0.5f, 0.5f);
                break;

            case ButtonName.BlueButton:
                //1회성
                oneTime = true;
                targetTransform.DOMoveY(1.0f, 0.5f);
                break;

            case ButtonName.RedButton:
                //1회성
                oneTime = true;
                target[1].transform.DOMoveY(-3.0f, 0.5f);
                yield return new WaitForSeconds(0.2f);
                targetTransform.DOMoveY(-3.0f, 0.5f);
                targetNavMeshObstacle.enabled = true;
                break;
        }
        yield return null;
    }
    IEnumerator ICE01()
    {
        target[0].GetComponent<NavMeshObstacle>().enabled = true;
        target[1].GetComponent<NavMeshObstacle>().enabled = true;
        target[2].GetComponent<NavMeshObstacle>().enabled = true;

        target[0].transform.DOMoveY(-1.0f, 0.3f);
        yield return new WaitForSeconds(0.5f);
        target[1].transform.DOMoveY(-1.0f, 0.3f);
        yield return new WaitForSeconds(0.5f);
        target[2].transform.DOMoveY(-1.0f, 0.3f);
        yield return new WaitForSeconds(0.5f);
    }
}
