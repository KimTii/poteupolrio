﻿using UnityEngine;
using System.Collections;

public class Swamp : MonoBehaviour {

    void OnTriggerEnter(Collider coll)
    {
        //Player의 속도를 늦춘다
        if(coll.CompareTag("Player"))
        {
            coll.GetComponent<NavMeshAgent>().speed = 1.0f;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        //Player의 속도를 정상화시킨다.
        if (coll.CompareTag("Player"))
        {
            coll.GetComponent<NavMeshAgent>().speed = 3.5f;
        }
    }
}
