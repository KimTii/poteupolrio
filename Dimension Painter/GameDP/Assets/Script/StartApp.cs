﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using GooglePlayGames.BasicApi;


public class StartApp {
    static public class DebugModeSetting
    {
        static public bool deBugingModeOn = true;
        static public bool deBugingModeOff = false;
    }


    // constinue Memory saved
    private static StartApp instance = null;
    public static StartApp Instance
   {
        get
        {
            if (instance == null)
                instance = new StartApp() ;
            return instance;
        }
    }
    public bool titleOnTrigger = false;
    public bool newGameStart = false;
    public bool deBugModeDrag;

    public void SetTitleOn(bool _b) { titleOnTrigger = _b; }
    public bool GetTitleOn() { return titleOnTrigger; }
    public void DeBugModeOnOff(bool _d) { deBugModeDrag = DebugModeSetting.deBugingModeOn; }
    public bool GetDebugModeDrag() { return deBugModeDrag; }

    // 새로운 게임을 시작
    public bool GetNewGameStartInfo() 
    {

        newGameStart = true;
        return true;
    }
}

public class GPGmng
{
    private static GPGmng Getinstance = null;
    public static GPGmng Instance
    {
        get
        {
            if (Getinstance == null)
                Getinstance = new GPGmng();
            return Getinstance;
        }
    }

    // 로그인 판별 드래그 값
    public bool bLogin
    {
        get;
        set;
    }

    // 첫 시작 GPG 초기화
    public void InitializeGPGS()
    {
        bLogin = false;

        PlayGamesPlatform.Activate();
    }

    // GPGS 로그인
    public void LoginGPGS()
    {
        // 로그인이 안되어 있다면
        if (!Social.localUser.authenticated)
            Social.localUser.Authenticate(LoginCallBackGPGS);
    }

    // 로그 아웃
    public void LogOutGPGS()
    {
        // 로그인이 되어 있다면
        if (Social.localUser.authenticated)
        {
            ((GooglePlayGames.PlayGamesPlatform)Social.Active).SignOut();
            bLogin = false;
        }
    }

    // GPGS 에서 자신의 프로필 사진을 가져 옵니다.
    public Texture2D GetImageGPGS()
    {
        // 로그인이 되어있다면 이미지를 리턴하고 아니면 널을 리턴
        if (Social.localUser.authenticated)
            return Social.localUser.image;
        else
            return null;
    }

    // GPGS 에서 사용자의 이름을 가져옵니다.
    public string GetNameGPGS()
    {
        if (Social.localUser.authenticated)
            return Social.localUser.userName;
        else
            return null;
    }

    // 자신의 업적&리더보드 UI를 불러 옵니다.
    public void ShowAchievmentUI() { Social.ShowAchievementsUI(); }
    public void ShowLeaderBoard() { Social.ShowLeaderboardUI(); }

    // 로그인 드래그 콜백함수
    public void LoginCallBackGPGS(bool result) { bLogin = result; }
}


// Game Data Management
public class GameData
{
    public int mIstageInfo;
    public string mSselectStage;

    private static GameData instance = null;
    public static GameData Instance
    {
        get
        {
            if (instance == null)
                instance = new GameData();
            return instance;
        }
    }

    public void SetStageInfo(int mIfile) { mIstageInfo = mIfile; }
    public void NextStageInfo() { mIstageInfo++; }
    public void SetSelectStage(string _select) { mSselectStage = _select; }

    public int GetStageInfo() { return mIstageInfo;  }
    public string GetSelectStage() { return mSselectStage;  }
    
}
