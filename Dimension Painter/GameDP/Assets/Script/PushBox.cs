﻿using UnityEngine;
using System.Collections;

public class PushBox : MonoBehaviour {

    bool forward, back, right, left;
    Ray forwardRay, backRay, rightRay, leftRay;
    RaycastHit forwardHit, backHit, rightHit, leftHit;

	// Use this for initialization
	void Awake () {
        forward = back = right = left = true;
        
        //StartCoroutine(this.MyUpdate());
	}

    IEnumerator MyUpdate()
    {
        while(true)
        {
            //print(this.name + " : " + forward + ", " + back + ", " + right + ", " + left);

            forwardRay.origin = backRay.origin = rightRay.origin = leftRay.origin = transform.position;
            forwardRay.direction = Vector3.forward;
            backRay.direction = Vector3.back;
            rightRay.direction = Vector3.right;
            leftRay.direction = Vector3.left;

            Physics.Raycast(forwardRay, out forwardHit, 1.0f);
            Physics.Raycast(backRay, out backHit, 1.0f);
            Physics.Raycast(rightRay, out rightHit, 1.0f);
            Physics.Raycast(leftRay, out leftHit, 1.0f);

            Debug.DrawRay(forwardRay.origin, forwardRay.direction, Color.red);
            Debug.DrawRay(backRay.origin, backRay.direction, Color.yellow);
            Debug.DrawRay(rightRay.origin, rightRay.direction, Color.yellow);
            Debug.DrawRay(leftRay.origin, leftRay.direction, Color.yellow);

            if (forwardHit.collider != null)
            {
                if (forwardHit.collider.CompareTag("PushBox") || forwardHit.collider.CompareTag("NoPush"))
                    forward = false;
            }
            else { forward = true; }
                

            if (backHit.collider != null)
            {
                if (backHit.collider.CompareTag("PushBox") || backHit.collider.CompareTag("NoPush"))
                    back = false;
            }
            else { back = true; }
                
            if (rightHit.collider != null)
            {
                if (rightHit.collider.CompareTag("PushBox") || rightHit.collider.CompareTag("NoPush"))
                    right = false;
            }
            else { right = true; }
                
            if (leftHit.collider != null)
            {
                if (leftHit.collider.CompareTag("PushBox") || leftHit.collider.CompareTag("NoPush"))
                    left = false;
                print(leftHit.collider.tag);
                print(leftHit.collider.gameObject.transform.name);
            }
            else { left = true; }
                
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void CheckPushBox()
    {
        //print(this.name + " : " + forward + ", " + back + ", " + right + ", " + left);

        forwardRay.origin = backRay.origin = rightRay.origin = leftRay.origin = transform.position;
        forwardRay.direction = Vector3.forward;
        backRay.direction = Vector3.back;
        rightRay.direction = Vector3.right;
        leftRay.direction = Vector3.left;

        Physics.Raycast(forwardRay, out forwardHit, 1.0f);
        Physics.Raycast(backRay, out backHit, 1.0f);
        Physics.Raycast(rightRay, out rightHit, 1.0f);
        Physics.Raycast(leftRay, out leftHit, 1.0f);

        Debug.DrawRay(forwardRay.origin, forwardRay.direction, Color.red);
        Debug.DrawRay(backRay.origin, backRay.direction, Color.yellow);
        Debug.DrawRay(rightRay.origin, rightRay.direction, Color.yellow);
        Debug.DrawRay(leftRay.origin, leftRay.direction, Color.yellow);

        if (forwardHit.collider != null)
        {
            if (forwardHit.collider.CompareTag("PushBox") || forwardHit.collider.CompareTag("NoPush"))
                forward = false;
            else
                forward = true;

            //print("HitColl : " + forwardHit.collider.tag + " BoolForward : " + forward);
        }
        else { forward = true; }


        if (backHit.collider != null)
        {
            if (backHit.collider.CompareTag("PushBox") || backHit.collider.CompareTag("NoPush"))
                back = false;
            else
                back = true;
        }
        else { back = true; }

        if (rightHit.collider != null)
        {
            if (rightHit.collider.CompareTag("PushBox") || rightHit.collider.CompareTag("NoPush"))
                right = false;
            else
                right = true;
        }
        else { right = true; }

        if (leftHit.collider != null)
        {
            if (leftHit.collider.CompareTag("PushBox") || leftHit.collider.CompareTag("NoPush"))
                left = false;
            else
                left = true;
            //print(leftHit.collider.tag);
            //print(leftHit.collider.gameObject.transform.name);
        }
        else { left = true; }
    }
    public bool GetForward() { return forward; }
    public bool GetBack() { return back; }
    public bool GetRight() { return right; }
    public bool GetLeft() { return left; }
}
