﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {

    public Transform pen;
    bool penFollow;

    //Getcomponent 미리 선언
    CameraManager cameraManager;
    PlayerManager playerManager;
    //Transform 미리 선언
    Transform pickObjectTransform;

    //RayCast구현 관련 변수
    Ray ray;
    RaycastHit hit;
    RaycastHit[] hits;
    //Target까지 다가갈때 도착 했는지 확인할 유효거리
    float playerMovePositionDistance;
    float pushBoxDistance;
    //PickObject는 선택이 되었는지 확인이 필요한 경우에만 사용한다.
    GameObject pickObject;
    //터치 할수 있음 없음? 3D에서 사용가능
    bool touchEnabled;
    //Camera Control을 위한 변수
    CameraManager.TouchState touchState;
    //DoubleTouch
    float doubleTouchSecond;
    float lastTouchTime;

    //해상도 설정
    int width;
        

	// Use this for initialization
    void Start() {
        Screen.SetResolution(Screen.width, Screen.width / 16 * 9, true);
        //Screen.SetResolution(1280, 720, true);
        Application.targetFrameRate = 60;

        penFollow = false;

        cameraManager = GetComponent<CameraManager>();
        playerManager = GetComponent<PlayerManager>();
        playerMovePositionDistance = 0.45f;
        pushBoxDistance = 1.1f;
        pickObject = null;
        touchEnabled = true;
        doubleTouchSecond = 0.2f;
        lastTouchTime = 0;

        StartCoroutine(this.MyUpdate());
	}

    IEnumerator MyUpdate()
    {
        while (true) {
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);

            if (touchEnabled)
            {
                //Camera Control 관련
                touchState = cameraManager.GetTouchState();

                if (Input.GetMouseButtonDown(0) && Input.touchCount <= 1)//Input.GetMouseButtonDown(0) && Input.touchCount <= 1)
                {
                    //초기화
                    cameraManager.SetTouchState(CameraManager.TouchState.Wait);
                    cameraManager.CameraSwipeInitialization();
                    //UI가 선택시 무시되도록 하는 코드//
                    if (IsPointerOverUIObject())
                    {
                        cameraManager.SetTouchState(CameraManager.TouchState.UI);
                    }
                }
                else if (Input.GetMouseButton(0) && Input.touchCount <= 1)
                {
                    //계속해서 마우스의 좌표를 저장한다.
                    //Distance가 일정범위 이상으로 벌어지면 Swipe한다.
                    if (touchState == CameraManager.TouchState.Wait || touchState == CameraManager.TouchState.Swipe)
                    {
                        cameraManager.DragAndSwipe();
                    }

                }
                else if (Input.GetMouseButtonUp(0) && Input.touchCount <= 1)
                {
                    //StartCoroutine(this.DoubleTouchTimer());
                    //TouchState가 Swipe인 경우 꺼져.
                    //TouchState가 Wait인 경우에만 SingleTouch를 작동시킨다.
                    if (touchState == CameraManager.TouchState.Wait)
                        SingleTouch();
                    
                }
                else if (Input.touchCount == 2)
                {
                    cameraManager.SetTouchState(CameraManager.TouchState.MultiTouch);
                    cameraManager.PinchZoomInOut();
                }
            }

            //캐릭터가 도착지점에 도착했나?
            if (PlayerIsArrive(playerManager.GetPlayerMovePosition(), playerMovePositionDistance))
            {
                //Player Run Animation OFF
                playerManager.SetPlayerAnimatorIdle();
                if (pickObject.CompareTag("PaintWall"))
                {
                    if (pickObject.GetComponent<PaintWall>().CheckPaintWallWarpState() && playerManager.GetWarp())
                    {
                        //워프이동 함수
                        playerManager.EnterWarp(pickObject);
                    }
                }
            }
            yield return new WaitForSeconds(0.0005f);
            //print(pickObject);
        }
    }

    IEnumerator DoubleTouchTimer()
    {
        //print("DoubleTouchTimer");
        while((Time.time - lastTouchTime) < doubleTouchSecond)
        {
            //print("DoubleTouch!!");
            cameraManager.FollowPlayer();
            yield return null;
        }
        lastTouchTime = Time.time;
    }

    bool ScreenPointToRay()
    {
        if (touchEnabled == false)
            return false;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if(Physics.Raycast(ray, out hit))
        {
            if (hit.collider == null)
                return false;

            pickObjectTransform = hit.transform;
            pickObject = pickObjectTransform.gameObject;
            //print(pickObject.name);

            return true;
        }
        print("ScreenPointToRay Error!!");
        return false;
    }
    void SingleTouch()
    {
        //print("EnterSingleTouch");
        // 레이캐스트를 쏘고
        if (!ScreenPointToRay())
        {
            StartCoroutine(this.DoubleTouchTimer());
            return;
        }
            

        // Check the Tag and Do It!
        if (pickObject.CompareTag("Map"))
        {
            pen.GetComponent<Pen>().StopPenAnimation();
            pen.position = new Vector3(999, 999, 999);
            pen.GetComponent<Pen>().StartPenAnimation();
            playerManager.Move(hit.point);
            //한칸씩이동
            //playerManager.Move(hit.collider.gameObject.transform.position + new Vector3(0, 0.92f, 0));
        }
            
        else if (pickObject.CompareTag("PaintWall"))
        {
            pen.GetComponent<Pen>().StopPenAnimation();
            pen.position = pickObjectTransform.position + new Vector3(0, 1.5f, 0);
            pen.GetComponent<Pen>().StartPenAnimation();
            playerManager.Approach(pickObject);
        }
            
        else if (pickObject.CompareTag("PushBox"))
        {
            if (PlayerIsArrive(pickObjectTransform.position, pushBoxDistance))
            {
                pen.GetComponent<Pen>().StopPenAnimation();
                pen.position = new Vector3(999, 999, 999);
                pen.GetComponent<Pen>().StartPenAnimation();
                playerManager.PushBox();
                
            }
                
            else
            {
                pen.GetComponent<Pen>().StopPenAnimation();
                pen.position = pickObjectTransform.position + new Vector3(0, 1.5f, 0);
                pen.GetComponent<Pen>().StartPenAnimation();
                playerManager.Approach(pickObject);
            }       
        }

        else if (pickObject.CompareTag("LeverController"))
        {
            pen.GetComponent<Pen>().StopPenAnimation();
            pen.position = pickObjectTransform.position + new Vector3(0, 1.5f, 0);
            pen.GetComponent<Pen>().StartPenAnimation();
            playerManager.Move(pickObjectTransform.position + new Vector3(0, 0.4f, 0));
        }

        else if (pickObject.CompareTag("DoorButton"))
        {
            pen.GetComponent<Pen>().StopPenAnimation();
            pen.position = pickObjectTransform.position + new Vector3(0, 1.5f, 0);
            pen.GetComponent<Pen>().StartPenAnimation();
            playerManager.Move(pickObjectTransform.position + new Vector3(0, 0.1f, 0));
        }
       
        else
        {
            pen.GetComponent<Pen>().StopPenAnimation();
            pen.position = new Vector3(999, 999, 999);
            pen.GetComponent<Pen>().StartPenAnimation();
        }

        //Debug.Log(pickObject.name);
    }

    public IEnumerator StopTouch(float time)
    {
        //print("STOP TOUCH ON!!");
        touchEnabled = false;
        yield return new WaitForSeconds(time);
        //print("STOP TOUCH OFF!!");
        touchEnabled = true;
    }

    public bool PlayerIsArrive(Vector3 pickObjectPosition, float distance)
    {
        //print("PlayerPosition : " + playerManager.GetPlayerPosition() + "PickObjectPosition : " + pickObjectPosition + (Vector3.Distance(playerManager.GetPlayerPosition(), pickObjectPosition) < distance));
        if (Vector3.Distance(playerManager.GetPlayerPosition(), pickObjectPosition) < distance)
            return true;
        else
            return false;
    }

    //PlayerManager에서 Skill구현시 필요함
    public GameObject GetPickObject() { return pickObject; }

    //워프 그리고나서 바로 워프탐 ㅜㅜ
    public void SetPickobject(GameObject setPickObject) { pickObject = setPickObject; }
    //PlayerManager에서 Warp()에 필요
    public float GetPlayerMovePositionDistance() { return playerMovePositionDistance; }

    private bool IsPointerOverUIObject()
    {
        // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
        // the ray cast appears to require only eventData.position.
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
 
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    /*
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 1000, 1000), SystemInfo.deviceModel);
        GUI.Label(new Rect(100, 100, 1000, 1000), SystemInfo.deviceName);
        GUI.Label(new Rect(200, 200, 1000, 1000), SystemInfo.deviceType.ToString());
    }
     */
}
