﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TouchEvent : MonoBehaviour {
    public enum TouchState
    {
        Wait,
        Swipe,
        SingleTouch,
        UI
    }
    public TouchState touchState = TouchState.Wait;

    public GameObject planet;
    string stageSelectName = null;
    bool stageSelectDrag = false;

    Ray ray;
    RaycastHit hit;

    // Planet Rotation
    Vector2 beganPosition;
    Vector2 endPosition;
    Vector2 deltaPosition;
    Vector2 swipeBeganPosition;
    float distancePosition;
    float beganTime;
    float endTime;
    float deltaTime;
    float currentSpeed;
    float rotationMagnification;
    //시작터치와 마지막터치의 거리값을 이용하여, 일반 터치의 감도를 조절한다.
    float touchDistance;

    bool isDragging;

    //
    float deltaPositionX;
    float deltaPositionY;

    //허용오차
    float minTolerance;
    float maxTolerance;

    //터치 허용 여부
    bool touchEnabled;
    bool touchEnabledIland = true;

    //
    GameObject pickObject;

	// Use this for initialization
	void Start () {
        rotationMagnification = 0.2f;
        touchDistance = 1.0f;
        isDragging = false;

        minTolerance = 20.0f;
        maxTolerance = 300;

        StartCoroutine(this.MyUpdate());
	}

    IEnumerator MyUpdate()
    {
        while(true)
        {
            //Debug.Log("Call_MyYdate()");
            /*
            if (Input.GetMouseButtonDown(0) && isDragging == false )
            {
                beganPosition = Input.mousePosition;
                beganTime = Time.time;
                currentSpeed = 1;
            }
            else if (Input.GetMouseButton(0) && this.gameObject.GetComponent<SecStageAnimation>().GetTitleAniDone() == true )
            {
                DragAndSwipe();
            }

            if (Input.GetMouseButtonUp(0))
            {
                NomalyTouch();
            }
            */
            if (Input.GetMouseButtonDown(0) && Input.touchCount <= 1)//Input.GetMouseButtonDown(0) && Input.touchCount <= 1)
            {
                //초기화
                touchState = TouchState.Wait;
                beganPosition = Input.mousePosition;
                swipeBeganPosition = Input.mousePosition;
                beganTime = Time.time;
                currentSpeed = 1;

                //UI가 선택시 무시되도록 하는 코드//
                
                if (IsPointerOverUIObject())
                {
                    touchState = TouchState.UI;
                }
                 
            }
            else if (Input.GetMouseButton(0) && Input.touchCount <= 1)
            {
                //계속해서 마우스의 좌표를 저장한다.
                //Distance가 일정범위 이상으로 벌어지면 Swipe한다.
                if (touchState == TouchState.Wait || touchState == TouchState.Swipe)
                {
                    DragAndSwipe();
                }

            }
            else if (Input.GetMouseButtonUp(0) && Input.touchCount <= 1)
            {
                //TouchState가 Swipe인 경우 꺼져.
                //TouchState가 Wait인 경우에만 SingleTouch를 작동시킨다.
                StartCoroutine(this.PlanetSwipeSliding());
                if (touchState == TouchState.Wait)
                {
                    touchState = TouchState.SingleTouch;
                    SingleTouch();
                }
            }
            yield return null;
        }
    }

    void DragAndSwipe()
    {
        

        /*
        endPosition = Input.mousePosition;
        if (Vector2.Distance(beganPosition, endPosition) > touchDistance)
        {
            isDragging = true;
            //ColliderEnabledFalse("NotAll");
        }
        else
        {
            return;
        }

        //print("Dragging");
        //DistanceTouch가 0이 아닐때 ( 드래그 앤 스와이프 )
        deltaPosition.x = endPosition.x - beganPosition.x;
        deltaPosition.y = endPosition.y - beganPosition.y;
        deltaPosition.Normalize();

        planet.gameObject.transform.RotateAround(Vector3.zero, new Vector2(deltaPosition.y, -deltaPosition.x), Time.deltaTime * (currentSpeed * rotationMagnification));


        //Draging~
        //DeltaPosition = EndPosition - BeganPosition;
        //DeltaTime = EndTime - BeganTime;
        //DeltaSpeed = EndSpeed - BeganSpeed;

        endTime = Time.time;

        deltaTime = endTime - beganTime;
        distancePosition = Vector2.Distance(endPosition, beganPosition);

        currentSpeed = distancePosition / deltaTime;

        beganPosition = endPosition;
        beganTime = endTime;

        //print("deltaPosition :" + deltaPosition + " deltaTime :" + deltaTime);
        //print("currentSpeed :" + currentSpeed);
         */
        if (!GetComponent<SecStageAnimation>().GetTitleAniDone())
            return;

        endPosition = Input.mousePosition;

        if ((Vector2.Distance(swipeBeganPosition, endPosition) > minTolerance && Vector2.Distance(swipeBeganPosition, endPosition) < maxTolerance) || touchState == TouchState.Swipe)
        {
            touchState = TouchState.Swipe;
            deltaPositionX = (endPosition.x - swipeBeganPosition.x);
            deltaPositionY = (endPosition.y - swipeBeganPosition.y);
            deltaPosition = new Vector3(deltaPositionX, deltaPositionY, 0);
            deltaPosition *= Time.deltaTime;
            deltaPosition.Normalize();

            planet.gameObject.transform.RotateAround(Vector3.zero, new Vector2(deltaPosition.y, -deltaPosition.x), Time.deltaTime * (currentSpeed * rotationMagnification));

            //Draging~
            //DeltaPosition = EndPosition - BeganPosition;
            //DeltaTime = EndTime - BeganTime;
            //DeltaSpeed = EndSpeed - BeganSpeed;

            endTime = Time.time;

            deltaTime = endTime - beganTime;
            distancePosition = Vector2.Distance(endPosition, beganPosition);

            currentSpeed = distancePosition / deltaTime;

            beganPosition = endPosition;
            beganTime = endTime;

            //print("deltaPosition :" + deltaPosition + " deltaTime :" + deltaTime);
            //print("currentSpeed :" + currentSpeed);

            swipeBeganPosition = Input.mousePosition;
        }
    }
    void SingleTouch()
    {
        //print(currentSpeed);
        /*
        //Drag 종료
        if (isDragging == true)
        {
            isDragging = false;
            endPosition = Input.mousePosition;
            return;
        }

        // 일반 터치 시작
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if( hit.collider.gameObject.CompareTag("Map"))
            {
                Debug.Log("Onclick");
                // 스테이지 선택 입장

                if (this.gameObject.GetComponent<SecStageAnimation>().GetTitleAniDone())
                {
                    switch (hit.collider.gameObject.name)
                    {
                        case "GrassLand":
                            StageSelectAnimation();
                            break;
                        case "NullCollider":
                            GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
                            GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 1f).OnUpdate( () => GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 0f));
                            
                            GameObject.Find("StageButton").transform.DOLocalMoveX(-1285f, 1f);
                            break;
                    }
                }
                
            }


            //TouchDistance가 일정거리 이하 일때 ( 일반터치 )
            if (Vector2.Distance(beganPosition, endPosition) > touchDistance)
            {
                if (hit.collider.gameObject.CompareTag("Stage"))
                {
                    // *** 스테이지 선택 UI 추가
                    switch (hit.collider.gameObject.name)
                    {
                        
                    case "TutorialIsland":
                        //iTween.RotateTo(_planet.gameObject, iTween.Hash("x", 0, "y", 0, "z", 0, "speed", 100.0f));
                        //ColliderEnabledFalse(_hit.collider.gameObject.name);
                        break;
                         
                    }

                }
            }
        }
         */

        // 일반 터치 시작
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit);

        // Check the Tag and Do It!
        //if (pickObject.CompareTag("Map"))
        if (hit.collider.gameObject.CompareTag("Map") && touchEnabledIland)
        {
            Debug.Log("Onclick");
            // 스테이지 선택 입장

            if (this.gameObject.GetComponent<SecStageAnimation>().GetTitleAniDone())
            {
                switch (hit.collider.gameObject.name)
                {
                    case "GrassLand":
                        StageSelectAnimation();
                        StartCoroutine("NotSingleTouch", 1.0f);
                      
                        break;
                    case "IceLandColider":
                        IceStageSelectAnimation();
                        StartCoroutine("NotSingleTouch", 1.0f);
                        break;
                    case "DesertLand":
                        DesertStageSelectAnimation();
                        StartCoroutine("NotSingleTouch", 1.0f);
                        break;
                    case "HellLand":
                        HellStageSelectAnimation();
                        StartCoroutine("NotSingleTouch", 1.0f);
                        break;

                    case "NullCollider":
                        GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
                        GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 1f).OnUpdate(() => GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 0f));

                        GameObject.Find("StageButton").transform.DOLocalMoveX(-1285f, 1f);
                        GameObject.Find("IceStageButton").transform.DOLocalMoveX(-1280f, 1f);
                        GameObject.Find("DesertStageButton").transform.DOLocalMoveX(-1280f, 1f);
                        GameObject.Find("HellStageButton").transform.DOLocalMoveX(-1280f, 0f);
                        
                        break;
                }
            }
        }
    }

    IEnumerator PlanetSwipeSliding()
    {
        //Draging~
        while (currentSpeed > 1.0f)
        {
            float minusSpeed = currentSpeed;
            minusSpeed *= rotationMagnification;
            planet.gameObject.transform.RotateAround(Vector3.zero, new Vector2(deltaPosition.y, -deltaPosition.x), Time.deltaTime * (currentSpeed * rotationMagnification));
            currentSpeed -= minusSpeed;
            //print(currentSpeed);
            yield return null;
        }
    }

    bool ScreenPointToRay()
    {
        if (touchEnabled == false)
            return false;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider == null)
                return false;

            pickObject = hit.collider.gameObject;
            //print(hit.collider.name);

            return true;
        }
        print("ScreenPointToRay Error!!");
        return false;
    }
    public string GetStageSelectName() { return stageSelectName; }
    public bool GetStageSelectDrage() { return stageSelectDrag; }

    public void StageSelectAnimation()
    {
        GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
        GameObject.Find("StageName").GetComponent<Text>().text = "초원의 땅";

        GameObject.Find("StageName").GetComponent<Text>().DOFade(1f, 1f).OnComplete(() =>
            GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 1f));


        planet.transform.DORotate(new Vector3(16f, -180f, -20f), 0.3f);

        stageSelectDrag = true;
        stageSelectName = "GrassLand";

        GameObject.Find("HellStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("IceStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("DesertStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);

        GameObject.Find("StageButton").transform.DOLocalMoveX(0f, 1f);

        
        GameData.Instance.SetSelectStage(GetStageSelectName());

    }

    public void IceStageSelectAnimation()
    {
        GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
        GameObject.Find("StageName").GetComponent<Text>().text = "빙하의 땅";

        GameObject.Find("StageName").GetComponent<Text>().DOFade(1f, 1f).OnComplete(() =>
            GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 1f));


        planet.transform.DORotate(new Vector3(351f, 274f, 277f), 0.3f);

        stageSelectDrag = true;
        stageSelectName = "IceLand";

        GameObject.Find("HellStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("StageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("DesertStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("IceStageButton").transform.DOLocalMoveX(0f, 1f);

        GameData.Instance.SetSelectStage(GetStageSelectName());
    }

    public void DesertStageSelectAnimation()
    {
        GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
        GameObject.Find("StageName").GetComponent<Text>().text = "사막의 땅";

        GameObject.Find("StageName").GetComponent<Text>().DOFade(1f, 1f).OnComplete(() =>
            GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 1f));


        planet.transform.DORotate(new Vector3(50f, 263f, 15.8f), 0.3f);

        stageSelectDrag = true;
        stageSelectName = "DesertLand";

        GameObject.Find("HellStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("StageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("IceStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("DesertStageButton").transform.DOLocalMoveX(0f, 1f);

        GameData.Instance.SetSelectStage(GetStageSelectName());
    }

    public void HellStageSelectAnimation()
    {
        GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
        GameObject.Find("StageName").GetComponent<Text>().text = "화산의 땅";

        GameObject.Find("StageName").GetComponent<Text>().DOFade(1f, 1f).OnComplete(() =>
            GameObject.Find("StageName").GetComponent<Text>().DOFade(0f, 1f));


        planet.transform.DORotate(new Vector3(66f, 118f, 302f), 0.3f);

        stageSelectDrag = true;
        stageSelectName = "HellLand";

        GameObject.Find("StageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("IceStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("DesertStageButton").transform.localPosition = new Vector3(-1280f, 0, -3700f);
        GameObject.Find("HellStageButton").transform.DOLocalMoveX(0f, 1f);
        
        GameData.Instance.SetSelectStage(GetStageSelectName());
    }

    IEnumerator NotSingleTouch(float _seconds) 
    {
        touchEnabledIland = false;
        yield return new WaitForSeconds(_seconds);

        touchEnabledIland = true;
    }

    private bool IsPointerOverUIObject()
    {
        // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
        // the ray cast appears to require only eventData.position.
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
