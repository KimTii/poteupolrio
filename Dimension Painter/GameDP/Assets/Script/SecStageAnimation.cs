﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class SecStageAnimation : MonoBehaviour {
    public GameObject[] stageGrassButton;
    public Text userId;
    Sequence myTweenSequence;
    //public GameObject achivementButton;                 

    public Transform gameTitle;                        // game title image.
    public Transform planetObject;                     // planat object
    public Transform TouchText;                        // Touch text

    private const bool swipeTouch = false;    
    bool titleDone = false;
    byte touchNum = 0;

    GameObject PlanetFlare;

    public bool GetTitleAniDone() { return titleDone; }
    public void SetTitleAniDone(bool _drag) { titleDone = _drag; }

    void TouChOnStart()
     {
         // Exit animation after Touch plase.
         if (Input.GetMouseButtonDown(0) && titleDone == true )
         {
             // first Action
             if (StartApp.Instance.GetTitleOn() == false)
             {
                 GetComponent<AudioSource>().Play();
                 if( touchNum != 0)
                     TouchText.transform.localPosition = new Vector3(21.0f, -600.0f, 0f);

                 StartApp.Instance.SetTitleOn(true);
                 planetObject.DOScale(1.0f, 1f);
                 PlanetFlare.transform.DOScale(1.0f, 1f).OnComplete( () =>
                     PlanetFlare.transform.DOScale(1.1f,1f).SetLoops(-1 , LoopType.Yoyo)
                     );
                 

                 gameTitle.DOLocalMoveY(1000f, 3.0f);

                 GameObject.Find("UITransform").transform.DOLocalMoveX(540, 0.5f);
                 if( GPGmng.Instance.bLogin )
                 {
                     GameObject.Find("UnrockImage").GetComponent<CanvasRenderer>().Clear();
                     GameObject.Find("LunrockImage").GetComponent<CanvasRenderer>().Clear();
                 }
             }
             // Second Action
             else { return; }
         }
     }

	// Use this for initialization
	void Start () {

        Screen.sleepTimeout = SleepTimeout.NeverSleep; 
        Screen.SetResolution(Screen.width, Screen.width / 16 * 9, true);
        Application.targetFrameRate = 60;
        
        // GameObject.Find("STAGEINFO").GetComponent<Text>().text = "Save Info : " + PlayerPrefs.GetInt("STAGEPROGRASS");

        // 이 앱을 처음으로 유저가 킨 것 이라면
        if (PlayerPrefs.GetInt("NEWGAME") == 0)
        {
            SaveGameInfo.NewCreateSaveAllInfomation();
            GameData.Instance.SetStageInfo(0);
            Debug.Log("New Game");
        }
        else
        {
            GameData.Instance.SetStageInfo(PlayerPrefs.GetInt("STAGEPROGRASS"));
            Debug.Log("Not New Game");
        }
        GameData.Instance.SetStageInfo(3);

        //show Button
        //ButtonShowSetting();

        PlayerPrefs.SetInt("STAGEPROGRASS", GameData.Instance.GetStageInfo());
        StageSelect();


        GameObject.Find("CFXM_SmokeTrail").GetComponent<ParticleSystem>().playbackSpeed = 0.3f;
        GameObject.Find("UIMenuIconButton").GetComponent<Image>().rectTransform.DOScale(0.005343006f, 0.45f).SetLoops(-1, LoopType.Yoyo);

        // Google Activation
        //GPGmng.Instance.InitializeGPGS();
        
        // Google Login on ?
       // GPGmng.Instance.LoginGPGS();

        // Google NickName.
        // userId.text = "User Name : " + GPGmng.Instance.GetNameGPGS();

        // StartApp.Instance.DeBugModeOnOff( StartApp.DebugModeSetting.deBugingModeOn );                 // erase <-- debug mode Setting

        PlanetFlare = GameObject.Find("PlanetFlare"); 
        myTweenSequence = DOTween.Sequence();

        // Find star animation[2]
        GameObject.Find("ShootingStar 1").transform.DOLocalMove(new Vector3(-2000.0f, -1500f, 0), 4.5f, false).SetRelative().SetLoops(-1,LoopType.Restart);
        GameObject.Find("ShootingStar 2").transform.DOLocalMove(new Vector3(2000.0f, -1500f, 0), 3.6f, false).SetRelative().SetLoops(-1, LoopType.Restart);

        // OngameStart Fisrt Access is this Code play.
        if (StartApp.Instance.GetTitleOn() == false)
        {
            // call corutin animation 
            // planetObject Move
            planetObject.transform.position = new Vector3(0, -2f, 0);
            PlanetFlare.transform.position = new Vector3(0, -2f, 0);

            PlanetFlare.transform.DOMoveY(0.0f, 1.5f, false)
            .OnComplete ( ()=>GameObject.Find("Indicater").GetComponent<Image>().DOFade(1f , 3.0f)
            );

            myTweenSequence.Append(planetObject.DOMoveY(0.0f, 1.5f, false)
            // gameTitle Move [2]
            .OnComplete(() => myTweenSequence.Join(gameTitle.GetComponentInChildren<Image>().DOFade(1f,1.5f)


            // Animation Done
            .OnComplete(() => titleDone = true)
            )));

            //TouchText.DOScale(15f, 1f).SetRelative().SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            ++touchNum;
            titleDone = true;

            PlanetFlare.transform.DOScale(1.0f, 1f).OnComplete(() =>
                      PlanetFlare.transform.DOScale(1.1f, 1f).SetLoops(-1, LoopType.Yoyo)
                      );

            planetObject.DOScale(1.0f, 1.0f);
            GameObject.Find("UITransform").transform.DOLocalMoveX(540, 0.5f);
            //UI On Animation
            //GameObject.Find("AchivementButton").GetComponent<RectTransform>().transform.DOLocalMoveX(-550f, 1f);
            //GameObject.Find("LeaderBoardButton").GetComponent<RectTransform>().transform.DOLocalMoveX(-550f, 1f);

            //GameObject.Find("OptionButton").GetComponent<RectTransform>().transform.DOLocalMoveX(550f, 1f);
            //GameObject.Find("StoryBoardButton").GetComponent<RectTransform>().transform.DOLocalMoveX(550f, 1f);

            //if (GPGmng.Instance.bLogin)
            //{
            //    GameObject.Find("UnrockImage").GetComponent<CanvasRenderer>().Clear();
            //    GameObject.Find("LunrockImage").GetComponent<CanvasRenderer>().Clear();
            //}
        }
	}
	
	// Update is called once per frame

    bool test =false;
    void FixedUpdate()
    {
        TouChOnStart();

        //if (Input.GetKeyUp(KeyCode.Escape))
        //{
        //    if(!test)
        //    {
        //        GameObject.Find("DEBUGSTAGEON").transform.DOLocalMoveX(-388f, 1.0f);
        //        test = true;
        //    }
               
        //    else
        //    {
        //        GameObject.Find("DEBUGSTAGEON").transform.DOLocalMoveX(-1700f, 1.0f);   
        //        test = false;
        //    }
                 
        //}

        if (titleDone == true && touchNum == 0 && StartApp.Instance.GetTitleOn() == false)
        {
            ++touchNum;
            TouchText.transform.localPosition = new Vector3(24f, -300.0f, 0f);
            TouchText.GetComponentInChildren<Text>().DOFade(1f, 0.6f).SetLoops(3,LoopType.Yoyo);
        }
    }


    // 스테이지 선택 애니메이션 실행 함수
    void StageSelect()
    {
        switch( GameData.Instance.GetSelectStage() )
        {   
            case "GrassLand" :
                GetComponent<TouchEvent>().StageSelectAnimation();
                break;
            case "DesertLand":
                GetComponent<TouchEvent>().DesertStageSelectAnimation();
                break;
            case "IceLand" :
                GetComponent<TouchEvent>().IceStageSelectAnimation();
                break;
            case "HellLand":
                GetComponent<TouchEvent>().HellStageSelectAnimation();
                break;
        }
    }

    void ButtonShowSetting()
    {
        Debug.Log("GameData.Instance.GetStageInfo() : " + GameData.Instance.GetStageInfo());
        for (int i = 0; i <= GameData.Instance.GetStageInfo(); i++)
        {
            stageGrassButton[i].GetComponent<Button>().enabled = true;
            stageGrassButton[i].GetComponent<Image>().enabled = true;
            stageGrassButton[i].GetComponentInChildren<Text>().enabled = true;
        }
    }
}
    