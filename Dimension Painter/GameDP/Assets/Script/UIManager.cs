﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour {

    PlayerManager playerManager;
    float delayTime;
    string sceneName;

    bool soundOn = true;
    bool tweenDone = false;
    int onClickCount = 0;


    void Awake()
    {
        playerManager = GetComponent<PlayerManager>();
        delayTime = 0.1f;
    }

    public void DoorButton() { StartCoroutine(this.DoorButtonCoroutine()); }
    IEnumerator DoorButtonCoroutine()
    {
        if (playerManager.GetPlayerState() != PlayerState.Idle)
            yield break;

        yield return new WaitForSeconds(delayTime);
        playerManager.Door();
    }

    public void WarpButton() { StartCoroutine(this.WarpButtonCoroutine()); }
    IEnumerator WarpButtonCoroutine()
    {
        if (playerManager.GetPlayerState() != PlayerState.Idle)
            yield break;

        yield return new WaitForSeconds(delayTime);
        //playerManager.DrawWarp();
        playerManager.StartCoroutine("DrawWarp");
    }

    public void Puase() 
    {
        if (Time.timeScale != 0)
        {
            GameObject.Find("Paruse").GetComponent<GraphicRaycaster>().enabled = false;
            GameObject.Find("PuaseFade").GetComponent<Image>().DOFade(0.7f, 0.8f).OnComplete(() => Time.timeScale = 0);
            GameObject.Find("ReStart").GetComponent<Image>().DOFade(1, 0.8f).OnComplete(() => tweenDone = true);

            GameObject.Find("MainWorldMap").GetComponent<Image>().DOFade(1, 0.8f);
            GameObject.Find("SoundOnoff").GetComponent<Image>().DOFade(1, 0.8f);
            GameObject.Find("PlayButton").GetComponent<Image>().DOFade(1, 0.8f);

            GameObject.Find("ReStart").GetComponent<GraphicRaycaster>().enabled = true;
            GameObject.Find("MainWorldMap").GetComponent<GraphicRaycaster>().enabled = true;
            GameObject.Find("SoundOnoff").GetComponent<GraphicRaycaster>().enabled = true;
            GameObject.Find("PlayButton").GetComponent<GraphicRaycaster>().enabled = true;
        }
    }

    public void PlayButton()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;

            GameObject.Find("Paruse").GetComponent<GraphicRaycaster>().enabled = true;
            GameObject.Find("PuaseFade").GetComponent<Image>().DOFade(0f, 1.5f);

            GameObject.Find("ReStart").GetComponent<Image>().DOFade(0, 0.8f);
            GameObject.Find("MainWorldMap").GetComponent<Image>().DOFade(0, 0.8f);
            GameObject.Find("SoundOnoff").GetComponent<Image>().DOFade(0, 0.8f);
            GameObject.Find("PlayButton").GetComponent<Image>().DOFade(0, 0.8f);

            GameObject.Find("ReStart").GetComponent<GraphicRaycaster>().enabled = false;
            GameObject.Find("MainWorldMap").GetComponent<GraphicRaycaster>().enabled = false;
            GameObject.Find("SoundOnoff").GetComponent<GraphicRaycaster>().enabled = false;
            GameObject.Find("PlayButton").GetComponent<GraphicRaycaster>().enabled = false;
        }
    }

    public void ReStartButton() 
    {

            if (Time.timeScale == 0)
                Time.timeScale = 1;

            DG.Tweening.DOTween.KillAll();
            Application.LoadLevelAsync(Application.loadedLevelName); 
        
           
    }
    public void MainWorldMapButton() 
    {

            if (Time.timeScale == 0)
            Time.timeScale = 1;

            DG.Tweening.DOTween.KillAll();

            if (Application.loadedLevelName == "HELL01")
                Application.LoadLevelAsync(1);
            else
                this.GetComponent<Loading>().StartCoroutine("StartLoad", "WorldMap");
  
    }
    public void SoundOnOffButton()
    {
        if (soundOn)
        {
            Camera.main.GetComponent<AudioSource>().Pause();
            soundOn = false;
        }
        else
        {
            Camera.main.GetComponent<AudioSource>().Play();
            soundOn = true;
        }

    }
}
