﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

static class PlayerState
{
    public const byte Idle = 0;
    public const byte Run = 1;
    public const byte Draw = 2;
}

static class Arrow
{
    public const byte Left = 0;
    public const byte Right = 1;
    public const byte Forward = 2;
    public const byte Back = 3;
    public const byte Error = 100;
}

static class WarpArrow
{
    public const bool Front = true;
    public const bool Back = false;
}

public class PlayerManager : MonoBehaviour {

    public GameObject player;
    public GameObject playerTargetPositionImage;    

    //Getcomponent 미리 선언
    Animator playerAnimator;
    GameManager gameManager;
    SoundMng soundManager;
    NavMeshAgent playerNavMeshAgent;
    Rigidbody playerRigidbody;
    CapsuleCollider playerCollider;
    CameraManager cameraManager;
    SpriteRenderer playerTargetPositonImageSpriteRenderer;
    ParticleSystem pushBoxParticle;
    //Transform 미리 선언
    Transform playerTransform;
    Transform playerTargetPositionImageTransform;
    Transform targetTransform;

    //Player의 상태관리
    private byte playerState;
    //Player Rotation 관련 변수
    Vector3 direction;
    Quaternion targetRotation;
    //Player가 실제로 가고 있는 좌표
    Vector3 playerMovePosition;

    //--------------------------------------------
    //SKILL : WARP 관련 변수
    //--------------------------------------------
    GameObject firstWarpGate;
    GameObject secondWarpGate;
    Vector3 firstWarpPosition;
    Vector3 secondWarpPosition;
    //워프 사용가능 여부
    bool warp;

    //CheckPlayerMove에 필요한 변수
    Vector3 lastPlayerPosition;
    float deltaPlayerPosition;

    //RunSound에 필요한 변수;
    bool runSound;

    // 캐릭터 지속적인 제자리 뛰기를 막는 변수들
    int checkStandNum;
    int checkStandNumTemp;
    Vector3 checkBeforePosition;
    Vector3 point;

    IEnumerator ResetPosition()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            // 5초천의 캐릭터의 좌표를 저장
            if (playerAnimator.GetBool("Run"))
            {
                //Debug.Log(" Running : " + checkBeforePosition + " + " + playerTransform.position + " , checkStandNumTemp = " + checkStandNumTemp);
                if (checkStandNum == 4)
                {
                    checkBeforePosition = playerTransform.position;
                    checkStandNum = 0;
                }   

                if (checkBeforePosition == playerTransform.position)
                {
                    checkStandNumTemp++;

                    if (checkStandNumTemp > 3)
                    {
                        // 캐릭터가 가지 못하는곳을 눌렀을때 걸음을 멈추기
                        Move(playerTransform.position);
                        checkStandNumTemp = 0;
                    }
                }
                else
                {
                    checkStandNumTemp = 0;
                }
                checkStandNum++;
            }
        }
    }


	// Use this for initialization
    void Awake() {

        DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(750, 10);
        //GetComponent
        gameManager = GetComponent<GameManager>();
        soundManager = GetComponent<SoundMng>();
        playerNavMeshAgent = player.GetComponent<NavMeshAgent>();
        playerAnimator = player.GetComponent<Animator>();
        playerRigidbody = player.GetComponent<Rigidbody>();
        playerCollider = player.GetComponent<CapsuleCollider>();
        cameraManager = GetComponent<CameraManager>();
        playerTargetPositonImageSpriteRenderer = playerTargetPositionImage.GetComponent<SpriteRenderer>();
        pushBoxParticle = (Resources.Load("Particle/PushBox") as GameObject).GetComponent<ParticleSystem>();
        //Transform
        playerTransform = player.transform;
        playerTargetPositionImageTransform = playerTargetPositionImage.transform;


        //Player상태 관리 변수
        playerState = PlayerState.Idle;
        //Player워프를 관리하기 위한 변수
        firstWarpGate = null;
        secondWarpGate = null;
        //워프의 사용 여부를 가리기 위한 변수
        warp = false;

        //제자리 뛰기 할때 멈추기 위한 변수
        lastPlayerPosition = playerTransform.position;

        runSound = false;

        checkBeforePosition = playerTransform.position;
        StartCoroutine("ResetPosition");
	}

    public void Move(Vector3 target)
    {
        playerNavMeshAgent.enabled = true; 
        //Player State Change
        playerState = PlayerState.Run;
        //player Animator = Run
        if(! playerAnimator.GetBool("Run") )
            playerAnimator.SetBool("Run", true);

        //Rotation Start
        StartCoroutine(this.Rotation(target));
        //Move Start
        playerMovePosition = target;

        //playerNavMeshAgent.destination = playerMovePosition;
        playerNavMeshAgent.SetDestination(playerMovePosition);
        //이동시 목표지점에 찍히는 이미지 생성
        PlayerTargetPositionImage(playerMovePosition);
    }
    IEnumerator Rotation(Vector3 target)
    {
        //도착할때까지 계속 로테이션 돌리자
        //velocity문제가 생긴다. 수정이 필요할듯하다
        while(playerState == PlayerState.Run)
        {
            //Player Turn Function
            direction = playerNavMeshAgent.velocity;
            //Vector3.zero 상태일때 일정방향으로 회전하는 것을 방지하기 위함
            if(direction != Vector3.zero)
            {
                targetRotation = Quaternion.LookRotation(direction);
                playerTransform.DORotate(new Vector3(targetRotation.eulerAngles.x,
                                                     targetRotation.eulerAngles.y,
                                                     targetRotation.eulerAngles.z), 0.5f);
            }
            yield return null;
        }
    }
    void PlayerTargetPositionImage(Vector3 target)
    {
        DOTween.Kill(playerTargetPositonImageSpriteRenderer, true);
        playerTargetPositonImageSpriteRenderer.color = new Color(255, 255, 255, 1);

        playerTargetPositonImageSpriteRenderer.DOFade(0f, 1.0f);



        playerTargetPositionImageTransform.position = target + new Vector3(0, 0.025f, 0);
        // TFB animation
        //playerTargetPositionImageTransform.DOScale(0.1f, 0f);
        //playerTargetPositionImageTransform.DOScale(0.2f, 1.0f).SetRelative().SetLoops(-1, LoopType.Yoyo);
        //playerTargetPositonImageSpriteRenderer.DOFade(0, 3.0f);
    }
    public void Approach(GameObject target)
    {
        Transform targetTransform = target.transform;
        if(target.CompareTag("PaintWall"))
        {
            //워프사용여부확인
            if (firstWarpGate != null && secondWarpGate != null)
                warp = true;

            Vector3 paintWallFrontPosition = target.transform.Find("WarpFrontPosition").transform.position;
            Vector3 paintWallBackPosition = target.transform.Find("WarpBackPosition").transform.position;
            float front = Vector3.Distance(playerTransform.position, paintWallFrontPosition);
            float back = Vector3.Distance(playerTransform.position, paintWallBackPosition);
            if (front < back)
                Move(paintWallFrontPosition);
            else
                Move(paintWallBackPosition);
        }
        else if(target.CompareTag("PushBox"))
        {
            byte arrow = GetArrow(target.transform);
            Vector3 arrowPosition = Vector3.zero;

            if (arrow == Arrow.Left)
            {
                arrowPosition = targetTransform.position + new Vector3(-0.5f, 0, 0);
            }
            else if (arrow == Arrow.Right)
            {
                arrowPosition = targetTransform.position + new Vector3(0.5f, 0, 0);
            }
            else if (arrow == Arrow.Forward)
            {
                arrowPosition = targetTransform.position + new Vector3(0, 0, 0.5f);
            }
            else if (arrow == Arrow.Back)
            {
                arrowPosition = targetTransform.position + new Vector3(0, 0, -0.5f);
            }
            else
            {
                print("Approach()에서 에러남");
                return;
            }
                

            Move(arrowPosition);
        }
    }
    byte GetArrow(Transform targetTransform)
    {
        bool left = false;
        bool right = false;
        bool front = false;
        bool back = false;
        Vector3 distance = playerTransform.position - targetTransform.position;

        if (distance.x < 0)
            left = true;
        else
            right = true;

        if (distance.z < 0)
            back = true;
        else
            front = true;

        if (Mathf.Abs(distance.x) > Mathf.Abs(distance.z))
        {
            //Left or Right
            back = false;
            front = false;
        }
        else
        {
            //Back or Front
            left = false;
            right = false;
        }

        if (left == true)
            return Arrow.Left;
        else if (right == true)
            return Arrow.Right;
        else if (back == true)
            return Arrow.Back;
        else if (front == true)
            return Arrow.Forward;
        else
        {
            print("GetArrow()에서 Error남!!");
            return Arrow.Error;
        }
    }

    //------------------------------------------------------------------------------------------
    //---------------- SKILL : DOOR   &   WARP   &   PUSH BOX ----------------------------------
    //------------------------------------------------------------------------------------------
    //UI를 눌렀을 때 실행되는 함수
    public void Door()
    {
        float playerMovePositionDistance = gameManager.GetPlayerMovePositionDistance();

        GameObject pickObject = gameManager.GetPickObject();
        //PickObject확인
        if (!pickObject.CompareTag("PaintWall"))
            return;

        PaintWall pickObjectPaintWall = pickObject.GetComponent<PaintWall>();
        Transform pickObjectTransform = pickObject.transform;
        ParticleSystem doorParticle = pickObjectTransform.Find("DoorParticle").GetComponent<ParticleSystem>();
        
        SpriteRenderer[] SpriteDoorRendererArr = 
        {
             pickObjectTransform.Find("DoorSpriteFront").GetComponent<SpriteRenderer>(),
             pickObjectTransform.Find("DoorSpriteBack").GetComponent<SpriteRenderer>()
        };
        
        //거리체크 / 그릴수 있는지 체크 
        if (gameManager.PlayerIsArrive(playerMovePosition, playerMovePositionDistance) && pickObjectPaintWall.CheckPaintWallState())
        {
            //카메라 플레이어에게 이동
            cameraManager.FollowPlayer();

            //대상 바라보기
            playerTransform.DOLookAt(new Vector3(pickObjectTransform.position.x, playerTransform.position.y, pickObjectTransform.position.z), 0.15f);
            //추가적으로 클릭이 안되게 설정하기
            gameManager.StartCoroutine("StopTouch", 1.2f);


            StartCoroutine(this.DoorAnimationAndParticle(doorParticle));
            // 그리기 트레일
            //traillAnimation.GetComponent<TrailRenderer>().time = 1.0f;
            //StartCoroutine("StoptheAnitrail", pickObjectTransform);
            
            //Collider & NavMeshObstacle 제거
            pickObjectPaintWall.TurnOffColliderAndObstacle();
            //PaintWall State Change
            pickObjectPaintWall.SetPaintWallState(PaintWallState.Door, true);
            //아이템 개수 조절
        }
    }
    IEnumerator DoorAnimationAndParticle(ParticleSystem doorParticle)
    {
        //Player Animation 실행
        playerState = PlayerState.Draw;
        playerAnimator.SetTrigger("Draw");
        yield return new WaitForSeconds(0.2f);

        doorParticle.Play();
    }
    //UI를 눌렀을 때 실행되는 함수
    public IEnumerator DrawWarp()
    {
        float playerMovePositionDistance = gameManager.GetPlayerMovePositionDistance();

        GameObject pickObject = gameManager.GetPickObject();
        //PickObject확인
        if (!pickObject.CompareTag("PaintWall"))
            yield break;

        PaintWall pickObjectPaintWall = pickObject.GetComponent<PaintWall>();
        Transform pickObjectTransform = pickObject.transform;
        //SpriteRenderer doorSpriteFront = pickObjectTransform.Find("WarpSpriteFront").GetComponent<SpriteRenderer>();
        //SpriteRenderer doorSpriteBack = pickObjectTransform.Find("WarpSpriteBack").GetComponent<SpriteRenderer>();
        Vector3 warpFrontPosition = pickObjectTransform.Find("WarpFrontPosition").position;
        Vector3 warpBackPosition = pickObjectTransform.Find("WarpBackPosition").position;
        ParticleSystem warpParticleFront = pickObjectTransform.Find("WarpParticleFront").GetComponent<ParticleSystem>();
        ParticleSystem warpParticleBack = pickObjectTransform.Find("WarpParticleBack").GetComponent<ParticleSystem>();


        //대상 바라보기
        //playerTransform.LookAt(new Vector3(pickObjectTransform.position.x, playerTransform.position.y, pickObjectTransform.position.z));
        playerTransform.DOLookAt(new Vector3(pickObjectTransform.position.x, playerTransform.position.y, pickObjectTransform.position.z), 0.15f);
        yield return new WaitForSeconds(0.2f);
        
        //거리계산 && 그릴수 있는지 체크
        if (gameManager.PlayerIsArrive(playerMovePosition, playerMovePositionDistance) && pickObjectPaintWall.CheckPaintWallState())
        {
            float fAngle;
            //angle : 0 Front true / 180 Back false
            fAngle = Vector3.Angle(playerTransform.forward, pickObjectTransform.right);
            print("Angle : " + fAngle);

            bool bAngle;
            if (fAngle > -30 && fAngle < 30)    
                bAngle = WarpArrow.Front;
            else if (fAngle > 150 && fAngle < 210)
                bAngle = WarpArrow.Back;
            else
            {
                print("Angle Error!!!");
                yield break;
            }
                
            //카메라 플레이어에게 이동
            cameraManager.FollowPlayer();

            if(firstWarpGate == null && secondWarpGate == null)
            {
                //선택된 오브젝트 저장하고
                firstWarpGate = pickObject;
                //Front인지 Back인지 저장하고
                if (bAngle == WarpArrow.Front)
                {
                    firstWarpPosition = warpFrontPosition;
                    //doorSpriteFront.enabled = true;
                    warpParticleFront.Play();
                }
                else
                {
                    firstWarpPosition = warpBackPosition;
                    //doorSpriteBack.enabled = true;
                    warpParticleBack.Play();
                }
            }
            else if(firstWarpGate != null && secondWarpGate == null)
            {
                //선택된 오브젝트 저장하고
                secondWarpGate = pickObject;
                //Front인지 Back인지 저장하고
                if (bAngle == WarpArrow.Front)
                {
                    secondWarpPosition = warpFrontPosition;
                    //doorSpriteFront.enabled = true;
                    warpParticleFront.Play();
                }
                    
                else
                {
                    secondWarpPosition = warpBackPosition;
                    //doorSpriteBack.enabled = true;
                    warpParticleBack.Play();
                }
                    
            }
            else if(firstWarpGate != null && secondWarpGate != null)
            {
                //워프 사용불가
                warp = false;
                //기존에 생성되어 있던 WarpSprite 지우기
                /*
                firstWarpGate.transform.Find("WarpSpriteFront").GetComponent<SpriteRenderer>().enabled = false;
                firstWarpGate.transform.Find("WarpSpriteBack").GetComponent<SpriteRenderer>().enabled = false;
                secondWarpGate.transform.Find("WarpSpriteFront").GetComponent<SpriteRenderer>().enabled = false;
                secondWarpGate.transform.Find("WarpSpriteBack").GetComponent<SpriteRenderer>().enabled = false;
                */
                firstWarpGate.transform.Find("WarpParticleFront").GetComponent<ParticleSystem>().Stop();
                firstWarpGate.transform.Find("WarpParticleFront").GetComponent<ParticleSystem>().Clear();
                firstWarpGate.transform.Find("WarpParticleBack").GetComponent<ParticleSystem>().Stop();
                firstWarpGate.transform.Find("WarpParticleBack").GetComponent<ParticleSystem>().Clear();

                secondWarpGate.transform.Find("WarpParticleFront").GetComponent<ParticleSystem>().Stop();
                secondWarpGate.transform.Find("WarpParticleFront").GetComponent<ParticleSystem>().Clear();
                secondWarpGate.transform.Find("WarpParticleBack").GetComponent<ParticleSystem>().Stop();
                secondWarpGate.transform.Find("WarpParticleBack").GetComponent<ParticleSystem>().Clear();
                //기존에 생성되어 있던 WarpState 변경
                firstWarpGate.GetComponent<PaintWall>().SetPaintWallState(PaintWallState.Warp, false);
                secondWarpGate.GetComponent<PaintWall>().SetPaintWallState(PaintWallState.Warp, false);
                firstWarpGate = null;
                secondWarpGate = null;

                //선택된 오브젝트 저장하고
                firstWarpGate = pickObject;
                //Front인지 Back인지 저장하고
                if (bAngle == WarpArrow.Front)
                {
                    firstWarpPosition = warpFrontPosition;
                    //doorSpriteFront.enabled = true;
                    warpParticleFront.Play();
                }
                else
                {
                    firstWarpPosition = warpBackPosition;
                    //doorSpriteBack.enabled = true;
                    warpParticleBack.Play();
                }
            }
            else
            {
                print("Warp System Erorr!!");
                yield break;
            }

            //추가적으로 클릭이 안되게 설정하기
            gameManager.StartCoroutine("StopTouch", 1.2f);
            //Player Animation 켜고
            playerState = PlayerState.Draw;

            playerAnimator.SetTrigger("Draw");
            //PaintWall State Change
            pickObjectPaintWall.SetPaintWallState(PaintWallState.Warp, true);
        }
        
    }
    public void EnterWarp(GameObject pickObject)
    {
        //워프를 체크해서 하나라도 null이면 진입금지
        if (firstWarpGate == null || secondWarpGate == null)
            return;

        if(pickObject == firstWarpGate)
        {
            //Front 인지 Back 인지 확인해야함
            if (gameManager.PlayerIsArrive(firstWarpPosition, gameManager.GetPlayerMovePositionDistance()))
            {
                //second로
                //playerNavMeshAgent.Warp(secondWarpPosition);
                Vector3[] targetPosition = { pickObject.transform.position, secondWarpGate.transform.position , secondWarpPosition };
                StartCoroutine("WarpEffect", targetPosition);
            }
        }
        else if(pickObject == secondWarpGate)
        {
            if (gameManager.PlayerIsArrive(secondWarpPosition, gameManager.GetPlayerMovePositionDistance()))
            {
                //first로
                //playerNavMeshAgent.Warp(firstWarpPosition);
                Vector3[] targetPosition = { pickObject.transform.position, firstWarpGate.transform.position, firstWarpPosition };
                StartCoroutine("WarpEffect", targetPosition);
            }
        }
        else
        {
            print("EnterWarp Error!!!!!!");
        }
    }
    public IEnumerator WarpEffect(Vector3[] targetPosition)
    {
        gameManager.StartCoroutine("StopTouch", 1.3f);
        float warpEffectSpeed = 0.5f;
        //충돌방지
        playerNavMeshAgent.enabled = false;
        playerCollider.enabled = false;

        // 0번엔 PickObjectPosition, 1번엔 이동해야할 오브젝트 Position, 2번엔 이동해야할 워프 포지션
        // 카메라 플레이어에게 이동
        cameraManager.FollowPlayer();
        playerTransform.DOScale(0, warpEffectSpeed);
        playerTransform.DOMove(targetPosition[0], warpEffectSpeed);
        yield return new WaitForSeconds(warpEffectSpeed + 0.1f);

        playerTransform.position = targetPosition[1];
        //playerTransform.DOLookAt(new Vector3(targetPosition[2].x, playerTransform.position.y, targetPosition[2].z), 0.15f);
        playerTransform.LookAt(new Vector3(targetPosition[2].x, playerTransform.position.y, targetPosition[2].z));

        // 카메라 플레이어에게 이동
        cameraManager.FollowPlayer();
        playerTransform.DOScale(1, warpEffectSpeed);
        playerTransform.DOMove(targetPosition[2] + new Vector3(0, 0.40f, 0), warpEffectSpeed);
        yield return new WaitForSeconds(warpEffectSpeed + 0.1f);

        playerNavMeshAgent.enabled = true;
        playerCollider.enabled = true;
    }

    public void LeverController()
    {
        GameObject pickObject = gameManager.GetPickObject();

        if (!pickObject.CompareTag("LeverController") || playerState != PlayerState.Idle)
            return;

        //Debug.Log("DD");

    }

    //푸쉬박스
    public void PushBox()
    {
        GameObject pickObject = gameManager.GetPickObject();
        //PickObject확인
        if (!pickObject.CompareTag("PushBox") || playerState != PlayerState.Idle)
            return;

        //추가적으로 클릭이 안되게 설정하기
        //gameManager.StartCoroutine("StopTouch", 0.1f);

        Transform pickObjectTransform = pickObject.transform;
        byte arrow = GetArrow(pickObjectTransform);
        PushBox pushBox = pickObject.GetComponent<PushBox>();
        //대상 바라보기
        playerTransform.DOLookAt(new Vector3(pickObjectTransform.position.x, playerTransform.position.y, pickObjectTransform.position.z), 0.15f);
        //밀기
        //반대방향을 확인해봐야한다.
        pickObject.GetComponent<PushBox>().CheckPushBox();
        if (arrow == Arrow.Left && pushBox.GetRight())
        {
            //print("Arrow : Left, GetRight() : " + pushBox.GetRight());
            Instantiate(pushBoxParticle, pickObjectTransform.position, Quaternion.identity);
            pickObject.GetComponent<AudioSource>().Play();
            pickObjectTransform.DOMove((pickObjectTransform.position + new Vector3(1.0f, 0, 0)), 0.5f).SetEase(Ease.OutQuint);
            //pickObjectTransform.DOShakeScale(0.3f, 0.2f,1, 5f);
        }
        if (arrow == Arrow.Right && pushBox.GetLeft())
        {
            //print("Arrow : Right, GetLeft() : " + pushBox.GetLeft());
            Instantiate(pushBoxParticle, pickObjectTransform.position, Quaternion.identity);
            pickObject.GetComponent<AudioSource>().Play();
            pickObjectTransform.DOMove((pickObjectTransform.position + new Vector3(-1.0f, 0, 0)), 0.5f).SetEase(Ease.OutQuint);
            //pickObjectTransform.DOShakeScale(0.3f, 0.2f, 1, 5f);
        }
        if (arrow == Arrow.Forward && pushBox.GetBack())
        {
            //print("Arrow : Forward, GetBack() : " + pushBox.GetBack());
            Instantiate(pushBoxParticle, pickObjectTransform.position, Quaternion.identity);
            pickObject.GetComponent<AudioSource>().Play();
            pickObjectTransform.DOMove((pickObjectTransform.position + new Vector3(0, 0, -1.0f)), 0.5f).SetEase(Ease.OutQuint);
            //pickObjectTransform.DOShakeScale(0.3f, 0.2f, 1, 5f);
        }
        if (arrow == Arrow.Back && pushBox.GetForward())
        {
            //print("Arrow : Back, GetForward() : " + pushBox.GetForward());
            Instantiate(pushBoxParticle, pickObjectTransform.position, Quaternion.identity);
            pickObject.GetComponent<AudioSource>().Play();
            pickObjectTransform.DOMove((pickObjectTransform.position + new Vector3(0, 0, 1.0f)), 0.5f).SetEase(Ease.OutQuint);
            //pickObjectTransform.DOShakeScale(0.3f, 0.2f, 1, 5f);
        }
            /*
        else
        {
            print("PushBox Error!!");
            print("Arrow : " + arrow);
            print("PushBoxForward : " + pushBox.GetForward());
            print("PushBoxBack : " + pushBox.GetBack());
            print("PushBoxRight : " + pushBox.GetRight());
            print("PushBoxLeft : " + pushBox.GetLeft());
            return;
        }
            */
    }

    public IEnumerator CheckPlayerMove()
    {
        bool firstPlayerInfo = false;
        bool secondPlayerInfo = false;
        while(playerState == PlayerState.Run)
        {
            deltaPlayerPosition = Vector3.Distance(playerTransform.position, lastPlayerPosition);
            deltaPlayerPosition *= 100;
            if(deltaPlayerPosition == 0)
            {
                firstPlayerInfo = true;
            }
            yield return new WaitForSeconds(0.5f);
            lastPlayerPosition = playerTransform.position;

            deltaPlayerPosition = Vector3.Distance(playerTransform.position, lastPlayerPosition);
            deltaPlayerPosition *= 100;
            if (deltaPlayerPosition == 0)
            {
                secondPlayerInfo = true;
            }

            // 현재 setPlayerAnimatorIdle() 부분의 거시적 버그 발생
            if (firstPlayerInfo && secondPlayerInfo)
               // SetPlayerAnimatorIdle();
            yield return new WaitForSeconds(0.5f);
        }
    }
    
    //GameManager에서 Player가 도착지에 잘 도착했는지 확인이 필요함.
    public Vector3 GetPlayerPosition() { return playerTransform.position; }
    //Player가 실제로 어디로 가는지 알고싶음. GameManager에서 필요
    public Vector3 GetPlayerMovePosition() { return playerMovePosition; }
    //Player Animator의 Run을 끄기위한 함수로 GameManager에서 필요
    public void SetPlayerAnimatorIdle() {
        playerNavMeshAgent.enabled = false;
        
        playerState = PlayerState.Idle;
        if (playerAnimator.GetBool("Run"))
            playerAnimator.SetBool("Run", false);
        
        runSound = false;
    }

    public bool GetStateMovement() { return playerAnimator.GetBool("Run"); }
    //RunSound 함수에서 필요합니다. gamemanager에서도 임시로 사용하고 있습니다.
    public byte GetPlayerState() { return playerState; }
    //GameManager에서 워프 사용여부를 알기 위한 함수
    public bool GetWarp() { return warp; }
}
