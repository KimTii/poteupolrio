﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

// 스테이지 연출을 하기위한 스크립팅
public class DirectionStage : MonoBehaviour {
    GameObject imageSet;

    bool touchDrag = false;

    bool cameraAniDone = false;
    bool startUpdate = false;

    Image backGround;
    Image ConfirmButtonAni;
    Image ConfirmButton;
    Image LineImage;
    Text ChapterName;
    Text StoryText;

    string stageName;

	// Use this for initialization
	void Start () {
        // image Find-----------------------------------------------------------------------
        backGround = GameObject.Find("Canvas/BackGround").GetComponent<Image>();
        LineImage = GameObject.Find("Canvas/BackGround/LineImage").GetComponent<Image>();
        ChapterName = GameObject.Find("Canvas/BackGround/ChapterName").GetComponent<Text>();
        ConfirmButtonAni = GameObject.Find("Canvas/ConfimeButton/ConfimeAnimation").GetComponent<Image>();
        ConfirmButton = GameObject.Find("Canvas/ConfimeButton").GetComponent<Image>();
        StoryText = GameObject.Find("Canvas/StoryText").GetComponent<Text>();
        // image find end -------------------------------------------------------------------

        Camera.main.orthographicSize = 15.0f;

        GameObject.Find("Cloud01").transform.DOLocalMoveX(-2000.0f, 2.5f);
        GameObject.Find("Cloud02").transform.DOLocalMoveX(2000.0f, 2.5f).OnComplete(() => cameraAniDone = true);

        Camera.main.DOOrthoSize(5.0f, 2.0f);
        stageName = Application.loadedLevelName;

        StartCoroutine("StartUICall");
	}

    IEnumerator StartUICall()
    {
        while (!startUpdate)
        {
            yield return new WaitForFixedUpdate();
            if( cameraAniDone )
            {
                // Fade In -------------------------------->
                ChapterName.DOFade(1, 1.5f);
                backGround.DOFade(0.6f, 1.5f);
                LineImage.DOFade(1, 1.5f);
                ConfirmButton.DOFade(1, 1.5f);
                ConfirmButtonAni.DOFade(1, 1.5f).OnComplete(() => ConfirmButtonAni.DOFade(0, 1f).OnComplete(() => ConfirmButtonAni.DOFade(1, 1f)).SetLoops(-1, LoopType.Yoyo));
                StoryText.DOFade(1, 1.5f).OnComplete(() => touchDrag = true);

                // Fade In --------------------------------
                cameraAniDone = false;
                startUpdate = true;
            }
        }
        yield return null;
    }

    public void StoryBoardFadeOutButton()
    {
        // Fado Out -------------------------------> 
        if (touchDrag)
        {
            ConfirmButtonAni.GetComponent<AudioSource>().Play();

            backGround.DOFade(0f, 1f).OnComplete(() => backGround.rectTransform.DOMoveY(2720.0f, 0f));
            LineImage.DOFade(0, 1f).OnComplete(() => LineImage.rectTransform.DOMoveY(2720.0f, 0f));
            ChapterName.DOFade(0, 1f).OnComplete(() => ChapterName.rectTransform.DOMoveY(2720.0f, 0f));
            ConfirmButton.DOFade(0, 1f).OnComplete(() => ConfirmButton.rectTransform.DOMoveY(2720.0f, 0f));
            ConfirmButtonAni.DOFade(0, 1f).OnComplete(() => ConfirmButtonAni.rectTransform.DOMoveY(2720.0f, 0f));
            StoryText.DOFade(0, 1f).OnComplete(() =>  StoryText.rectTransform.DOMoveY(2720.0f, 0f) );
            ConfirmButtonAni.DOFade(0, 0f).OnComplete(() => ConfirmButtonAni.DOKill());

            //TUTORIAL
            if (stageName == "STAGE01")
            {
                GameObject.Find("/TutorialTrigger/MoveTrigger01").GetComponent<Tutorial>().SetTrueTutorialEnable();
            }
            ConfirmButton.GetComponent<GraphicRaycaster>().enabled = false;

        }
        else
            return;
        // Fado Out -------------------------------
    }
}
