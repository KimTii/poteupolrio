﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

// Monster State FSM Script
// Crater is Jeun Wang Jeun
// crate date .2015.07.29

// phone : 010 4134 2929 

public class MonsterFSM : MonoBehaviour {
    public enum MonsterState
    {
        Idle,
        Trace,
        attack,
        Patrol
    }
    public MonsterState monsterState = MonsterState.Idle;



    // 내부 변수 셋팅
    private Transform playerTr;
    private Animator monsterAni;
    private NavMeshAgent nvAgent;
    private bool isTraceMonster;

    // 추적 반경
    public float traceDist = 3f;
    // 공격 반경
    public float attackDist = 1.5f;
    // 순찰 반경
    public float patrolRange = 5.0f;

    // ani temp 
    float aniDistFloat = 0.0f;
    float aniDistFloat_Attack = 0.0f;
    bool arrivalPatrol = false;
    Vector3 enteredTransform;
    Vector3 point;

    Vector3 checkBeforePosition;
    int checkStandNum = 1;

    void Start()
    {

        //DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(300, 10);
        nvAgent = GetComponent<NavMeshAgent>();
        playerTr = GameObject.Find("Player").transform;
        monsterAni = GetComponent<Animator>();
        isTraceMonster = false;

        StartCoroutine("CheckMonsterState");
        StartCoroutine("MonsterAction");


        // 순찰 범위 초기화
        enteredTransform = this.transform.position;
        point = this.transform.position;
    }

    IEnumerator CheckMonsterState()
    {
        while(true)
        {
            //Debug.Log(Vector3.Distance(transform.position, point));
            yield return new WaitForSeconds(0.2f);

            // 몬스터와 플레이어 거리 측정하기
            float dist = Vector3.Distance(playerTr.position, this.transform.position);

            // 공격 범위 안에 들어왔는지
            if (dist <= attackDist) { monsterState = MonsterState.attack; }
            // 추적 반경에 들어 왔는지
            else if (dist <= traceDist) {
                arrivalPatrol = false;
                monsterState = MonsterState.Trace;


                
                enteredTransform = this.transform.position;
            }


            else // 몬스터 순찰 및 정지 상태
            {
                if (nvAgent.enabled == false)
                    nvAgent.enabled = true;
                // 몬스터가 순찰을 마추었다면
                if (Vector3.Distance(transform.position, point) <= 0.2f)
                {
                    arrivalPatrol = true;

                    monsterState = MonsterState.Idle;

                    // Idle로 잠시 머물러 있는 시간
                    yield return new WaitForSeconds(0.6f);
                }
                    

                // 순찰을 돌아야 하면
                if (arrivalPatrol)
                {

                    // 랜덤 순찰 범위 판정
                    if (RandomPoint(enteredTransform, patrolRange, out point))
                    {

                        Debug.DrawRay(point, Vector3.up, Color.red, 1.0f);

                        this.transform.DOLookAt(new Vector3(point.x, this.transform.position.y, point.z), 0.15f);
                        nvAgent.SetDestination(point);
                        //nvAgent.destination = point;
                    }

                    monsterState = MonsterState.Patrol;
                    arrivalPatrol = false;
                }
            }
        }
    }

    
    IEnumerator MonsterAction()
    {
        while (true)
        {
                yield return new WaitForSeconds(0.2f);
            switch(monsterState)
            {
                case MonsterState.Idle:              // 몬스터 Idle
                    // 길이 막고있는걸 인지하고있는 시간 체크 초기화
                    checkStandNum = 0;
                    // 애니메이션 해제
                    if (monsterAni.GetBool("Run"))
                        monsterAni.SetBool("Run", false);

                    if (monsterAni.GetBool("Attack"))
                        monsterAni.SetBool("Attack", false);

                    point = this.transform.position;
                    break;

                case MonsterState.Trace:             // 플레이어 추적하기
                    float dist = Vector3.Distance(playerTr.position, this.transform.position);
                    if (dist >= traceDist)
                        monsterState = MonsterState.Idle;
                    
                    // -- Ani
                    if( monsterAni.GetBool("Attack") )
                        monsterAni.SetBool("Attack", false);
                    if (!monsterAni.GetBool("Run"))
                        monsterAni.SetBool("Run", true);
                    // -- Ani en                 d
                
                    
                    if (!nvAgent.enabled)
                        nvAgent.enabled = true;

                    // 몬스터 회전 & gogo
                    this.transform.DOLookAt(new Vector3(playerTr.position.x, this.transform.position.y, playerTr.position.z), 0.15f);
                    nvAgent.destination = playerTr.position;
                    break;

                case MonsterState.attack:                   // 플레이어 공격하기
                    if( monsterAni.GetBool("Run") )
                        monsterAni.SetBool("Run", false);


                    monsterAni.SetBool("Attack", true);

                    if (nvAgent.enabled)
                        nvAgent.enabled = false;

                    // 바라보는 시점을 실시간으로 변경
                    this.transform.DOLookAt(new Vector3(playerTr.position.x, this.transform.position.y, playerTr.position.z), 0.15f);

                    

                    monsterAni.SetBool("Attack", true);
                    break;

                case MonsterState.Patrol:                   // 몬스터가 순찰
                    // 애니메이션 셋팅
                    if (monsterAni.GetBool("Attack"))
                        monsterAni.SetBool("Attack", false);

                    if (!monsterAni.GetBool("Run"))
                        monsterAni.SetBool("Run", true);
                    // -----
                    ResetPosition();
                    
                    ++checkStandNum;
                    break;
            }
            yield return null;
        }
    }

    void ResetPosition()
    {
        if (checkStandNum == 10)
        {
            checkBeforePosition = this.transform.position;
            checkStandNum = 0;
        }

        if (checkBeforePosition == this.transform.position)
            point = this.transform.position;
    }

    void Rotation()
    {
        Vector3 direction = nvAgent.velocity;
        Quaternion targetRotation; 

            if (direction != Vector3.zero)
            {
                targetRotation = Quaternion.LookRotation(direction);
                this.transform.DORotate(new Vector3(targetRotation.eulerAngles.x,
                                                     targetRotation.eulerAngles.y,
                                                     targetRotation.eulerAngles.z), 0.5f);
        }
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }
}
