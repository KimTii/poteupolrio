﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class WorldButtonMng : MonoBehaviour {
    public ParticleSystem miscParticle;
    public GameObject[] WorldMapParticle;
    public AudioClip WorldMapBgm; 

    bool ButtonDrag = false;
    bool SBButtonDrag = false;
    bool BGSoundDrag = false;
    bool SkipButtonOn = false;
    bool OneButtonClickDrag = false; 

    public bool StartCinemeticStage;
    

   void Start()
   { 
       DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(100, 10);
       StartCinemeticStage = false;
   }


   // WorldMap UI InterFace Icon..
   // -----------------------------------------------------------------------------------------------------------
    public void WorldOptionButton()
    {
        GetComponent<AudioSource>().Play();
        if( ButtonDrag == false )
        {
            ButtonDrag = true;
            GameObject.Find("OptionSet").transform.DOLocalMoveX(-350f, 1f);
        }
            
        else
        {
            ButtonDrag = false;
            GameObject.Find("OptionSet").transform.DOLocalMoveX(-900, 1f);
        }
            

        Debug.Log("Option Button Click");
    }
    public void StroyBoardButton()
    {
        GetComponent<AudioSource>().Play();
        if (SBButtonDrag == false)
        {
            SBButtonDrag = true;
            GameObject.Find("StoryBoardSet").transform.DOLocalMoveX(370f, 1f);
        }

        else
        {
            SBButtonDrag = false;
            GameObject.Find("StoryBoardSet").transform.DOLocalMoveX(915f, 1f);
        }


        Debug.Log("Option Button Click");

    }
    public void WorldAcivementButton() {
        GetComponent<AudioSource>().Play();
       // GPGmng.Instance.ShowAchievmentUI(); 
    }
    public void WorldLeaderBoardButton() 
    { 
        GetComponent<AudioSource>().Play(); 
      //  GPGmng.Instance.ShowLeaderBoard(); 
    }
    public void WorldOptionLogOutButton() {

        GetComponent<AudioSource>().Play();
        // 로그아웃 됬으면
        GPGmng.Instance.LogOutGPGS(); 

        // 다시 월드맵을 리로드해라 로그인 다시받어
        if ( GPGmng.Instance.bLogin == false )
        { Application.LoadLevelAsync("WorldMap"); }
    
    }
    public void OptionExitButton() { GetComponent<AudioSource>().Play(); Application.Quit(); }
    public void OptionSoundButton() 
    { 
        GetComponent<AudioSource>().Play(); 
        Debug.Log("Button Click");

        if(BGSoundDrag == false)
        {
            GameObject.Find("Main Camera").GetComponent<AudioSource>().DOFade(0, 2f);
            BGSoundDrag = true;
        }
        else
        {
            GameObject.Find("Main Camera").GetComponent<AudioSource>().DOFade(1f, 2f);
            BGSoundDrag = false;
        }
        
    }
    public void OptionUserIdShow() { GetComponent<AudioSource>().Play(); GPGmng.Instance.GetNameGPGS(); }
    // -----------------------------------------------------------------------------------------------------------

    // Menu Button Icon
    //public void UIMenuButton()
    //{
    //    GetComponent<AudioSource>().Play();
    //    GameObject.Find("UIMenuIconButton").GetComponent<AudioSource>().Play();
    //    // image Animation
    //    if(!ButtonDrag)
    //    {
            
    //        // Image mesh 를 켜주고
    //        GameObject.Find("AchivementButton").GetComponent<Image>().enabled = true;
    //        GameObject.Find("LeaderBoardButton").GetComponent<Image>().enabled = true;
    //        GameObject.Find("SoundButton").GetComponent<Image>().enabled = true;
    //        GameObject.Find("LogOutButton").GetComponent<Image>().enabled = true;
    //        GameObject.Find("ExitButton").GetComponent<Image>().enabled = true;


    //        GameObject.Find("AchivementButton").GetComponent<Image>().rectTransform.DOLocalMoveY(1.13736f, 0.3f);
    //        GameObject.Find("LeaderBoardButton").GetComponent<Image>().rectTransform.DOLocalMoveY(0.6654358f,  0.3f);
    //        GameObject.Find("SoundButton").GetComponent<Image>().rectTransform.DOLocalMoveY(0.1934929f, 0.3f);
    //        GameObject.Find("LogOutButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-0.2784424f, 0.3f);
    //        GameObject.Find("ExitButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-0.7503815f, 0.3f);

    //        ButtonDrag = true;
    //    }
        
    //    else
    //    {
    //        GameObject.Find("AchivementButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-1.3f, 0.3f).OnComplete( () =>
    //            GameObject.Find("AchivementButton").GetComponent<Image>().enabled = false);
    //        GameObject.Find("LeaderBoardButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-1.3f, 0.3f).OnComplete(() =>
    //            GameObject.Find("LeaderBoardButton").GetComponent<Image>().enabled = false);
    //        GameObject.Find("SoundButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-1.3f, 0.3f).OnComplete(() =>
    //            GameObject.Find("SoundButton").GetComponent<Image>().enabled = false);
    //        GameObject.Find("LogOutButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-1.3f, 0.3f).OnComplete(() =>
    //            GameObject.Find("LogOutButton").GetComponent<Image>().enabled = false);
    //        GameObject.Find("ExitButton").GetComponent<Image>().rectTransform.DOLocalMoveY(-1.3f, 0.3f).OnComplete(() =>
    //            GameObject.Find("ExitButton").GetComponent<Image>().enabled = false);

    //        ButtonDrag = false;
    //    }
    //}

    // Stage 0 Play Button
    public void CinemaTicSceneButton()
    {
        StartCoroutine("CinemaTicSceneButtonFade");
    }
    
    //// Stage Play Button 
    //public void Stage01() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "GRASS01");}
    //public void Stage02() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "GRASS02"); }
    //public void Stage03() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "GRASS03"); }

    //public void Desert01() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "DESERT01"); }
    //public void Ice01() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "ICE01"); }
    //public void Hell01() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "HELL01"); }

    //// -- TEST --

    //public void Stage04() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "STAGE04 PushBox"); }
    //public void Stage05() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "STAGE05 LetsWarp"); }
    //public void Stage06() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "Combination"); }
    //public void Stage07() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "HurryUp"); }
    //public void Stage08() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "Socoban"); }
    //public void Stage09() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "temp"); }
    //public void Stage10() { MovePlanet(); this.GetComponent<Loading>().StartCoroutine("StartLoad", "temp2"); }

    //--------------------------------------------------------------------------------------------
    public void STAGE08() {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE08");
        OneButtonClickDrag = true;
    }

    public void STAGE09()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE09");
        OneButtonClickDrag = true;
    }

    public void STAGE10()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE10");
        OneButtonClickDrag = true;
    }

    public void STAGE11()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE11");
        OneButtonClickDrag = true;
    }

    public void STAGE12()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE12");
        OneButtonClickDrag = true;
    }

    public void STAGE13()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE13");
        OneButtonClickDrag = true;
    }

    public void STAGE14()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE14");
        OneButtonClickDrag = true;
    }

    public void STAGE15()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE15");
        OneButtonClickDrag = true;
    }

    //--------------------------------------------------------------------------------------------

    public void TESTSTAGE01() 
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE01");
        OneButtonClickDrag = true;
    }

    public void TESTSTAGE02()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE02");
        OneButtonClickDrag = true;
    }
    public void TESTSTAGE03()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE03");
        OneButtonClickDrag = true;
    }
    public void TESTSTAGE04()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE04");
        OneButtonClickDrag = true;
    }
    public void TESTSTAGE05()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE05");
        OneButtonClickDrag = true;
    }

    public void TESTSTAGE06()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE06");
        OneButtonClickDrag = true;
    }

    public void TESTSTAGE07()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE07");
        OneButtonClickDrag = true;
    }

    public void TESTSTAGE08()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE08");
        OneButtonClickDrag = true;
    }

    public void TESTSTAGE09()
    {
        if (OneButtonClickDrag)
            return;

        MovePlanet("STAGE09");
        OneButtonClickDrag = true;
    }

    // Cinematic Event Fade Start
    IEnumerator CinemaTicSceneButtonFade()
    {
        //GameObject.Find("Canvas").GetComponent<Canvas>().enabled = false;
        GameObject.Find("FadeImage").GetComponentInChildren<Image>().DOFade(1, 4.0f)
            .OnComplete(() => GameObject.Find("FadeImage").GetComponentInChildren<Image>().DOFade(0, 4.0f));

        GameObject.Find("Script01").GetComponent<Text>().DOFade(1, 4.0f)
            .OnComplete(() => GameObject.Find("Script01").GetComponent<Text>().DOFade(0, 4.0f));

        yield return new WaitForSeconds(4f);

        CinameticStagePlaySetting();

        //yield return new WaitForSeconds(3.5f);
        this.StartCinemeticStage = true;

        yield return null;
    }

    void MovePlanet(string stageName)
    {

        GameObject.Find("Planet").transform.DOScale(0.7f, 1.5f)
            .OnComplete(() => StartCoroutine("StageInAniMation", stageName));

        GameObject.Find("Manager").GetComponent<AudioSource>().Play();

        //GameObject.Find("Cloud01").transform.DOMoveX(0, 1.0f).SetEase(Ease.OutExpo);
        //GameObject.Find("Cloud02").transform.DOMoveX(0, 1.0f).SetEase(Ease.OutExpo);

        GameObject.Find("PlanetFlare").SetActive(false);
    }
    IEnumerator StageInAniMation(string stageName)
    {
        yield return new WaitForSeconds(0.3f);

        GameObject.Find("Planet").transform.DOScale(3.5f, 3.0f).SetEase(Ease.InQuint);
        // GameObject.Find("Planet").SetActive(false);

        yield return new WaitForSeconds(2.5f);
        GameObject.Find("Cloud01").transform.DOMoveX(0, 1.0f);
        GameObject.Find("Cloud02").transform.DOMoveX(0, 1.0f);

        yield return new WaitForSeconds(1.0f);
        this.GetComponent<Loading>().StartCoroutine("StartLoad", stageName);

         yield return null;
    }

    void CinameticStagePlaySetting()
    {
        Camera.main.GetComponent<AudioSource>().clip = this.GetComponent<OpeningStage>().billigeBgm;
        Camera.main.GetComponent<AudioSource>().Play();
        // Blur 주기


        // 월드맵 잔상 날리기
        GameObject.Find("DirectorBackGround").transform.SetSiblingIndex(1);
        GameObject.Find("Planet").transform.position = new Vector3(999, 999, 999);

        Camera.main.orthographic = false;
        Camera.main.transform.position = new Vector3(28.0f, 5.3f, 2.3f);
        Camera.main.transform.Rotate(new Vector3(50.0f, -40.0f, 0f));
    }

    

    public bool Tempbool = false;
    public void IntroSkipButton() 
    {
        //FadeOut Ani
        GameObject.Find("FadeImage").GetComponentInChildren<Image>().DOFade(1, 4.0f)
            .OnComplete( () =>  Application.LoadLevelAsync("WorldMap")
            );

        // 스킵 버튼은 한번만 눌렀을 때
        if ( !SkipButtonOn  )
        {
            SkipButtonOn = true;
            if (GameData.Instance.GetStageInfo() == 0 )
            {
                GameData.Instance.NextStageInfo();
                SaveGameInfo.SaveStageInfo();
            }
        }   
    }
}
