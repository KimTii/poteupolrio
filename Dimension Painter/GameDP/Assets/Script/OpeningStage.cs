﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class OpeningStage : MonoBehaviour {
    public AudioClip billigeBgm;
    public AudioClip darkBilligeBgm;

    public byte prograssNumber = 0;
    public bool stoppingDotween = false;

    bool startOpening = false;
    bool MoveChangeDrag = false;
    bool Temp = false;
    bool Temp02 = false;
    Image FadeImage;

    // 초기화
    void Start()
    {

        FadeImage = GameObject.Find("FadeImage").GetComponent<Image>();
        StartCoroutine("CrFUpdate");
    }

    // 업데이트
    IEnumerator CrFUpdate()
    {
        while(true)
        {
            yield return new WaitForFixedUpdate();

            if (GetComponent<WorldButtonMng>().StartCinemeticStage && prograssNumber == 0)
            {
                prograssNumber += 1;
                StartCoroutine("StartFadeIn");
            }
               

            if (startOpening && GetComponent<WorldButtonMng>().StartCinemeticStage && prograssNumber == 1)
            {
                StartCoroutine("MoveToCamera");
                prograssNumber += 1;
            }


            if (MoveChangeDrag && prograssNumber == 2 && GetComponent<WorldButtonMng>().StartCinemeticStage)
            {
                prograssNumber += 1;
                StartCoroutine("MoveToCamera02");
            }

            if (Temp && prograssNumber == 3 && GetComponent<WorldButtonMng>().StartCinemeticStage)
            {
                prograssNumber += 1;
                StartCoroutine("MoveToCamera03");
            }

            if (Temp02 && prograssNumber == 4 && GetComponent<WorldButtonMng>().StartCinemeticStage)
            {
                prograssNumber += 1;
                StartCoroutine("DarkDirection01");
            }

        }
    }

    // 처음 시작 할 때의 페이드 인을 연출 합니다.
    IEnumerator StartFadeIn()
    {
        GameObject.Find("EventSkipButton").transform.localPosition = new Vector3(1250f, -250f, 0);
        GameObject.Find("EventSkipButton").GetComponent<Image>().DOColor(new Color(255, 255, 0), 2.0f)
            .OnComplete(() => GameObject.Find("EventSkipButton").GetComponent<Image>().DOColor(new Color(255f, 255f, 255F), 2.0f))
            .SetLoops(-1, LoopType.Yoyo);

        yield return new WaitForSeconds(5.0f);
        startOpening = true;

        yield return null;
    }

    // 카메라의 연출이 시작됩니다.
    IEnumerator MoveToCamera()
    {
        Camera.main.transform.DOMove(GameObject.Find("CameraPoint01").transform.position, 10.0f)
            .OnComplete( () => MoveChangeDrag = true );

        GameObject.Find("Credit01").GetComponent<Image>().DOFade(1, 4.0f);

        yield return new WaitForSeconds(6.0f);

        GameObject.Find("Credit01").GetComponent<Image>().DOFade(0, 1.0f);
        FadeImage.GetComponent<Image>().DOFade(1, 4.0f);

        GameObject.Find("Script02").GetComponent<Text>().DOFade(1, 4.0f)
           .OnComplete(() => GameObject.Find("Script02").GetComponent<Text>().DOFade(0, 4.0f));

        yield return null;
    }

     IEnumerator MoveToCamera02()
    {
        FadeImage.DOFade(0, 4.0f);
        Camera.main.transform.DOMove(GameObject.Find("CameraPoint02").transform.position, 10.0f).OnComplete(() => Temp = true);
        GameObject.Find("Credit02").GetComponent<Image>().DOFade(1, 4.0f);

        Camera.main.transform.position = new Vector3(26.0f, 4.7f, 13.0f);
        Camera.main.transform.rotation = Quaternion.Euler(40, 210, 0);

        yield return new WaitForSeconds(6.0f);

        GameObject.Find("Credit02").GetComponent<Image>().DOFade(0, 1.0f);
        FadeImage.DOFade(1, 4.0f);

        GameObject.Find("Script03").GetComponent<Text>().DOFade(1, 4.0f)
          .OnComplete(() => GameObject.Find("Script03").GetComponent<Text>().DOFade(0, 4.0f));

        yield return null;
    }

    IEnumerator MoveToCamera03()
    {
        FadeImage.DOFade(0, 4.0f);
        Camera.main.transform.DOMoveY(7.7f, 8.0f).OnComplete(() => FadeImage.DOFade(1, 4.0f)
            .OnComplete( () => Temp02 = true )
       );

        GameObject.Find("Credit03").GetComponent<Image>().DOFade(1, 4.0f);

        Camera.main.transform.position = new Vector3(24f, 4.0f, 8.8f);
        Camera.main.transform.rotation = Quaternion.Euler(62, 270, 0f);

        yield return new WaitForSeconds(8.0f);

        GameObject.Find("Credit03").GetComponent<Image>().DOFade(0, 1.0f);

        GameObject.Find("Script04").GetComponent<Text>().DOFade(1, 4.0f)
          .OnComplete(() => GameObject.Find("Script04").GetComponent<Text>().DOFade(0, 4.0f));
    }

    IEnumerator DarkDirection01()
    {
        GameObject.Find("DirectorBackGround").transform.SetSiblingIndex(0);
        FadeImage.DOFade(0, 4.0f);

        Camera.main.transform.position = new Vector3(21.12f, 4.2f, 1.88f);
        Camera.main.transform.rotation = Quaternion.Euler(32.0f, 8.7f, 2.5f);

        GameObject.Find("Directional light").transform.DORotate(new Vector3(-15f, 330f, 0), 2.0f);
        GameObject.Find("DirectorBackGround").GetComponent<Image>().DOColor(new Color(0, 0, 0, 255), 2.0f);

        Camera.main.GetComponent<AudioSource>().clip = darkBilligeBgm;
        Camera.main.GetComponent<AudioSource>().Play();

        yield return null;
    }
}
