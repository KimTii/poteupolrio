using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Basics : MonoBehaviour
{
	public Transform cubeA, cubeB ,cubeC;
    AudioSource test2;

	void Start()
	{
        test2 = GameObject.Find("Main Camera").GetComponent<AudioSource>();

		// Initialize DOTween (needs to be done only once).
		// If you don't initialize DOTween yourself,
		// it will be automatically initialized with default values.
		DOTween.Init(false, true, LogBehaviour.Verbose).SetCapacity(1000,100);

		// Create two identical infinitely looping tweens,
		// one with the shortcuts way and the other with the generic way.
		// Both will be set to "relative" so the given movement will be calculated
		// relative to each target's position.

		// cubeA > SHORTCUTS WAY
        cubeA.DOMove(new Vector3(-2, 2, 0), 1).SetRelative().SetLoops(-1, LoopType.Restart);
       // cubeC.DOMove(new Vector3(1, 1, 1), 1).SetRelative().SetLoops(-1, LoopType.Yoyo);
        

		// cubeB > GENERIC WAY
		DOTween.To(()=> cubeB.position, x=> cubeB.position = x, new Vector3(-2, 2, 0), 1).SetRelative().SetLoops(-1, LoopType.Yoyo);
        //DOTween.To(()=> cubeC.position, Y=> cubeC.position = Y, new Vector3(1, 1, 1), 1);

        //cubeC.DORotate(new Vector3(0, -90f, 0), 1).SetRelative().SetLoops(-1, LoopType.Incremental);
        cubeC.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 3).SetRelative().SetLoops(-1, LoopType.Incremental);
       // cubeC.DOJump(new Vector3(0, 1, 0), 1f, 1, 1, false).SetRelative().SetLoops(-1, LoopType.Yoyo);
        //
        test2.DOFade(1f, 10f);



		// Voilà.
		// To see all available shortcuts check out DOTween's online documentation.
	}

    void Update()
    {

       
        if( Input.GetKeyDown(KeyCode.A) )
        {
            print(cubeC.DOScale(new Vector3(10f, 10f, 10f), 10f).IsComplete());
                DOTween.PauseAll();
            
           
        }
        if (Input.GetKeyDown(KeyCode.S))
        {

            cubeC.DOScale(new Vector3(1f, 1f, 1f), 3).Complete();
            DOTween.PlayAll();
        }

    }
}