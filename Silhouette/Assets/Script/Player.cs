﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    public class PlayerInfomation
    {
        int date;
        List<string> enableCharacters;
        Dictionary<Common.Position, int> friendships;

        public PlayerInfomation() { }
        public PlayerInfomation(List<string> _enableCharacters)
        {
            enableCharacters = new List<string>();
            friendships = new Dictionary<Common.Position, int>();

            date = 0;
            enableCharacters = _enableCharacters;
            friendships.Add(Common.Position.FreeCompany, 0);
            friendships.Add(Common.Position.EastKingdom, 35);
            friendships.Add(Common.Position.NorthKingdom, 100);
        }
        public string GetRandomName() { return enableCharacters[Random.Range(0, enableCharacters.Count)]; }
        public void SetFriendship(Common.Position _position, int _number, bool _up)
        {
            int value;
            if (_up)
                value = friendships[_position] + _number;
            else
                value = friendships[_position] - _number;

            if (value < 0)
                value = 0;

            friendships[_position] = value;
        }
        public Dictionary<Common.Position, int> GetFriendships() { return friendships; }
    }
}
