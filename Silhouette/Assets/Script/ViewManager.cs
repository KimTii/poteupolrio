﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ViewManager : MonoBehaviour {
    // Sprite 교체 방법
    // 스프라이트객체.GetComponent<UISprite>().spriteName = "바꿀 스프라이트명"; 

    DB db;
    ControllerManager controllerManager;

    public UIPanel characterPanel;
    public UISprite speechBubble;
    public UISprite characterSprite;
    public UILabel quesion;
    
    public UIPanel answerWindow;
    public UILabel answer1;
    public UILabel answer2;

    public UIPanel friendshipPanel;
    public UISprite friendshipButton;
    public UISprite friendshipFrame;
    public UILabel freeCompanyFriendship;
    public UILabel eastKingdomFriendship;
    public UILabel northKingdomFriendship;

    List<UIPanel> panels;

    void Start () {
        db = GetComponent<DB>();
        controllerManager = GetComponent<ControllerManager>();
        panels = new List<UIPanel>();

        // Title을 제외한 모든 Panle의 Alpha를 0으로 설정한다.
        // 나중에 리스트를 만들 필요가 없어지면 되돌리자.
        panels.Add(characterPanel);
        panels.Add(answerWindow);
        panels.Add(friendshipPanel);
        foreach(UIPanel member in panels)
        {
            member.alpha = 0f;
        }
    }

    public IEnumerator Fade(UIPanel panel, bool fadein, float speed = 1f)
    {
        if(fadein) {
            while (panel.alpha < 1f) {
                yield return null;
                panel.alpha += Time.deltaTime * speed;
            }
        } else {
            while (panel.alpha > 0f) {
                yield return null;
                panel.alpha -= Time.deltaTime * speed;
            }
        }
    }
    public IEnumerator Fade(UISprite sprite, bool fadein, float speed = 1f)
    {
        if (fadein)
        {
            while (sprite.alpha < 1f)
            {
                yield return null;
                sprite.alpha += Time.deltaTime * speed;
            }
        }
        else
        {
            while (sprite.alpha > 0f)
            {
                yield return null;
                sprite.alpha -= Time.deltaTime * speed;
            }
        }
    }
    public void GameStart()
    {
        speechBubble.alpha = 0f;
        characterSprite.alpha = 0f;
        characterPanel.alpha = 1f;
        friendshipFrame.alpha = 0f;
        friendshipFrame.transform.localPosition = new Vector2(0, 258);

        SettingGuest();
        StartCoroutine(EnterCharacter(characterSprite));
    }
    void SettingGuest()
    {
        controllerManager.InviteGuest();
        characterSprite.spriteName = controllerManager.GetName();
        quesion.text = controllerManager.GetQuesion();
        answer1.text = controllerManager.GetAnswer1();
        answer2.text = controllerManager.GetAnswer2();
    }

    IEnumerator EnterCharacter(UISprite sprite)
    {
        characterSprite.transform.localPosition = new Vector2(0f, -27f);
        characterSprite.transform.localScale = new Vector2(0.5f, 0.5f);
        yield return StartCoroutine(Fade(sprite, true, 2));
        yield return StartCoroutine(Fade(sprite, false, 2));

        characterSprite.transform.localPosition = new Vector2(0f, -75f);
        characterSprite.transform.localScale = new Vector2(1f, 1f);
        yield return StartCoroutine(Fade(sprite, true, 2));
        yield return StartCoroutine(Fade(sprite, false, 2));

        characterSprite.transform.localPosition = new Vector2(0f, -150f);
        characterSprite.transform.localScale = new Vector2(1.8f, 1.8f);
        yield return StartCoroutine(Fade(sprite, true, 2));

        // UI 커지는 순서
        yield return StartCoroutine(Fade(speechBubble, true));
        StartCoroutine(Fade(answerWindow, true));
        StartCoroutine(Fade(friendshipPanel, true));
    }
    IEnumerator ExitCharacter(UISprite sprite)
    {
        yield return StartCoroutine(Fade(answerWindow, false));
        yield return StartCoroutine(Fade(speechBubble, false));
        yield return StartCoroutine(Fade(sprite, false));
    }

    public void AnswerButton(GameObject _button) { controllerManager.AnswerButton(_button); }
    // 우호도
    public void Friendship(GameObject _button)
    {
        if (string.Equals(_button.name, "FriendshipButton"))
        {
            StartCoroutine(FriendshipCoroutine(false));
            // 우호도 업데이트
            FriendshipUpdate();
        }
        else
            StartCoroutine(FriendshipCoroutine(true));
    }
    IEnumerator FriendshipCoroutine(bool _friendshipButtonOn)
    {
        if(_friendshipButtonOn)
        {
            friendshipFrame.gameObject.GetComponent<BoxCollider>().enabled = false;
            StartCoroutine(Fade(friendshipFrame, false, 2));
            StartCoroutine(Fade(friendshipButton, true, 2));
            yield return new WaitForSeconds(1f);
            friendshipButton.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
        else
        {
            friendshipButton.gameObject.GetComponent<BoxCollider>().enabled = false;
            StartCoroutine(Fade(friendshipButton, false, 2));
            StartCoroutine(Fade(friendshipFrame, true, 2));
            yield return new WaitForSeconds(1f);
            friendshipFrame.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }
    void FriendshipUpdate()
    {
        int value = 0;
        string state;
        Color color = new Color();

        Dictionary<Common.Position, int> friendships = controllerManager.GetFriendships();
        foreach (KeyValuePair<Common.Position, int> member in friendships)
        {
            value = friendships[member.Key];
            state = SetFriendshipState(value);
            color = SetFriendshipState(value, color);

            switch (member.Key)
            {
                case Common.Position.FreeCompany:
                    if (value < 10)
                        freeCompanyFriendship.text = "부족";
                    else if (value < 50)
                        freeCompanyFriendship.text = "약간 부족";
                    else if (value < 80)
                        freeCompanyFriendship.text = "보통";
                    else if (value >= 80)
                        freeCompanyFriendship.text = "양호";
                    else
                        freeCompanyFriendship.text = "ERROR!!";
                    freeCompanyFriendship.color = color;
                    break;
                case Common.Position.EastKingdom:
                    eastKingdomFriendship.text = state;
                    eastKingdomFriendship.color = color;
                    break;
                case Common.Position.NorthKingdom:
                    northKingdomFriendship.text = state;
                    northKingdomFriendship.color = color;
                    break;
            }  
        }
    }

    string SetFriendshipState(int _value)
    {
        if (_value < 10)
            return "적대적";
        else if (_value < 40)
            return "약간 적대적";
        else if (_value < 60)
            return "중립적";
        else if (_value < 80)
            return "약간 우호적";
        else if (_value >= 80)
            return "우호적";
        else
            return "ERROR!!";
    }
    Color SetFriendshipState(int _value, Color _color)
    {
        if (_value < 10)
            return Color.red;
        else if (_value < 40)
            return new Color(1f, 0.5f, 0.5f);
        else if (_value < 60)
            return Color.white;
        else if (_value < 80)
            return new Color(0.5f, 1f, 0.5f);
        else if (_value >= 80)
            return Color.green;
        else
            return Color.blue;
    }
}
