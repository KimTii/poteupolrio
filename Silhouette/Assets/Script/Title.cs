﻿using UnityEngine;
using System.Collections;

public class Title : MonoBehaviour {

    public ViewManager viewManager;
    UIPanel panel;
    bool continueTitle;

	// Use this for initialization
	void Awake () {
        viewManager = viewManager.GetComponent<ViewManager>();
        panel = GetComponent<UIPanel>();
        panel.alpha = 0f;
        continueTitle = false;

        StartCoroutine(TitleStart());
	}

    IEnumerator TitleStart()
    {
        while(panel.alpha < 1f)
        {
            yield return null;
            panel.alpha += Time.deltaTime;
        }
        continueTitle = true;
    }

    public void OnClick()
    {
        if (continueTitle)
        {
            continueTitle = false;
            StartCoroutine(TitleEnd());
        }
    }
    IEnumerator TitleEnd()
    {
        while (panel.alpha > 0f)
        {
            yield return null;
            panel.alpha -= Time.deltaTime;
        }
        viewManager.GameStart();
        Destroy(panel.gameObject);
    }
}
