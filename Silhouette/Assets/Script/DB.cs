﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DB : MonoBehaviour {

    List<Character> characters;

    public class Character
    {
        Common.Position position;
        string name;
        List<Script> scripts;

        public Character() { }
        public Character(Common.Position _position, string _name, Script _script)
        {
            scripts = new List<Script>();

            position = _position;
            name = _name;
            scripts.Add(_script);
        }

        public string GetName() { return name; }
        public Script GetRandomScript() { return scripts[Random.Range(0, scripts.Count)]; }
        public void TESTAddScript(Script _script) { scripts.Add(_script); }
    }
    public class Script
    {
        string quesion;
        string answer1;
        ScriptEffectOrCondition answer1Effect;
        string answer2;
        ScriptEffectOrCondition answer2Effect;
        List<ScriptEffectOrCondition> conditions;

        public Script() { }
        public Script(string _quesion, string _answer1, string _answer2)
        {
            quesion = _quesion;
            answer1 = _answer1;
            answer1Effect = new ScriptEffectOrCondition(Common.Position.FreeCompany, 0, true);
            answer2 = _answer2;
            answer2Effect = new ScriptEffectOrCondition(Common.Position.FreeCompany, 0, true);
            conditions = new List<ScriptEffectOrCondition>();
        }
        public Script(string _quesion,
            string _answer1, Common.Position _answer1Position, int _answer1Number, bool _answer1Up,
            string _answer2, Common.Position _answer2Position, int _answer2Number, bool _answer2Up)
        {
            quesion = _quesion;
            answer1 = _answer1;
            answer1Effect = new ScriptEffectOrCondition(_answer1Position, _answer1Number, _answer1Up);
            answer2 = _answer2;
            answer2Effect = new ScriptEffectOrCondition(_answer2Position, _answer2Number, _answer2Up);
        }
        public Script(string _quesion, 
            string _answer1, Common.Position _answer1Position, int _answer1Number, bool _answer1Up,
            string _answer2, Common.Position _answer2Position, int _answer2Number, bool _answer2Up,
            Common.Position _position, int _number, bool _up)
        {
            quesion = _quesion;
            answer1 = _answer1;
            answer1Effect = new ScriptEffectOrCondition(_answer1Position, _answer1Number, _answer1Up);
            answer2 = _answer2;
            answer2Effect = new ScriptEffectOrCondition(_answer2Position, _answer2Number, _answer2Up);
            conditions = new List<ScriptEffectOrCondition>();
            ScriptEffectOrCondition condition = new ScriptEffectOrCondition(_position, _number, _up);
            conditions.Add(condition);
        }
        public string GetQuesion() { return quesion; }
        public string GetAnswer1() { return answer1; }
        public string GetAnswer2() { return answer2; }
        public ScriptEffectOrCondition GetAnswer1Effect() { return answer1Effect; }
        public ScriptEffectOrCondition GetAnswer2Effect() { return answer2Effect; }
    }
    public class ScriptEffectOrCondition
    {
        Common.Position position;
        int number;
        bool up;

        public ScriptEffectOrCondition() { }
        public ScriptEffectOrCondition(Common.Position _position, int _number, bool _up)
        {
            position = _position;
            number = _number;
            up = _up;
        }
        public Common.Position GetPosition() { return position; }
        public int GetNumber() { return number; }
        public bool GetUp() { return up; }
    }

    void Awake()
    {
        characters = new List<Character>();
        Test();
    }

    public Character GetCurrentCharacter(string name)
    {
        foreach (Character member in characters)
        {
            if (name.Equals(member.GetName()))
                return member;
        }
        return null;
    }
    
    public List<string> GetCharactersName()
    {
        // Player.cs 의 PlayerInformation Initialization을 위한 함수
        List<string> charactersName = new List<string>();
        foreach(Character member in characters)
        {
            charactersName.Add(member.GetName());
        }
        return charactersName;
    }
    

    void Test()
    {
        Script sc = new Script("안녕하세요 단장님!", 
            "좋은 아침이군.", Common.Position.FreeCompany, 1, true, 
            "시끄럽다.", Common.Position.FreeCompany, 1, false);
        Character ch = new Character(Common.Position.FreeCompany, "05", sc);
        characters.Add(ch);

        Script sc1 = new Script("얼마나 대단한 얼굴을 가지고 있는지 보러 왔다!", "아침부터 시끄럽군.",  "마음껏 보게나.");
        Character ch1 = new Character(Common.Position.EastKingdom, "11", sc1);
        characters.Add(ch1);

        Script sc2 = new Script("흐흐.. 당신이 단장이군요?", "(기분나쁜 웃음소리군)", "그렇소만.");
        Character ch2 = new Character(Common.Position.NorthKingdom, "24", sc2);
        characters.Add(ch2);
    }
}
