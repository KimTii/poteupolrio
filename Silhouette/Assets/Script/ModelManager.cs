﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ModelManager : MonoBehaviour {

    ControllerManager controllerManager;
    DB db;
    // Player Infomation
    Player.PlayerInfomation playerInformation;
    // Player Infomation

    void Start () {
        controllerManager = GetComponent<ControllerManager>();
        db = GetComponent<DB>();
        List<string> charactersName = new List<string>();

        charactersName = db.GetCharactersName();
        playerInformation = new Player.PlayerInfomation(charactersName);
    }

    public string GetRandomName() { return playerInformation.GetRandomName(); }

    public void AnswerButton(GameObject _button)
    {
        DB.ScriptEffectOrCondition scriptEffect = new DB.ScriptEffectOrCondition();
        if(string.Equals(_button.name, "Answer1"))
            scriptEffect = controllerManager.GetAnswer1Effect();
        else
            scriptEffect = controllerManager.GetAnswer2Effect();

        playerInformation.SetFriendship(scriptEffect.GetPosition(), scriptEffect.GetNumber(), scriptEffect.GetUp());
    }
    public Dictionary<Common.Position, int> GetFriendships() { return playerInformation.GetFriendships(); }
}
