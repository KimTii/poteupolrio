﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerManager : MonoBehaviour {

    ModelManager modelManager;
    ViewManager viewManager;
    DB db;
    DB.Character currentCharacter;
    DB.Script currentScript;


	void Start () {
        modelManager = GetComponent<ModelManager>();
        viewManager = GetComponent<ViewManager>();
        db = GetComponent<DB>();
        currentCharacter = new DB.Character();
        currentScript = new DB.Script();
    }

    public void InviteGuest()
    {
        currentCharacter = db.GetCurrentCharacter(modelManager.GetRandomName());
        currentScript = currentCharacter.GetRandomScript();
    }
    public string GetName() { return currentCharacter.GetName(); }
    public string GetQuesion() { return currentScript.GetQuesion(); }
    public string GetAnswer1() { return currentScript.GetAnswer1(); }
    public string GetAnswer2() { return currentScript.GetAnswer2(); }
    public DB.ScriptEffectOrCondition GetAnswer1Effect() { return currentScript.GetAnswer1Effect(); }
    public DB.ScriptEffectOrCondition GetAnswer2Effect() { return currentScript.GetAnswer2Effect(); }
    public void AnswerButton(GameObject _button) { modelManager.AnswerButton(_button); }
    public Dictionary<Common.Position, int> GetFriendships() { return modelManager.GetFriendships(); }
}

