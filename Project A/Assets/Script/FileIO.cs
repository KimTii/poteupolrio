﻿using UnityEngine;
using System.Collections;
using System.IO;

public class FileIO : MonoBehaviour {
    const string UnitDB = "UnitDB.csv";
    const string LabDB = "LabDB.csv";
    const string EnemyDB = "EnemyDB.csv";
    const string WorldmapDB = "WorldmapDB.csv";
    const string UserDB = "User.csv";
    const string ProductionDB = "Production.csv";

    delegate void InputDB(string[] partWord);
    DB db;
    User user;

    public void Init()
    {
        db = GetComponent<DB>();
        user = GetComponent<User>();
        user = user.GetUser();

        // UnitDB Load
        Load(UnitDB, InputUnitDB);
        // LabDB Load
        Load(LabDB, InputLabDB);
        // Enemy Load
        Load(EnemyDB, InputEnemyDB);
        //Worldmap Load
        Load(WorldmapDB, InputWorldmapDB);
        //db.PrintLabDB();

        //Save();
        Load(UserDB, InputUserDB);
        Load(ProductionDB, InputProductionDB);
        Save();
    }

    void Load(string filename, InputDB func)
    {
        string path = Application.dataPath + "/Resources/" + filename;
        //Debug.Log(path);
        FileInfo file = new FileInfo(path);
        TextReader reader = null;
        if (file != null && file.Exists)
            reader = file.OpenText();

        if (reader == null)
        {
            Debug.LogError("File not found or not readable : " + path);
            Debug.LogError("LoadFile Fail : " + path);
            return;
        }

        // 첫 줄은 데이터가 아닙니다.
        string line = reader.ReadLine();
        string[] partWord = null;
        while (line != null)
        {
            line = reader.ReadLine();
            //Debug.Log(line);
            try { partWord = line.Split(','); }
            catch { break; }

            if (partWord.Length == 0)
                continue;


            func(partWord);
        }
        reader.Close();
    }

    public void Save()
    {
        UserInfoSave(UserDB);
        ProductionSave(ProductionDB);
    }
    void UserInfoSave(string fileName)
    {
        string path = Application.dataPath + "/Resources/" + fileName;
        FileStream file = new FileStream(path, FileMode.Create);
        StreamWriter writer = new StreamWriter(file, System.Text.Encoding.UTF8);
        if (writer == null)
        {
            Debug.LogError("Save Fail : " + path);
            return;
        }

        // 첫 줄은 데이터가 아닙니다.
        writer.WriteLine("Gold, Gene, 늑대인간, 리자드맨, 마법사, 워리어, 블루우마루, 레드우마루, 그린우마루, 블루우마루 연구, 레드우마루 연구, 그린우마루 연구, Stage");
        writer.Write(user.gold + ",");
        writer.Write(user.gene + ",");
        writer.Write(user.GetUnit("늑대인간") + ",");
        writer.Write(user.GetUnit("리자드맨") + ",");
        writer.Write(user.GetUnit("마법사") + ",");
        writer.Write(user.GetUnit("워리어") + ",");
        writer.Write(user.GetUnit("블루우마루") + ",");
        writer.Write(user.GetUnit("레드우마루") + ",");
        writer.Write(user.GetUnit("그린우마루") + ",");
        writer.Write(user.GetUnit("블루우마루 연구") + ",");
        writer.Write(user.GetUnit("레드우마루 연구") + ",");
        writer.Write(user.GetUnit("그린우마루 연구") + ",");
        writer.Write(user.stage);
        writer.Close();
    }
    void ProductionSave(string fileName)
    {
        string path = Application.dataPath + "/Resources/" + fileName;
        FileStream file = new FileStream(path, FileMode.Create);
        StreamWriter writer = new StreamWriter(file, System.Text.Encoding.UTF8);
        if (writer == null)
        {
            Debug.LogError("Save Fail : " + path);
            return;
        }

        // 첫 줄은 데이터가 아닙니다.
        writer.WriteLine("Name, Start Time, End Time");
        writer.WriteLine("늑대인간," +       user.GetTime("늑대인간").startTime + "," + user.GetTime("늑대인간").endTime);
        writer.WriteLine("리자드맨," +       user.GetTime("리자드맨").startTime + "," + user.GetTime("리자드맨").endTime);
        writer.WriteLine("마법사," +         user.GetTime("마법사").startTime + "," + user.GetTime("마법사").endTime);
        writer.WriteLine("워리어," +         user.GetTime("워리어").startTime + "," + user.GetTime("워리어").endTime);
        writer.WriteLine("블루우마루," +     user.GetTime("블루우마루").startTime + "," + user.GetTime("블루우마루").endTime);
        writer.WriteLine("레드우마루," +     user.GetTime("레드우마루").startTime + "," + user.GetTime("레드우마루").endTime);
        writer.WriteLine("그린우마루," +     user.GetTime("그린우마루").startTime + "," + user.GetTime("그린우마루").endTime);
        writer.WriteLine("블루우마루 연구," + user.GetTime("블루우마루 연구").startTime + "," + user.GetTime("블루우마루 연구").endTime);
        writer.WriteLine("레드우마루 연구," + user.GetTime("레드우마루 연구").startTime + "," + user.GetTime("레드우마루 연구").endTime);
        writer.WriteLine("그린우마루 연구," + user.GetTime("그린우마루 연구").startTime + "," + user.GetTime("그린우마루 연구").endTime);
        writer.Close();
    }

    void InputUnitDB(string[] partWord)
    {
        if (partWord.Length == 8)
        {
            DBSchema.Unit tempUnit = new DBSchema.Unit(
                System.Convert.ToInt32(partWord[0]),    // ID
                partWord[1],                            // Name
                System.Convert.ToInt32(partWord[2]),    // Gold
                System.Convert.ToInt32(partWord[3]),    // Gene
                System.Convert.ToInt32(partWord[4]),    // HP
                System.Convert.ToInt32(partWord[5]),    // Damege
                System.Convert.ToInt32(partWord[6]),    // Seconds
                partWord[7]                             // Illust Name
                );

            db.SetUnitDB(tempUnit);
        }
        else
        {
            
            Debug.LogError("InputUnitDB ERROR >> " + partWord.Length);
            Debug.LogError(partWord[0]);
        }
    }

    void InputLabDB(string[] partWord)
    {
        if (partWord.Length == 7)
        {
            DBSchema.Lab tempUnit = new DBSchema.Lab(
                System.Convert.ToInt32(partWord[0]),    // ID
                partWord[1],                            // Name
                System.Convert.ToInt32(partWord[2]),    // Gold
                System.Convert.ToInt32(partWord[3]),    // Gene
                System.Convert.ToInt32(partWord[4]),    // Seconds
                partWord[5],                            // Illust Name
                partWord[6]                             // Unit Name
                );

            db.SetLabDB(tempUnit);
        }
        else
        {
            Debug.LogError("InputLabDB ERROR >> " + partWord.Length);
            Debug.LogError(partWord[0]);
        }
    }

    void InputEnemyDB(string[] partWord)
    {
        if (partWord.Length == 5)
        {
            DBSchema.Enemy tempUnit = new DBSchema.Enemy(
                System.Convert.ToInt32(partWord[0]),    // ID
                partWord[1],                            // Name
                System.Convert.ToInt32(partWord[2]),    // Hp
                System.Convert.ToInt32(partWord[3]),    // Damege
                partWord[4]                             // Illust Name
                );

            db.SetEnemyDB(tempUnit);
        }
        else
        {
            Debug.LogError("InputLabDB ERROR >> " + partWord.Length);
            Debug.LogError(partWord[0]);
        }
    }

    void InputWorldmapDB(string[] partWord)
    {
        if (partWord.Length == 13)
        {
            DBSchema.Worldmap tempUnit = new DBSchema.Worldmap(
                System.Convert.ToInt32(partWord[0]),    // ID
                partWord[1],                            // Name
                System.Convert.ToInt32(partWord[2])     // Gold
                );

            tempUnit.ClearBattleUnit();
            int nameIndex = 3;
            int countIndex = 4;
            for(int i = 0; i < DBSchema.MaxBattleUnit; ++i)
            {
                try
                {
                    // ( string Name, int Unit_Count )
                    //Debug.Log("[name, count] >> " + nameIndex + ", " + countIndex);
                    tempUnit.AddBattleUnit(new DBSchema.BattleUnit(partWord[nameIndex], System.Convert.ToInt32(partWord[countIndex])));
                    //Debug.Log("[name, count] >> " + partWord[nameIndex] + ", " + partWord[countIndex]);
                    nameIndex += 2;
                    countIndex += 2;
                }
                catch {
                    //Debug.Log("[name, count] >> " + partWord[i + 3] + ", " + partWord[i + 4]);
                    continue; }
            }
            db.SetWorldDB(tempUnit);
        }
        else
        {
            Debug.LogError("InputLabDB ERROR >> " + partWord.Length);
            Debug.LogError(partWord[0]);
        }
    }

    void InputUserDB(string[] partWord)
    {
        if (partWord.Length == 13)
        {
            User tempUser = user.GetUser();
            user.SetGold(System.Convert.ToInt32(partWord[0]));      // Gold
            user.SetGene(System.Convert.ToInt32(partWord[1]));      // Gene
            user.AddUnit("늑대인간", System.Convert.ToInt32(partWord[2]));
            user.AddUnit("리자드맨", System.Convert.ToInt32(partWord[3]));
            user.AddUnit("마법사", System.Convert.ToInt32(partWord[4]));
            user.AddUnit("워리어", System.Convert.ToInt32(partWord[5]));
            user.AddUnit("블루우마루", System.Convert.ToInt32(partWord[6]));
            user.AddUnit("레드우마루", System.Convert.ToInt32(partWord[7]));
            user.AddUnit("그린우마루", System.Convert.ToInt32(partWord[8]));
            user.AddUnit("블루우마루 연구", System.Convert.ToInt32(partWord[9]));
            user.AddUnit("레드우마루 연구", System.Convert.ToInt32(partWord[10]));
            user.AddUnit("그린우마루 연구", System.Convert.ToInt32(partWord[11]));
            user.SetStage(System.Convert.ToInt32(partWord[12]));    // Stage
        }
        else
        {
            Debug.LogError("InputUserDB ERROR >> " + partWord.Length);
            Debug.LogError(partWord[0]);
        }
    }

    void InputProductionDB(string[] partWord)
    {
        if (partWord.Length == 3)
        {
            User tempUser = user.GetUser();
            user.AddTime(partWord[0],                       // Name
                System.Convert.ToDateTime(partWord[1]),     // Start Time
                System.Convert.ToDateTime(partWord[2])      // End Time
                );
        }
        else
        {
            Debug.LogError("InputProductionDB ERROR >> " + partWord.Length);
            Debug.LogError(partWord[0]);
        }
    }
}