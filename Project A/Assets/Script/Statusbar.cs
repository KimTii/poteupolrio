﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statusbar : MonoBehaviour {

    public UILabel lGold;
    public UILabel lGene;
    public UILabel lTime;

    User user;

    public void Init()
    {
        user = GetComponent<User>().GetUser();

        lGold.text = user.gold.ToString();
        lGene.text = user.gene.ToString();
        lTime.text = System.DateTime.Now.ToString("HH:mm");

        StartCoroutine("UpdateTime");
    }
    public void UpdateStatusbar()
    {
        //Debug.Log("스테이터스바 업데이트");
        lGold.text = user.gold.ToString();
        lGene.text = user.gene.ToString();
    }

    IEnumerator UpdateTime()
    {
        while(true)
        {
            lTime.text = System.DateTime.Now.ToString("HH:mm");
            yield return new WaitForSecondsRealtime(60);
        }
    }
}
