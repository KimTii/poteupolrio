﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle : MonoBehaviour {
    // 전투에 참여할 유닛의 리스트
    public UISprite[] enemys;
    UILabel[] enemysLabel;
    public UISprite[] players;
    UILabel[] playersLabel;
    //
    // 플레이어가 선택하는 유닛들 리스트
    public UISprite[] playerList;
    UILabel[] playerListLabel;
    //
    // 초기화를 위한 유닛의 이름들
    ArrayList unitNames;
    public GameObject clearButton;
    public GameObject startButton;
    //
    // Log
    public List<UILabel> lLog;
    //
    //
    DB db;
    User user;
    TrainingSchool training;
    Button button;
    Worldmap worldmap;
    FileIO file;
    //
    int stageNumber;
    int selectIndex;
    //
    

    class Unit
    {
        public string name { get; private set; }
        public int damege { get; private set; }
        public int hp { get; private set; }
        public int count { get; private set; }
        public UISprite sSprite { get; private set; }
        public UILabel lCount { get; private set; }
        public string script;
        int lostDamege;

        public Unit() { name = null; count = 0; lostDamege = 0; }
        public Unit(UISprite _sprite, UILabel _label)
        {
            name = null;
            count = 0;
            sSprite = _sprite;
            lCount = _label;
            lostDamege = 0;
        }
        public Unit(string _name, int _damege, int _hp, int _count, UISprite _sprite, UILabel _lCount)
        {
            name = _name;
            damege = _damege;
            hp = _hp;
            count = _count;
            sSprite = _sprite;
            lCount = _lCount;
            lostDamege = 0;
        }
        public void SetUIComponent(UISprite _sprite, UILabel _label) { sSprite = _sprite;lCount = _label; }
        public void SetUnit(string _name, int _damege, int _hp, int _count) {
            name = _name;
            damege = _damege;
            hp = _hp;
            SetCount(_count);
        }
        public void SetSprite(string _spriteName)
        {
            sSprite.spriteName = _spriteName;
            sSprite.GetComponent<UIButton>().normalSprite = _spriteName;
        }
        public void SetCount(int _count)
        {
            count = _count;
            if (count != 0)
                lCount.text = count.ToString();
            else
                lCount.text = "";
        }
        public void SetClear()
        {
            name = null;
            SetCount(0);
            SetSprite("Attack");
        }
        public void SetCollider(bool _coll) { sSprite.GetComponent<BoxCollider>().enabled = _coll; }
        public void SetName(string _name) { name = _name; }
        //
        public void Attack(Unit target, bool isPlayer)
        {
            int minusUnit = (damege * count + lostDamege) / (target.hp);
            if (minusUnit == 0)
                lostDamege += (damege * count + lostDamege);
            else
                lostDamege = (damege * count + lostDamege) % (target.hp);
            int _count = target.count - minusUnit;

            if (isPlayer)
                script = "[0d5302]" + name + "[-]-> [b0120a]" + target.name + "[-]  " + damege * count + "의 피해, " + minusUnit + "만큼 죽임.";
            else
                script = "[b0120a]" + name + "[-]-> [0d5302]" + target.name + "[-]  " + damege * count + "의 피해, " + minusUnit + "만큼 죽임.";

            if (_count < 1)
            {
                _count = 0;
                target.SetName(null);
                Debug.Log(target.name);
            }
            target.SetCount(_count);
        }
    }
    ArrayList playerUnit;
    ArrayList enemyUnit;

    public void Init()
    {
        db = GetComponent<DB>();
        user = GetComponent<User>().GetUser();
        training = GetComponent<TrainingSchool>();
        button = GetComponent<Button>();
        worldmap = GetComponent<Worldmap>();
        file = GetComponent<FileIO>();

        enemysLabel = new UILabel[enemys.Length];
        playersLabel = new UILabel[players.Length];
        playerListLabel = new UILabel[playerList.Length];
        unitNames = new ArrayList();
        playerUnit = new ArrayList(5);
        enemyUnit = new ArrayList(5);

        for (int i = 0; i < enemysLabel.Length; ++i)
        {
            enemysLabel[i] = enemys[i].transform.FindChild("Label").GetComponent<UILabel>();
            playersLabel[i] = players[i].transform.FindChild("Label").GetComponent<UILabel>();
            playerUnit.Add(new Unit(players[i], playersLabel[i]));
        }
        for(int i = 0; i < playerList.Length; ++i)
        {
            playerListLabel[i] = playerList[i].transform.FindChild("Label").GetComponent<UILabel>();
        }
        foreach (KeyValuePair<string, DBSchema.Unit> member in db.GetUnitDB())
        {
            unitNames.Add(member.Key);
        }

        foreach (UILabel member in lLog) { member.gameObject.SetActive(false); }
    }
    public void PlayerLIstUpdate()
    {
        for(int i = 0; i < playerList.Length; ++i)
        {
            playerList[i].spriteName = training.unit[i].spriteName;
            playerList[i].GetComponent<UIButton>().normalSprite = training.unit[i].spriteName;
            playerListLabel[i].text = user.GetUnit(unitNames[i].ToString()).ToString();
        }
    }
    public void ClearEnemyUnit() { enemyUnit.Clear(); }
    public void SetEnemy(int index, DBSchema.Enemy enemy, string unitCount, int _stageNumber)
    {
        enemys[index].spriteName = enemy.illustName;
        enemysLabel[index].text = unitCount;
        stageNumber = _stageNumber;
        //
        enemyUnit.Add(new Unit(enemy.name, enemy.damege, enemy.hp, System.Convert.ToInt32(unitCount), enemys[index], enemysLabel[index]));
        //Debug.Log("적이 추가 됨 : " + enemy.name);
        //Debug.Log("적의 수는 " + enemyUnit.Count + "입니다.");
        //int i = 0;
        //foreach (Unit member in enemyUnit) { Debug.Log("[" + (i++) + "]" + member.name); }
        //
    }
    public void SetEnemyEnabled(int index, bool _enabled)
    {
        enemys[index].enabled = _enabled;
        enemysLabel[index].enabled = _enabled;
    }
    //
    public void SetPlayerIndex(int _index) {
        selectIndex = _index;
        for (int i = 0; i < players.Length; ++i)
            players[i].GetComponent<UIButton>().defaultColor = Color.white;
        players[_index].GetComponent<UIButton>().defaultColor = Color.green;
    }
    public void SetPlayer(int index)
    {
        // 선택한 유닛의 이름
        string unitName = training.unit[index].transform.FindChild("Name").GetComponent<UILabel>().text;
        // 선택한 유닛의 마리수
        int unitCount = user.GetUnit(unitName);
        // 선택한 칸의 정보
        Unit pu = (Unit)playerUnit[selectIndex];

        if (unitName != pu.name)
        {
            pu.SetSprite("Attack");
            if(unitCount < 1)
                pu.SetUnit(null, 0, 0, 0);
            else
                pu.SetUnit(unitName, db.GetUnit(unitName).damege, db.GetUnit(unitName).hp, 0);
        }
            
        if(pu.count < unitCount && PlayerTotlaCount(pu.name) < unitCount)
        {
            //Debug.Log("총 : " + PlayerTotlaCount(pu.GetName()));
            pu.SetSprite(training.unit[index].spriteName);
            pu.SetUnit(unitName, db.GetUnit(unitName).damege, db.GetUnit(unitName).hp, pu.count + 1);
            
        }
    }
    int PlayerTotlaCount(string _name)
    {
        int result = 0;
        foreach (Unit member in playerUnit)
        {
            if (member.name == _name)
                result += member.count;
        }
        return result;
    }

    public void Clear()
    {
        foreach(Unit member in playerUnit)
        {
            member.SetClear();
        }
    }
    public void BattleStart()
    {
        SetBattle(false);
        int unitCount = 0;
        foreach(Unit member in playerUnit)
        {
            try
            {
                unitCount = user.GetUnit(member.name) - member.count;
                //Debug.Log("UnitCount = " + user.GetUnit(member.name) + " - " + member.count);
                if (unitCount < 0)
                    Debug.LogError("Battle Start ERROR!!");

                user.AddUnit(member.name, unitCount);
                //Debug.Log("UnitCount : " + unitCount);
                //Debug.Log("User : " + user.GetUnit(member.name));
            }
            catch { continue; }
        }
        for (int i = 0; i < players.Length; ++i)
            players[i].GetComponent<UIButton>().defaultColor = Color.white;

        StartCoroutine("BattleCoroutine");
        //
    }
    void SetBattle(bool ui)
    {
        foreach (UISprite member in playerList) { member.gameObject.SetActive(ui); }
        startButton.SetActive(ui);
        clearButton.SetActive(ui);
        foreach (Unit member in playerUnit) { member.SetCollider(ui); }

        foreach(UILabel member in lLog) { member.gameObject.SetActive(!ui); }
    }
    IEnumerator BattleCoroutine()
    {
        int playerIndex = 0;
        int enemyIndex = 0;
        //int index = 0;
        //foreach (Unit member in playerUnit) { Debug.Log("[" + (index++) + "]" + member.name); }
        //index = 0;
        //foreach (Unit member in enemyUnit) { Debug.Log("[" + (index++) + "]" + member.name); }
        while (true)
        {
            Debug.Log("코루틴중");
            playerIndex = (int)Random.Range(0, playerUnit.Count);
            enemyIndex = (int)Random.Range(0, enemyUnit.Count);
            Unit player = playerUnit[playerIndex] as Unit;
            Unit enemy = enemyUnit[enemyIndex] as Unit;
            while (player.count == 0 || enemy.count == 0)
            {
                if(player.count == 0)
                {
                    playerIndex = (int)Random.Range(0, playerUnit.Count);
                    player = playerUnit[playerIndex] as Unit;
                }
                if (enemy.count == 0)
                {
                    enemyIndex = (int)Random.Range(0, enemyUnit.Count);
                    enemy = enemyUnit[enemyIndex] as Unit;
                }
            }
            Attack(player, enemy, true);
            Attack(enemy, player, false);
            
            if(IsAllDie(playerUnit))
            {
                // 전투 패배
                yield return new WaitForSecondsRealtime(1);
                BattleOver();
            }
            if(IsAllDie(enemyUnit))
            {
                // 전투 승리
                BattleResult();
                yield return new WaitForSecondsRealtime(1);
                BattleOver();
            }
            
            yield return new WaitForSecondsRealtime(1);
        }
    }
    void Attack(Unit attacker, Unit target, bool isPlayer)
    {
        Debug.Log("어택중");
        attacker.Attack(target, isPlayer);
        LogAdd(attacker.script);
    }
    bool IsAllDie(ArrayList target)
    {
        Debug.Log("다 죽었는지 확인중");
        bool live = false;
        for (int i = 0; i < target.Count; ++i)
        {
            try
            {
                Unit t = target[i] as Unit;
                if (t.count <= 0)
                    live = false;
                else
                    live = true;

                if (live)
                    break;
            }
            catch { continue; }
        }
        if (live == false)
            return true;
        else
            return false;
    }
    
    void BattleOver()
    {
        Debug.Log("전투 종료 중");
        SetBattle(true);
        LogClear();
        button.GoWorldmap();
        // 현재 몬스터 정보를 유저 데이터에 반영해야함
        int unitCount = 0;
        foreach (Unit member in playerUnit)
        {
            try
            {
                unitCount = user.GetUnit(member.name) + member.count;
                user.AddUnit(member.name, unitCount);
            }
            catch { continue; }
        }
        Debug.Log("배틀 종료");
        StopCoroutine("BattleCoroutine");
        file.Save();
    }
    void BattleResult()
    {
        Debug.Log("전투 승리 반영 중");

        Debug.Log("Gold : " + user.gold);
        Debug.Log("Gene : " + user.gene);
        int gold = worldmap.GetStageGold();
        user.AddGold(gold);
        user.AddGene(Random.Range(1, gold));
        Debug.Log("Gold : " + user.gold);
        Debug.Log("Gene : " + user.gene);
    }

    void LogAdd(string script)
    {
        for(int i = lLog.Count - 1; i > 0; --i)
        {
            lLog[i].text = lLog[i - 1].text;
        }
        lLog[0].text = script;

        //int index = 0;
        //foreach(UILabel member in lLog) { Debug.Log("[" + index++ + "]" + member.text); }
    }
    void LogClear()
    {
        foreach (UILabel member in lLog) { member.text = ""; }
    }
}

