﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

    //DB db;
    //MyTime time;
    TrainingSchool ts;
    Worldmap world;
    Battle battle;

    void Start()
    {
        // Scene Button Start
        panels = new UIPanel[] { pCamp, pUnit, pLab, pWorldmap, pBattle };

        // Training School Button Start
        ts = GetComponent<TrainingSchool>();

        // Worldmap School Button Start
        world = GetComponent<Worldmap>();
        battle = GetComponent<Battle>();
    }

    #region Scene Button
    static UIPanel[] panels;
    public UIPanel pCamp;
    public UIPanel pUnit;
    public UIPanel pLab;
    public UIPanel pWorldmap;
    public UIPanel pBattle;

    public void GoButton(UIPanel panel)
    {
        AllMvePanels();
        panel.transform.position = Vector3.zero;
        StartCoroutine("GoCoroutine", panel);
    }

    IEnumerator GoCoroutine(UIPanel _in)
    {
        _in.alpha = 0;
        float num = 0.1f;

        while(true)
        {
            if (_in.alpha >= 1)
                break;

            _in.alpha += num;
            yield return null;
        }
    }
    void AllMvePanels()
    {
        for (int i = 0; i < panels.Length; ++i)
            panels[i].transform.position = new Vector3(1200, 0, 0);
    }
    #endregion

    #region Training School Button
    public void MakeUnit(string name)
    {
        ts.MakeUnit(name);
    }
    public void StartResearch(GameObject obj)
    {
        string name = obj.transform.FindChild("Name").GetComponent<UILabel>().text;
        obj.transform.FindChild("Button").GetComponent<UIButton>().isEnabled = false;
        ts.StartResearch(name);
        
    }
    #endregion

    #region Worldmap
    public void StageSelect(GameObject stage) { world.StageSelect(stage); }
    public void PopupClear() { world.PopupReset(); }

    #region Battle
    //public void SetPlayerIndex(int _index) { battle.SetPlayerIndex(_index); }
    public void SetPlayerIndex0() { battle.SetPlayerIndex(0); }
    public void SetPlayerIndex1() { battle.SetPlayerIndex(1); }
    public void SetPlayerIndex2() { battle.SetPlayerIndex(2); }
    public void SetPlayerIndex3() { battle.SetPlayerIndex(3); }
    public void SetPlayerIndex4() { battle.SetPlayerIndex(4); }
    public void SetPlayer(GameObject obj) { battle.SetPlayer(System.Convert.ToInt32(obj.name)); }
    public void SetClear() { battle.Clear(); }
    public void BattleStart() { battle.BattleStart(); }
    
    public void GoWorldmap()
    {
        world.PopupReset();
        GoButton(pWorldmap);
    }
    #endregion
    #endregion
}
