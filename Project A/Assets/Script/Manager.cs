﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    FileIO file;

    void Awake()
    {
        file = GetComponent<FileIO>();
        file.Init();
    }

    TrainingSchool training;
    MyTime myTime;
    Worldmap world;
    Statusbar statusbar;
    Battle battle;

    void Start()
    {
        training = GetComponent<TrainingSchool>();
        training.Init();

        myTime = GetComponent<MyTime>();
        myTime.Init();

        world = GetComponent<Worldmap>();
        world.Init();

        statusbar = GetComponent<Statusbar>();
        statusbar.Init();

        battle = GetComponent<Battle>();
        battle.Init();

        StartCoroutine("ManagerUpdate");
    }

    IEnumerator ManagerUpdate()
    {
        while(true)
        {
            // Update
            statusbar.UpdateStatusbar();
            training.UpdateCount();
            yield return new WaitForSecondsRealtime(1);
        }
    }
}
