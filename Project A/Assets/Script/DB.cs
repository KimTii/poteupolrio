﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DB : MonoBehaviour
{
    #region UnitDB
    private static Dictionary<string, DBSchema.Unit> unitDB = new Dictionary<string, DBSchema.Unit>();

    public void SetUnitDB(DBSchema.Unit unit) { unitDB[unit.name] = unit; }

    public Dictionary<string, DBSchema.Unit> GetUnitDB() { return unitDB; }
    public DBSchema.Unit GetUnit(string name) { return unitDB[name]; }
    public void PrintUnitDB()
    {
        foreach (KeyValuePair<string, DBSchema.Unit> member in unitDB)
        {
            Debug.Log(member.Key + " / " + member.Key.Length);
        }
    }
    #endregion

    #region LabDB
    private static Dictionary<string, DBSchema.Lab> labDB = new Dictionary<string, DBSchema.Lab>();

    public void SetLabDB(DBSchema.Lab lab) { labDB[lab.name] = lab; }

    public Dictionary<string, DBSchema.Lab> GetLabDB() { return labDB; }
    public DBSchema.Lab GetLab(string name) { return labDB[name]; }

    public void PrintLabDB()
    {
        foreach (KeyValuePair<string, DBSchema.Lab> member in labDB)
        {
            Debug.Log(member.Key + " / " + member.Key.Length);
        }
    }
    #endregion

    #region EnemyDB
    private static Dictionary<string, DBSchema.Enemy> enemyDB = new Dictionary<string, DBSchema.Enemy>();

    public void SetEnemyDB(DBSchema.Enemy enemy) { enemyDB[enemy.name] = enemy; }

    public Dictionary<string, DBSchema.Enemy> GetEnemyDB() { return enemyDB; }
    public DBSchema.Enemy GetEnemy(string name) { return enemyDB[name]; }

    public void PrintEnemyDB()
    {
        foreach (KeyValuePair<string, DBSchema.Enemy> member in enemyDB)
        {
            Debug.Log(member.Key + " / " + member.Key.Length);
        }
    }
    #endregion

    #region WorldmapDB
    private static Dictionary<int, DBSchema.Worldmap> worldmapDB = new Dictionary<int, DBSchema.Worldmap>();

    public void SetWorldDB(DBSchema.Worldmap worldmap) { worldmapDB[worldmap.id] = worldmap; }

    public Dictionary<int, DBSchema.Worldmap> GetWorldmapDB() { return worldmapDB; }
    public DBSchema.Worldmap GetWorldmap(int id) { return worldmapDB[id]; }

    public void PrintWorldDB()
    {
        foreach (KeyValuePair<int, DBSchema.Worldmap> member in worldmapDB)
        {
            Debug.Log(member.Key + " / " + member.Value.name);
        }
    }
    #endregion

}
