﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingSchool : MonoBehaviour {
    enum DBType
    {
        UNIT,
        LAB
    }

    public UISprite[] unit;
    public UISprite[] lab;

    UILabel lName;
    UILabel lGene;
    UILabel lGold;
    UILabel lDamege;
    UILabel lHp;
    UILabel lSeconds;

    UILabel lWaitTime;
    UILabel lWaitCount;
    UILabel lCount;
    UIButton makeButton;

    DB db;
    MyTime myTime;
    User user;
    Battle battle;
    FileIO file;

    public void Init()
    {
        // Start 대용
        myTime = GetComponent<MyTime>();
        db = GetComponent<DB>();
        user = GetComponent<User>().GetUser();
        battle = GetComponent<Battle>();
        file = GetComponent<FileIO>();
        //

        Initialize(unit, DBType.UNIT);
        Initialize(lab, DBType.LAB);
    }

    void Initialize(UISprite[] target, DBType type)
    {
        DBSchema.Unit dbUnit = new DBSchema.Unit();
        ArrayList unitNames = new ArrayList();

        DBSchema.Lab dbLab = new DBSchema.Lab();
        ArrayList labNames = new ArrayList();

        switch (type) {
            case DBType.UNIT:
                foreach (KeyValuePair<string, DBSchema.Unit> member in db.GetUnitDB()) { unitNames.Add(member.Key); }
                break;

            case DBType.LAB:
                foreach (KeyValuePair<string, DBSchema.Lab> member in db.GetLabDB()) { labNames.Add(member.Key); }
                break;

            default:
                break;
        }

        
        for (int i = 0; i < target.Length; ++i)
        {
            Transform targetTransform = target[i].transform;
            lName =     targetTransform.FindChild("Name").GetComponent<UILabel>();
            lGene =     targetTransform.FindChild("Gene").transform.FindChild("Label").GetComponent<UILabel>();
            lGold =     targetTransform.FindChild("Gold").transform.FindChild("Label").GetComponent<UILabel>();
            lDamege =   targetTransform.FindChild("Damege").transform.FindChild("Label").GetComponent<UILabel>();
            lHp =       targetTransform.FindChild("Hp").transform.FindChild("Label").GetComponent<UILabel>();
            lSeconds =  targetTransform.FindChild("Seconds").transform.FindChild("Label").GetComponent<UILabel>();
            switch (type) {
                case DBType.UNIT:
                    lWaitTime = targetTransform.FindChild("Button").transform.FindChild("WaitTime").GetComponent<UILabel>();
                    lWaitCount = targetTransform.FindChild("Button").transform.FindChild("WaitCount").GetComponent<UILabel>();
                    lWaitCount.text = "0";
                    //lCount = targetTransform.FindChild("Count").GetComponent<UILabel>();

                    dbUnit = db.GetUnit(unitNames[i].ToString());
                    target[i].spriteName = dbUnit.illustName;
                    lName.text = dbUnit.name;
                    lGene.text = dbUnit.gene.ToString();
                    lGold.text = dbUnit.gold.ToString();
                    lDamege.text = dbUnit.damege.ToString();
                    lHp.text = dbUnit.hp.ToString();
                    lSeconds.text = dbUnit.seconds.ToString();
                    break;

                case DBType.LAB:
                    dbLab = db.GetLab(labNames[i].ToString());
                    target[i].spriteName = dbLab.illustName;
                    lName.text = dbLab.name;
                    lGene.text = dbLab.gene.ToString();
                    lGold.text = dbLab.gold.ToString();
                    //
                    DBSchema.Unit labUnit = db.GetUnit(dbLab.unitName);
                    lDamege.text = labUnit.damege.ToString();
                    lHp.text = labUnit.hp.ToString();
                    //
                    lSeconds.text = dbLab.seconds.ToString();
                    break;

                default:
                    break;
            }
        }
    }

    UISprite FindSprite(UISprite[] target, string _name)
    {
        string unitName;
        for(int i = 0; i < target.Length; ++i)
        {
            unitName = target[i].transform.FindChild("Name").GetComponent<UILabel>().text;
            //Debug.Log(unit[i].spriteName);
            //Debug.Log(i + " >> " + unitName + " / " + _name);
            if (_name == unitName)
                return target[i];
        }
        return null;
    }

    public void MakeUnit(string name)
    {
        UISprite target = FindSprite(unit, name);
        UILabel waitTime = target.transform.FindChild("Button").transform.FindChild("WaitTime").GetComponent<UILabel>();
        UILabel waitCount = target.transform.FindChild("Button").transform.FindChild("WaitCount").GetComponent<UILabel>();

        // DB 가져오기
        DBSchema.Unit dbUnit = db.GetUnit(name);
        //
        if(user.gold >= dbUnit.gold && user.gene >= dbUnit.gene)
        {
            user.AddGold(-dbUnit.gold);
            user.AddGene(-dbUnit.gene);
            // 시간 더하기 & 라벨에 적용 (생산을 시작하라)
            myTime.StartTimer(name, waitTime, dbUnit.seconds, waitCount);
            file.Save();
        }
    }
    public void StartResearch(string name)
    {
        UISprite target = FindSprite(lab, name);
        UILabel waitTime = target.transform.FindChild("Button").transform.FindChild("WaitTime").GetComponent<UILabel>();

        DBSchema.Lab dbLab = db.GetLab(name);

        myTime.StartTimer(name, waitTime, dbLab.seconds);
    }

    public void UpdateCount()
    {
        foreach (UISprite member in unit)
        {
            UILabel count = member.transform.FindChild("Count").GetComponent<UILabel>();
            string name = member.transform.FindChild("Name").GetComponent<UILabel>().text;
            count.text = user.GetUnit(name).ToString();
            battle.PlayerLIstUpdate();
        }

        foreach (UISprite member in lab)
        {
            UIButton button = member.transform.FindChild("Button").GetComponent<UIButton>();
            UILabel count = member.transform.FindChild("Count").GetComponent<UILabel>();
            string name = member.transform.FindChild("Name").GetComponent<UILabel>().text;
            int getCount = user.GetUnit(name);
            // 블루우마루, 레드우마루 :: 이렇게 이름이 5글자 공통이기에 가능한 코드
            name = name.Substring(0, 5);
            if (getCount == 0)
            {
                count.text = "연구필요";
                button.isEnabled = true;
                MakeButtonUpdate(name, false);
            }
            else
            {
                count.text = "연구완료";
                button.isEnabled = false;
                MakeButtonUpdate(name, true);
            }
            
        }
    }
    void MakeButtonUpdate(string _name, bool _complete)
    {
        UIButton makeButton;
        
        foreach (UISprite member in unit)
        {
            string name = member.transform.FindChild("Name").GetComponent<UILabel>().text;
            //Debug.Log(name + " / " + _name);
            if(name == _name)
            {
                makeButton = member.transform.FindChild("Button").GetComponent<UIButton>();
                makeButton.isEnabled = _complete;
            }
        }
    }
}
