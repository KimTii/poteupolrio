﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBSchema : MonoBehaviour {

    public const int MaxBattleUnit = 5;

    public class StandardSchema
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public int gold { get; private set; }
        public int gene { get; private set; }
        public int seconds { get; private set; }
        public string illustName { get; private set; }

        public StandardSchema() { }
        public StandardSchema(int _id, string _name, int _gold, int _gene, int _seconds, string _illustName)
        {
            id = _id;
            name = _name;
            gold = _gold;
            gene = _gene;
            seconds = _seconds;
            illustName = _illustName;
        }
    }

    public class Unit : StandardSchema
    {
        public int hp { get; private set; }
        public int damege { get; private set; }

        public Unit() { }
        public Unit(int _id, string _name, int _gold, int _gene, int _hp, int _damege, int _seconds, string _illustName)
            : base(_id, _name, _gold, _gene, _seconds, _illustName)
        {
            hp = _hp;
            damege = _damege;
        }
    }

    public class Lab : StandardSchema
    {
        public string unitName { get; private set; }

        public Lab() { }
        public Lab(int _id, string _name, int _gold, int _gene, int _seconds, string _illustName, string _unitName)
            : base(_id, _name, _gold, _gene, _seconds, _illustName)
        {
            unitName = _unitName;
        }
    }

    public class Enemy
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public int hp { get; private set; }
        public int damege { get; private set; }
        public string illustName { get; private set; }

        public Enemy() { }
        public Enemy(int _id, string _name, int _hp, int _damege, string _illustName)
        {
            id = _id;
            name = _name;
            hp = _hp;
            damege = _damege;
            illustName = _illustName;
        }
    }

    public class Worldmap
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public int gold { get; private set; }
        public BattleUnit[] battleUnit { get; private set; }
        public int battleUnitIndex { get; private set; }

        public Worldmap()
        {
            battleUnit = new BattleUnit[MaxBattleUnit];
            battleUnitIndex = 0;
        }
        public Worldmap(int _id, string _name, int _gold)
        {
            id = _id;
            name = _name;
            gold = _gold;
            battleUnit = new BattleUnit[MaxBattleUnit];
            battleUnitIndex = 0;
        }

        public void AddBattleUnit(BattleUnit unit)
        {
            if (battleUnitIndex >= MaxBattleUnit)
                return;

            battleUnit[battleUnitIndex] = unit;
            ++battleUnitIndex;
        }
        public void ClearBattleUnit()
        {
            battleUnit = new BattleUnit[MaxBattleUnit];
            battleUnitIndex = 0;
        }
        public string GetUnitName(int number)
        {
            if (number > MaxBattleUnit)
                return null;
            try { return battleUnit[number].name; }
            catch { throw; }
        }
        public int GetUnitCount(int number)
        {
            if (number > MaxBattleUnit)
                return 0;
            return battleUnit[number].count;
        }
    }
    public class BattleUnit
    {
        public string name { get; private set; }
        public int count { get; private set; }

        public BattleUnit() { }
        public BattleUnit(string _name, int _count)
        {
            name = _name;
            count = _count;
        }
    }

}
