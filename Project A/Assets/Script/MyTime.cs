﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTime : MonoBehaviour {

    class Timer
    {
        public DateTime startTime { get; protected set; }
        public DateTime endTime { get; protected set; }
        public UILabel lTimer { get; protected set; }
        public int seconds { get; protected set; }
        public bool isMake { get; protected set; }

        public Timer() { }
        public Timer(DateTime _startTime, DateTime _endTime, UILabel _lTimer, int _seconds)
        {
            startTime = _startTime;
            endTime = _endTime;
            lTimer = _lTimer;
            seconds = _seconds;
            isMake = false;
        }
        
        public void SetStartTime(DateTime time) { startTime = time; }
        public void SetEndTime(int seconds) { endTime = endTime.AddSeconds(seconds); }
        public void SetIsMake(bool _isMake) { isMake = _isMake; }
    }

    class TimerUnit : Timer
    {
        public UILabel lCount { get; protected set; }
     
        public TimerUnit() { }
        public TimerUnit(DateTime _startTime, DateTime _endTime, UILabel _lTimer, UILabel _lCount, int _seconds)
            : base(_startTime, _endTime, _lTimer, _seconds)
        { lCount = _lCount; }
        public int GetCount()
        {
            int temp = System.Convert.ToInt32(lCount.text);
            return temp;
        }

        public void SetCount(int number) { lCount.text = number.ToString(); }
    }
    class TimerLab : Timer
    {
        public TimerLab() { }
        public TimerLab(DateTime _startTime, DateTime _endTime, UILabel _lTimer, int _seconds)
            : base(_startTime, _endTime, _lTimer, _seconds) { }
    }

    Dictionary<string, TimerUnit> timerUnit;
    Dictionary<string, TimerLab> timerLab;
    TrainingSchool training;
    FileIO file;

    public void Init()
    {
        timerUnit = new Dictionary<string, TimerUnit>();
        timerLab = new Dictionary<string, TimerLab>();
        training = GetComponent<TrainingSchool>();
        file = GetComponent<FileIO>();
    }
    
    public void StartTimer(string name, UILabel targetTimer, int seconds, UILabel targetCount = null)
    {
        if(targetCount == null) // Lab
        {
            if (timerLab.ContainsKey(name))
                return;
            else
                timerLab.Add(name, new TimerLab(DateTime.Now, DateTime.Now.AddSeconds(seconds), targetTimer, seconds));
        }
        else    // Unit
        {
            if (timerUnit.ContainsKey(name))
            {
                TimerUnit temp = timerUnit[name];
                temp.SetEndTime(seconds);
                timerUnit[name] = temp;
            }
            else
            {
                timerUnit.Add(name, new TimerUnit(DateTime.Now, DateTime.Now.AddSeconds(seconds), targetTimer, targetCount, seconds));
            }

            int count = timerUnit[name].GetCount();
            timerUnit[name].SetCount(++count);
        }

        StopCoroutine("TimerCoroutine");
        StartCoroutine("TimerCoroutine");
    }

    IEnumerator TimerCoroutine()
    {
        while(true)
        {
            if (timerUnit.Count == 0 && timerLab.Count == 0)
                break;

            TimerUnitLoop();
            TimerLabLoop();

            yield return new WaitForSeconds(0.3f);
        }
    }
    void TimerUnitLoop()
    {
        if (timerUnit.Count == 0)
        {
            timerUnit.Clear();
            return;
        }
            

        TimeSpan waitTime;
        string time = null;
        int count = 0;
        List<string> removeUnit = new List<string>();
        User user = GetComponent<User>().GetUser();

        foreach (KeyValuePair<string, TimerUnit> member in timerUnit)
        {
            waitTime = (member.Value.endTime - DateTime.Now);
            //Debug.Log(waitTime.TotalSeconds + " / " + seconds + " = " + (int)(waitTime.TotalSeconds % seconds));
            if (DateTime.Now - member.Value.startTime > TimeSpan.FromSeconds(3))
                member.Value.SetIsMake(true);
            else
                member.Value.SetIsMake(false);

            //Debug.Log("[" + member.Key + "]" + waitTime.TotalSeconds + " / " + member.Value.seconds);
            //Debug.Log(member.Key + " >> " + (int)(waitTime.TotalSeconds % seconds) + " / " + (DateTime.Now - member.Value.startTime) + "(" + member.Value.isMake + ")");
            if ((int)(waitTime.TotalSeconds % member.Value.seconds) == 0 && member.Value.isMake)
            {
                Debug.Log(member.Key + " >> 생산됨!!");
                //
                int unitCount = user.GetUnit(member.Key);
                user.AddUnit(member.Key, ++unitCount);
                training.UpdateCount();
                file.Save();
                //
                member.Value.SetStartTime(DateTime.Now);

                count = member.Value.GetCount();
                --count;
                member.Value.SetCount(count);
            }

            if (DateTime.Now >= member.Value.endTime)
            {
                removeUnit.Add(member.Key);
            }
            else
            {
                time = waitTime.Hours.ToString("D2") + ":" + waitTime.Minutes.ToString("D2") + ":" + waitTime.Seconds.ToString("D2");
                member.Value.lTimer.text = time;
            }
        }

        for (int i = 0; i < removeUnit.Count; ++i) { timerUnit.Remove(removeUnit[i]); }
        removeUnit.Clear();
    }
    void TimerLabLoop()
    {
        if (timerLab.Count == 0)
        {
            timerLab.Clear();
            return;
        }

        TimeSpan waitTime;
        string time = null;
        List<string> removeUnit = new List<string>();
        User user = GetComponent<User>().GetUser();

        foreach (KeyValuePair<string, TimerLab> member in timerLab)
        {
            waitTime = (member.Value.endTime - DateTime.Now);
            if (DateTime.Now - member.Value.startTime > TimeSpan.FromSeconds(3))
                member.Value.SetIsMake(true);
            else
                member.Value.SetIsMake(false);
            if (((int)(waitTime.TotalSeconds % member.Value.seconds) == 0 && member.Value.isMake) || DateTime.Now >= member.Value.endTime)
            {
                Debug.Log(member.Key + " >> 연구완료!!");
                //
                int unitCount = user.GetUnit(member.Key);
                user.AddUnit(member.Key, ++unitCount);
                training.UpdateCount();
                file.Save();
                //
                removeUnit.Add(member.Key);
                member.Value.lTimer.text = "00:00:00";
            }
            else
            {
                time = waitTime.Hours.ToString("D2") + ":" + waitTime.Minutes.ToString("D2") + ":" + waitTime.Seconds.ToString("D2");
                member.Value.lTimer.text = time;
            }
        }

        for (int i = 0; i < removeUnit.Count; ++i) { timerLab.Remove(removeUnit[i]); }
        removeUnit.Clear();
    }
}