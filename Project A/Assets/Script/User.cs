﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User : MonoBehaviour {

    public class SaveTime
    {
        public DateTime startTime { get; private set; }
        public DateTime endTime { get; private set; }

        public SaveTime()
        {
            startTime = DateTime.Now;
            endTime = DateTime.Now;
        }
        public SaveTime(DateTime _startTime, DateTime _endTime)
        {
            startTime = _startTime;
            endTime = _endTime;
        }
    }

    public int gold { get; private set; }
    public int gene { get; private set; }
    public Dictionary<string, int> unit { get; private set; }
    public int stage { get; private set; }
    public Dictionary<string, SaveTime> time { get; private set; }
    //
    Statusbar statusbar;
    TrainingSchool training;

    public User()
    {
        gold = 0;
        gene = 0;
        unit = new Dictionary<string, int>();
        stage = 0;
        time = new Dictionary<string, SaveTime>();
    }

    public void Init()
    {
        statusbar = GetComponent<Statusbar>();
        training = GetComponent<TrainingSchool>();
    }

    public void SetGold(int _gold)
    {
        gold = _gold;
        // Update
        //try { statusbar.UpdateStatusbar(); }
        //catch { }
    }
    public void AddGold(int _gold)
    {
        gold += _gold;
        // Update
        //statusbar.UpdateStatusbar();
    }
    public void SetGene(int _gene)
    {
        gene = _gene;
        // Update
        //try { statusbar.UpdateStatusbar(); }
        //catch { }
    }
    public void AddGene(int _gene)
    {
        gene += _gene;
        // Update
        //statusbar.UpdateStatusbar();
    }
    public void SetStage(int _stage) { stage = _stage; }
    public void AddUnit(string _unitName, int _value)
    {
        try { unit.Add(_unitName, _value); }
        catch { unit[_unitName] = _value; }
        // Update
        try { training.UpdateCount(); }
        catch { }
    }
    public void AddTime(string _timeName, DateTime _startTime, DateTime _endTime)
    {
        try { time.Add(_timeName, new SaveTime(_startTime, _endTime)); }
        catch { time[_timeName] = new SaveTime(_startTime, _endTime); }
        // Update
        try { training.UpdateCount(); }
        catch { }
    }

    public int GetUnit(string name)
    {
        try { return unit[name]; }
        catch {
            unit.Add(name, 0);
            return unit[name];
        }
    }
    public SaveTime GetTime(string name)
    {
        try { return time[name]; }
        catch {
            time.Add(name, new SaveTime());
            return time[name];
        }
    }

    static User user = new User();
    public User GetUser() { return user; }
    public void SetUser(User _user) { user = _user; }
}
