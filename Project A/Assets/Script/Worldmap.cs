﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worldmap : MonoBehaviour {
    public UILabel stageName;
    public UILabel goldLabel;
    public UISprite[] enemys;
    UILabel[] enemysLabel;

    DB db;
    Battle battle;
    public void Init()
    {
        db = GetComponent<DB>();
        battle = GetComponent<Battle>();
        enemysLabel = new UILabel[enemys.Length];

        for (int i = 0; i < enemys.Length; ++i)
        {
            enemysLabel[i] = enemys[i].transform.FindChild("Label").GetComponent<UILabel>();
        }
    }

    public void StageSelect(GameObject stage)
    {
        //
        battle.Clear();
        battle.PlayerLIstUpdate();
        //
        int stageNumber = System.Convert.ToInt32(stage.name);
        Vector3 stagePosition = stage.transform.localPosition;
        DBSchema.Worldmap worldmap =  db.GetWorldmap(stageNumber);
        DBSchema.Enemy enemy;
        stageName.text = worldmap.name;
        goldLabel.text = worldmap.gold.ToString();

        battle.ClearEnemyUnit();
        for(int i = 0; i < enemys.Length; ++i)
        {
            try
            {
                enemys[i].enabled = true;
                enemysLabel[i].enabled = true;

                enemy = db.GetEnemy(worldmap.GetUnitName(i));
                enemys[i].spriteName = enemy.illustName;
                enemysLabel[i].text = worldmap.GetUnitCount(i).ToString();
                //
                battle.SetEnemyEnabled(i, true);
                battle.SetEnemy(i, enemy, enemysLabel[i].text, stageNumber);
            }
            catch
            {
                enemys[i].enabled = false;
                enemysLabel[i].enabled = false;
                //
                battle.SetEnemyEnabled(i, false);
            }
        }

        // Vector 이동
        Transform popupTransform = stageName.transform.parent;
        popupTransform.localPosition = stagePosition + new Vector3(0, 350, 0);
    }

    public void PopupReset()
    {
        Transform popupTransform = stageName.transform.parent;
        popupTransform.localPosition = new Vector3(-1000, 0, 0);
    }

    public int GetStageGold() { return System.Convert.ToInt32(goldLabel.text); }
}
